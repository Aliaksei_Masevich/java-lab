package by.epam.newsapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Class is used for handling controller exceptions.
 */
@ControllerAdvice
public class ExceptionHandlingController {

	/**
	 * Handles exceptions thrown by methods of controller.
	 * 
	 * @param exception
	 *            Exception to be handled
	 * @param request
	 *            HTTP request
	 * @return View name
	 */
	@ExceptionHandler(value = Exception.class)
	public String handlerServiceException(Exception exception,
			HttpServletRequest request) {
		request.setAttribute("exception", exception);
		return "error";
	}

}
