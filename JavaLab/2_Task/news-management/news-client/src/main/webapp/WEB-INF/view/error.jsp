<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>

<div class="b-error__container">
	<div class="b-error__item">
		<s:message code="error.label.error"/>
	</div>
	<c:url var="newsListUrl" value="/newslist"/>
	<div class="b-error__login">
		<a href="${newsListUrl}">
			<s:message code="error.reference.back"/>
		</a>
	</div>
	<c:forEach var="i" begin="0" end="10" step="1">
		<c:if test="${not empty exception}">
			<div class="b-error__item">
				<div class="b-error__message">
					<c:out value="${exception}"/>
				</div>
				<div class="b-error__stacktrace">
					<c:forEach var="entry" items="${exception.stackTrace}">
						<c:out value="${entry}"/><br>
					</c:forEach>
				</div>
			</div>
			<c:set var="exception" value="${exception.cause}"/>
		</c:if>
	</c:forEach>
</div>