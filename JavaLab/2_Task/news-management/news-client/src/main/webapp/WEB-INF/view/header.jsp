<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<div class="b-header">
	<div class="b-header__title">
		<s:message code="header.label.title" />
	</div>
	<div class="b-header__language">
		<a href="?language=en">
			<s:message code="header.reference.en"/>
		</a> 
		<a href="?language=ru">
			<s:message code="header.reference.ru"/>
		</a>
	</div>
</div>