<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>

<div class="b-header">
	<div class="b-header__title">
		<s:message code="header.label.title" />
	</div>
	<div class="b-header__user">			
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<s:message var="logoutButton" code="header.button.logout"/>
			<c:url var="logoutUrl" value="/logout"/>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<s:message code="header.label.welcome" />
				<c:out value="${sessionScope.username}"/>
				<input type="submit" value="${logoutButton}">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</sec:authorize>
	</div>
	<div class="b-header__language">
		<a href="?language=en"><s:message code="header.reference.en"/></a> 
		<a href="?language=ru"><s:message code="header.reference.ru"/></a>
	</div>
</div>