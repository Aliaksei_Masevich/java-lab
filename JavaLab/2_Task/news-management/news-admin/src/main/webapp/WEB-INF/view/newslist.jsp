<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<fmt:setLocale value="${requestScope.response.locale}" />

<div class="b-content__container">
	<div class="b-content">
		<div class="b-filer__container">
			<c:url value="/newslist" var="filterAction" />
			<sf:form method="POST" action="${filterAction}" commandName="searchCriteria">
				<div class="b-filter">
					<s:message var="authorPlaceholder" code="newslist.select.author"/>
					<sf:select class="b-authorSelect" path="authorId">
						<option value="" selected>${authorPlaceholder}</option>
						<c:forEach var="author" items="${authorList}">
							<sf:option value="${author.authorId}">
								<c:out value="${author.authorName}"/>
							</sf:option>
						</c:forEach>
					</sf:select>
					<s:message var="tagPlaceholder" code="newslist.select.tags"/>
					<sf:select class="b-tagListSelect" multiple="multiple" path="tagIdList">
						<option value="" selected disabled>${tagPlaceholder}</option>
						<c:forEach var="tag" items="${tagList}">
							<sf:option value="${tag.tagId}" >
								<c:out value="${tag.tagName}"/>
							</sf:option>
						</c:forEach>
					</sf:select>
				</div>
				<s:message var="filterButton" code="newslist.button.filter"/>
				<input class="b-filter__button" type="submit" value="${filterButton}" />
				<c:url  var="resetUrl" value="/newslist/reset"/>
				<s:message  var="resetButton" code="newslist.button.reset"/>
				<input class="b-filter__button" form="resetForm" type="button" 
					value="${resetButton}" onclick="location.href='${resetUrl}';"/>
			</sf:form>
		</div>
		<div class="b-news__container">
			<div class="b-content__news">
				<c:forEach var="newsVO" items="${newsVOList}">
					<div class="b-news__item">
						<c:url value="/newsview/${newsVO.news.newsId}" var="newsViewUrl"/>
						<div class="b-news__title">
							<a href="${newsViewUrl}">
								<c:out value="${newsVO.news.title}"/>
							</a>
						</div>
						<div class="b-news__author">
							<s:message code="newslist.label.author" />
							<c:out value="${newsVO.author.authorName}"/>
							<s:message code="newslist.label.authorend"/>
						</div>
						<div class="b-news__date">
							<fmt:formatDate dateStyle="short" 
								value="${newsVO.news.modificationDate}" />
						</div>
						<div class="b-news__shorttext">
							<c:out value="${newsVO.news.shortText}"/>
						</div>
						<div class="b-news__tags">
							<c:forEach var="tag" items="${newsVO.tagList}">
								<c:out value="${tag.tagName}"/>
							</c:forEach>
						</div>
						<div class="b-news__comments">
							<s:message code="newslist.label.comments"/>
							<c:out value="${fn:length(newsVO.commentList)}"/>
							<s:message code="newslist.label.commentsend"/>
						</div>
						<div class="b-news__view__button">
							<c:url var="updateNewsUrl" value="/news/update/${newsVO.news.newsId}"/>
							<a href="${updateNewsUrl}">
								<s:message code="newslist.reference.edit"/>
							</a>
							<input form="deleteForm" type="checkbox" 
								name="newsId" value="${newsVO.news.newsId}"/>
						</div>
					</div>
				</c:forEach>
				<c:url var="deleteNewsUrl" value="/newslist/deletenews"/>
				<s:message var="confirmMessage" code="newslist.confirm.delete"/>
				<s:message var="noItemSelectedMessage" code="newslist.alert.delete"/>
				<form action="${deleteNewsUrl}" id="deleteForm" name="deleteForm" method="POST" 
					onsubmit="return checkDeleteForm(this,'${confirmMessage}','${noItemSelectedMessage}')">
					<s:message  var="deleteButton" code="newslist.button.delete"/>
					<input type="hidden" name="currentPage" value="${currentPage}"/>
					<input class="b-news__delete__button" type="submit" value="${deleteButton}" />
				</form>
			</div>
		</div>
		<div class="b-pagination">
			<div class="b-pagination__container">
				<c:forEach var="i" begin="${startPage}" end="${endPage}">
					<c:choose>
						<c:when test="${i ne currentPage}">
							<c:url var="pageUrl" value="/newslist?currentPage=${i}"/>
							<a href="${pageUrl}" class="b-page">
								<c:out value="${i}"/>
							</a>
						</c:when>
						<c:otherwise>
							<span class="b-page b-page__active">
								<c:out value="${i}"/>
							</span>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
