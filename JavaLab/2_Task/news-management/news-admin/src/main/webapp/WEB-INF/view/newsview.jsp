<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<fmt:setLocale value="${requestScope.response.locale}" />
<s:message var="deletecCommentButton" code="newsview.button.deletecomment"/>
<s:message var="confirmMessage" code="newsview.confirm.delete"/>

<div class="b-content__container">
	<div class="b-content">
		<c:url value="/newslist/back?newsId=${newsVO.news.newsId}" var="goBackUrl"/>
		<div class="b-newsview__navigation__back">
			<a href="${goBackUrl}">
				<s:message code="newsview.reference.back"/>
			</a>
		</div>
		<div class="b-newsview__news__container">
			<div class="b-newsview__news__title">
				<c:out value="${newsVO.news.title}"/>
			</div>
			<div class="b-newsview__news__author">
				<s:message code="newsview.label.author" />
				<c:out value="${newsVO.author.authorName}"/>
				<s:message code="newsview.label.authorend"/>
			</div>
			<div class="b-newsview__news__date">
				<fmt:formatDate dateStyle="short" value="${newsVO.news.modificationDate}"/>
			</div>
			<div class="b-newsview__news__fulltext">
				<c:out value="${newsVO.news.fullText}"/>
			</div>
			<div class="b-newsview__news__comment__container">
				<c:url var="deleteComment" value="/newsview/deletecomment"/>
				<c:forEach var="comment" items="${newsVO.commentList}">
					<div class="b-newsview__news__comment__item">
						<div class="b-newsview__news__comment__date">
							<fmt:formatDate dateStyle="short" value="${comment.creationDate}" />
						</div>
						<div class="b-newsview__news__comment__body">
							<div class="b-newsview__news__comment__text">
								<c:out value="${comment.commentText}"/>
							</div>
							<form method="POST" action="${deleteComment}"  onsubmit="return confirm('${confirmMessage}')">
								<input type="hidden" name="commentId" value="${comment.commentId}"/>
								<input type="hidden" name="newsId" value="${newsVO.news.newsId}"/>
								<input class="b-newsview__news__comment__delete" type="submit" value="${deletecCommentButton}">
							</form>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<c:url var="postComment" value="/newsview/postcomment"/>
		<div class="b-newview__postform">
			<sf:form method="POST" action="${postComment}"  commandName="commentTO">
				<sf:textarea class="b-newsview__news__comment__textarea" path="commentText"></sf:textarea>
				<s:message var="postButton" code="newsview.button.post"/>
				<sf:input type="hidden" path="newsId" value="${newsVO.news.newsId}" />
				<div class="b-newsview__news__comment__post">
					<input type="submit" value="${postButton}">
				</div>
				<sf:errors path="*" cssClass="b-errorblock" element="div" />
			</sf:form>
		</div>
		<div class="b-newsvview__navigation__previous">
			<c:if test="${newsVO.news.newsId ne previousNewsId}">
				<c:url var="previousNewsUrl" value="/newsview/${previousNewsId}" />
				<a href="${previousNewsUrl}">
					<s:message code="newsview.reference.previous"/>
				</a>
			</c:if>
		</div>
		<div class="b-newsvview__navigation__next">
			<c:if test="${newsVO.news.newsId ne nextNewsId}">
				<c:url var="nextNewsUrl" value="/newsview/${nextNewsId}" />
				<a href="${nextNewsUrl}">
					<s:message code="newsview.reference.next" />
				</a>
			</c:if>
		</div>
	</div>
</div>
