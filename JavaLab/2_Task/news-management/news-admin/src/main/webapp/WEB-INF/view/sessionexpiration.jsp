<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>

<div class="b-sessionexpiration__container">
	<div class="b-sessionexpiration__title">
		<s:message code="sessionexpiration.label.error"/>
	</div>
	<c:url value="/login" var="backToLoginUrl"/>
	<div class="b-sessionexpiration__login">
		<a href="${backToLoginUrl}">
			<s:message code="sessionexpiration.reference.back"/>
		</a>
	</div>
</div>
