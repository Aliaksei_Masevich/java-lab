function startEdit(form) {
	form.tagName.disabled = false;
	form.updateButton.type = "hidden";
	if (form.deleteButton != null) {
		form.deleteButton.type = "button";
	}
	form.saveButton.type = "submit";
	form.cancelButton.type = "button";
}

function cancelEdit(form) {
	form.tagName.value = form.hiddenName.value;
	form.tagName.disabled = true;
	form.updateButton.type = "submit";
	if (form.deleteButton != null) {
		form.deleteButton.type = "hidden";
	}
	form.saveButton.type = "hidden";
	form.cancelButton.type = "hidden";
}

function confirmDelete(form, deleteUrl, confirmMessage){
	if (confirm(confirmMessage)){
		form.action = deleteUrl;
		form.submit();
	}
}