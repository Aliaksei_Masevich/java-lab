function startEdit(form) {
	form.authorName.disabled = false;
	form.updateButton.type = "hidden";
	if (form.deleteButton != null) {
		form.deleteButton.type = "button";
	}
	form.saveButton.type = "submit";
	form.cancelButton.type = "button";
}

function cancelEdit(form) {
	form.authorName.value = form.hiddenName.value;
	form.authorName.disabled = true;
	form.updateButton.type = "button";
	if (form.deleteButton != null) {
		form.deleteButton.type = "hidden";
	}
	form.saveButton.type = "hidden";
	form.cancelButton.type = "hidden";
}