package by.epam.newsapp.util;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class PasswordEncoderGenerator {

	public static void main(String[] args) {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
	    String hashedPass = encoder.encodePassword("admin1", null);
	    System.out.println(hashedPass);
	}

}
