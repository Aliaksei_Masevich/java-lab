package by.epam.newsapp.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * Class is used for handling request connected with login logic.
 */
@Controller
@SessionAttributes("username")
public class LoginController {

	/**
	 * Handles GET requests for /login. Sets login view with error parameter if
	 * login error occurs.
	 * 
	 * @param error
	 *            Error identifier
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value = "error", required = false) String error,
			Model model) {
		if (error != null) {
			model.addAttribute("error", true);
		}
		return "login";
	}

	/**
	 * Handles GET requests for /403. Sets model attributes for 403 view.
	 * 
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 */
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accesssDenied(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addAttribute("username", userDetail.getUsername());
		}
		return "403";

	}

	/**
	 * Handles GET requests for /sessionexpiration.
	 * 
	 * @return View name
	 */
	@RequestMapping(value = "/sessionexpiration", method = RequestMethod.GET)
	public String handleSessionExpiration() {
		return "sessionexpiration";
	}

	/**
	 * Handles GET request for /setusername. Sets username session attribute if
	 * user is logged in.
	 * 
	 * @param model
	 *            Model attribute holder
	 * @return Redirection URL
	 */
	@RequestMapping(value = "/setusername", method = RequestMethod.GET)
	public String setUserName(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addAttribute("username", userDetail.getUsername());
		}
		return "redirect:/newslist";
	}

}
