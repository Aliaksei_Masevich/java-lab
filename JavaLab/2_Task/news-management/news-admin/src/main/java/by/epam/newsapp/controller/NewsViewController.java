package by.epam.newsapp.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.service.NewsManagementService;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for handling requests for /newsview.
 */
@Controller
@SessionAttributes("searchCriteria")
@RequestMapping("/newsview")
public class NewsViewController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private NewsService newsService;

	/**
	 * Handles GET requests for /newsview/{newdId}. Sets model attributes for
	 * newsview view.
	 * 
	 * @param newsId
	 *            News id
	 * @param searchCriteria
	 *            Search criteria
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	public String showNews(
			@PathVariable(value = "newsId") Long newsId,
			@ModelAttribute(value = "searchCriteria") SearchCriteria searchCriteria,
			Model model) throws ServiceException {
		model.addAttribute("newsVO", newsManagementService.getNews(newsId));
		model.addAttribute("nextNewsId",
				newsService.getNextNewsId(newsId, searchCriteria));
		model.addAttribute("previousNewsId",
				newsService.getPreviousNewsId(newsId, searchCriteria));
		if (!model.containsAttribute("commentTO")) {
			model.addAttribute("commentTO", new CommentTO());
		}
		return "newsview";
	}

	/**
	 * Handles POST requests for /newsview/postcomment. Posts comment if passed
	 * parameters are valid. Otherwise sets redirect attributes with error
	 * messages.
	 * 
	 * @param commentTO
	 *            CommentTO to be posted
	 * @param result
	 *            Binding result
	 * @param redirectAttributes
	 *            Redirect attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/postcomment", method = RequestMethod.POST)
	public String postComment(@Valid CommentTO commentTO, BindingResult result,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes.addFlashAttribute(result.getObjectName(),
					commentTO);
		} else {
			commentTO.setCreationDate(new Date());
			commentService.addComment(commentTO);
		}
		return "redirect:/newsview/" + commentTO.getNewsId();
	}

	/**
	 * Handles POST requests for /newsview/deletecomment. Deletes comment by
	 * comment id.
	 * 
	 * @param commentId
	 *            Comment id
	 * @param newsId
	 *            News id
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deletecomment", method = RequestMethod.POST)
	public String deleteComment(
			@RequestParam(value = "commentId", required = true) Long commentId,
			@RequestParam(value = "newsId", required = true) Long newsId)
			throws ServiceException {
		commentService.deleteComment(commentId);
		return "redirect:/newsview/" + newsId;
	}
}
