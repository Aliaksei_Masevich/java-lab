package by.epam.newsapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import by.epam.newsapp.entity.NewsVO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManagementService;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.PaginationUtil;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for handling requests for /newslist.
 */
@Controller
@SessionAttributes("searchCriteria")
@RequestMapping(value = "/newslist")
public class NewsListController {

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Value("${newslist.newsperpage}")
	private Long newsPerPage;

	@Value("${newslist.maxpagecount}")
	private Long maxPageCount;

	@ModelAttribute("searchCriteria")
	public SearchCriteria createSearchCriteria() {
		return new SearchCriteria();
	}

	/**
	 * Handles POST and GET requests for /newslist. Sets model attributes for
	 * newslist view.
	 * 
	 * @param currentPage
	 *            Current page number
	 * @param searchCriteria
	 *            Search criteria
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String showNewsList(
			@RequestParam(value = "currentPage", defaultValue = "1") Long currentPage,
			@ModelAttribute(value = "searchCriteria") SearchCriteria searchCriteria,
			Model model) throws ServiceException {
		PaginationUtil paginationUtil = new PaginationUtil(newsPerPage,
				maxPageCount);
		Long newsCount = newsService.getSearchedNewsCount(searchCriteria);
		currentPage = paginationUtil.setPagination(currentPage, newsCount,
				model);
		model.addAttribute("newsVOList",
				getNewsVOForPage(currentPage, searchCriteria));
		model.addAttribute("authorList", authorService.getAllAuthors());
		model.addAttribute("tagList", tagService.getAllTags());
		return "newslist";
	}

	/**
	 * Handles POST requests for /newslist/deletenews. Deletes news by ids.
	 * 
	 * @param newsIds
	 *            News id array
	 * @param currentPage
	 *            Current page number
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deletenews", method = RequestMethod.POST)
	public String deleteNews(@RequestParam(value = "newsId") Long[] newsIds,
			@RequestParam(value = "currentPage") Long currentPage)
			throws ServiceException {
		if (newsIds != null) {
			for (Long newsId : newsIds) {
				newsManagementService.deleteNews(newsId);
			}
		}
		return "redirect:/newslist?currentPage=" + currentPage;
	}

	/**
	 * Handles POST requests for /newslist/reset. Resets search criteria.
	 * 
	 * @param searchCriteria
	 *            Search criteria
	 * @return Redirection URL
	 */
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String resetFilter(
			@ModelAttribute("searchCriteria") SearchCriteria searchCriteria) {
		searchCriteria.setAuthorId(null);
		searchCriteria.setTagIdList(null);
		return "redirect:/newslist";
	}

	/**
	 * Handles GET request for /newslist/back. Calculates news list page number
	 * by news id.
	 * 
	 * @param newsId
	 *            News id
	 * @param searchCriteria
	 *            Search criteria
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/back", method = RequestMethod.GET)
	public String goBack(
			@RequestParam(value = "newsId") Long newsId,
			@ModelAttribute(value = "searchCriteria") SearchCriteria searchCriteria)
			throws ServiceException {
		Long newsNumber = newsService.getSearchedNewsNumber(searchCriteria,
				newsId);
		PaginationUtil paginationUtil = new PaginationUtil(newsPerPage,
				maxPageCount);
		Long currentPage = paginationUtil
				.getCurrentPageByElementNumber(newsNumber);
		return "redirect:/newslist?currentPage=" + currentPage;
	}

	/**
	 * Gets NewsVO list for current page.
	 * 
	 * @param currentPage
	 *            Current page
	 * @param searchCriteria
	 *            Search criteria
	 * @return NewsVO list
	 * @throws ServiceException
	 */
	private List<NewsVO> getNewsVOForPage(Long currentPage,
			SearchCriteria searchCriteria) throws ServiceException {
		PaginationUtil paginationUtil = new PaginationUtil(newsPerPage,
				maxPageCount);
		Long startNewsNumber = paginationUtil
				.getStartElementNumber(currentPage);
		Long endNewsNumber = paginationUtil.getEndElementNumber(currentPage);
		return newsManagementService.search(searchCriteria, startNewsNumber,
				endNewsNumber);
	}
}
