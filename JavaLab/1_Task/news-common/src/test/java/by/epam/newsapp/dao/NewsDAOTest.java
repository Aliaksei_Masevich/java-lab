package by.epam.newsapp.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.DateParser;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for {@link NewsDAO} testing.
 */
@RunWith(UnitilsJUnit4TestClassRunner.class)
@SpringApplicationContext("test-application-context.xml")
@DataSet("dao/NewsDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class NewsDAOTest {
	
	@SpringBean("newsDAO")
	private NewsDAO newsDAO;
	
	/**
	 * Tests {@link NewsDAO#getAllNews()} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetAllNews() throws DAOException, ParseException{
		List<NewsTO> newsList = newsDAO.getAllNews();
		int expectedNewsCount = 3;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������","������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59",
				"2015-03-22 23:59:59");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#getNewsById(Long)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetNewsById() throws DAOException, ParseException{
		NewsTO expectedNews = createNews(
				1L,
				"��� � ������",
				"��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00",
				"2015-06-16 22:30:00");
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#addNews(NewsTO)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testAddNews() throws DAOException, ParseException{
		NewsTO expectedNews = createNews(
				null,
				"IPhone 6",
				"� �������� ���������� ����������� ������� iPhone 6",
				"������ ������ ��� ����� ������ ������� iPhone ����������� ������� ������������ ���������� �������-�� ���������� � � ��������.",
				"2015-06-26 08:00:00",
				"2015-06-26 08:00:00");
		expectedNews.setNewsId(newsDAO.addNews(expectedNews));
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#updateNews(NewsTO)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testUpdateNews() throws DAOException, ParseException{
		NewsTO expectedNews = createNews(
				1L,
				"����� ������������� ���������",
				"����� ������������� ���������: ������ � ������� ������ ��� ��������",
				"������ ������� ������ ������� � ������� ����� ���� ������� ��� �������� ��������. ...",
				"2015-06-24 10:00:00",
				"2015-06-24 11:05:00");
		newsDAO.updateNews(expectedNews);
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#getAuthorIdByNewsId(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetAuthorIdByNewsId() throws DAOException {
		Long expectedAuthorId = 2L;
		Long actualAuthorId = newsDAO.getAuthorIdByNewsId(3L);
		assertEquals(expectedAuthorId, actualAuthorId);
	}
	
	/**
	 * Tests {@link NewsDAO#getTagIdListByNewsId(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetTagIdListByNewsId() throws DAOException {
		List<Long> expectedTagIdList = new ArrayList<Long>();
		expectedTagIdList.add(3L);
		expectedTagIdList.add(4L);
		List<Long> actualTagIdList = newsDAO.getTagIdListByNewsId(1L);
		assertEquals(expectedTagIdList.size(), actualTagIdList.size());
		assertEquals(expectedTagIdList.get(0), actualTagIdList.get(0));
	}
	
	/**
	 * Tests {@link NewsDAO#attachNewsAuthor(Long, Long) for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testAttachNewsAuthor() throws DAOException {
		Long expectedAuthorId = 2L;
		Long newsId = 2L;
		newsDAO.attachNewsAuthor(newsId, expectedAuthorId);
		Long actualAuthorId = newsDAO.getAuthorIdByNewsId(newsId);
		assertEquals(expectedAuthorId, actualAuthorId);
	}
	
	/**
	 * Tests {@link NewsDAO#attachNewsTags(Long, List)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testAttachNewsTags() throws DAOException {
		List<Long> expectedTagIdList = new ArrayList<Long>();
		expectedTagIdList.add(1L);
		expectedTagIdList.add(2L);
		Long newsId = 2L;
		newsDAO.attachNewsTags(newsId, expectedTagIdList);
		List<Long> actualTagIdList = newsDAO.getTagIdListByNewsId(newsId);
		assertEquals(expectedTagIdList.size(), actualTagIdList.size());
		assertEquals(expectedTagIdList.get(0), actualTagIdList.get(0));
	}
	
	/**
	 * Tests {@link NewsDAO#deleteNewsAuthor(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteNewsAuthor() throws DAOException {
		Long newsId = 1L;
		newsDAO.deleteNewsAuthor(newsId);
		Long actualAuthorId = newsDAO.getAuthorIdByNewsId(newsId);
		assertNull(actualAuthorId);
	}
	
	/**
	 * Tests {@link NewsDAO#deleteNewsTags(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteNewsTags() throws DAOException {
		Long newsId = 1L;
		newsDAO.deleteNewsTags(newsId);
		List<Long> actualTagIdList = newsDAO.getTagIdListByNewsId(newsId);
		int expectedTagIdCount = 0;
		assertEquals(expectedTagIdCount, actualTagIdList.size());
	}
	
	/**
	 * Tests {@link NewsDAO#deleteNews(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteNews() throws DAOException{
		Long newsId = 2L;
		newsDAO.deleteNews(newsId);
		NewsTO actualNews = newsDAO.getNewsById(newsId);
		assertNull(actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#deleteNews(Long)} for throwing an exception.
	 * @throws DAOException
	 */
	@Test(expected = DAOException.class)
	public void testDeleteNewsForException() throws DAOException {
		Long newsId = 1L;
		newsDAO.deleteNews(newsId);
	}
	
	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by author.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByAuthor() throws DAOException, ParseException{
		Long authorId = 2L;
		List<Long> tagIdList = new ArrayList<Long>();
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria,1L,3L);
		int expectedNewsCount = 1;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������","������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59",
				"2015-03-22 23:59:59");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by tags.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByTags() throws DAOException, ParseException{
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(3L);
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria,1L,3L);
		int expectedNewsCount = 2;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������",
				"������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59",
				"2015-03-22 23:59:59");
		NewsTO actualNews = newsList.get(1);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by author and tags.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByAuthorAndTags() throws DAOException, ParseException{
		Long authorId = 1L;
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria,1L,3L);
		int expectedNewsCount = 1;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				1L,
				"��� � ������",
				"��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00",
				"2015-06-16 22:30:00");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search without search criteria.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchWithoutSearchCriteria() throws DAOException, ParseException{
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsTO> newsList = newsDAO.search(searchCriteria,1L,3L);
		int expectedNewsCount = 3;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������","������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59",
				"2015-03-22 23:59:59");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#getNewsPart(Long, Long)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetNewsPart() throws DAOException, ParseException{
		List<NewsTO> newsList = newsDAO.getNewsPart(1L, 2L);
		int expectedNewsCount = 2;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������","������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59",
				"2015-03-22 23:59:59");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#getNewsCount()} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetNewsCount() throws DAOException {
		Long expectedNewsCount = 3L;
		Long actualNewsCount = newsDAO.getNewsCount();
		assertEquals(expectedNewsCount, actualNewsCount);
	}
	
	/**
	 * Tests {@link NewsDAO#getAuthorByNewsId(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetAuthorByNewsId() throws DAOException {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("Lenta.ru");
		AuthorTO actualAuthor = newsDAO.getAuthorByNewsId(3L);
		assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
		assertEquals(expectedAuthor.getExpired(), actualAuthor.getExpired());
	}
	
	/**
	 * Tests {@link NewsDAO#getTagListByNewsId(Long) for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetTagListByNewsId() throws DAOException {
		List<TagTO> expectedTagList = new ArrayList<TagTO>();
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(3L);
		expectedTag.setTagName("����������");
		TagTO tag = new TagTO();
		tag.setTagId(4L);
		tag.setTagName("��������");
		expectedTagList.add(expectedTag);
		expectedTagList.add(tag);
		List<TagTO> actualTagList = newsDAO.getTagListByNewsId(1L);
		assertEquals(expectedTagList.size(), actualTagList.size());
		TagTO actualTag = actualTagList.get(0);
		assertEquals(expectedTag.getTagId(), actualTag.getTagId());
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
	
	/**
	 * Tests {@link NewsDAO#getSearchedNewsCount(SearchCriteria)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetSearchedNewsCount() throws DAOException {
		Long authorId = 1L;
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(3L);
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		Long expectedNewsCount = 2L;
		Long actualNewsCount = newsDAO.getSearchedNewsCount(searchCriteria);
		assertEquals(expectedNewsCount, actualNewsCount);
	}
	
	/**
	 * Tests {@link NewsDAO#updateNewsAuthor(Long, Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testUpdateNewsAuthor() throws DAOException {
		Long expectedAuthorId = 2L;
		Long newsId = 1L;
		newsDAO.updateNewsAuthor(newsId, expectedAuthorId);
		Long actualAuthorId = newsDAO.getAuthorIdByNewsId(newsId);
		assertEquals(expectedAuthorId, actualAuthorId);
	}
	
	/**
	 * Tests {@link NewsDAO#getSearchedNewsNumber(SearchCriteria, Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetSearchedNewsNumber() throws DAOException {
		Long expectedNewsNumber = 3L;
		Long newsId = 2L;
		SearchCriteria searchCriteria = new SearchCriteria();
		Long actualNewsNumber = newsDAO.getSearchedNewsNumber(searchCriteria, newsId);
		assertEquals(expectedNewsNumber, actualNewsNumber);
	}
	
	/**
	 * Tests {@link NewsDAO#detachTagFromNews(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDetachTagFromNews() throws DAOException {
		Long newsId = 3L;
		newsDAO.detachTagFromNews(3L);
		List<Long> expectedTagIdList = new ArrayList<Long>();
		List<Long> actualTagIdList = newsDAO.getTagIdListByNewsId(newsId);
		assertEquals(expectedTagIdList.size(), actualTagIdList.size());
	}
	
	/**
	 * Tests {@link NewsDAO#getPreviousNewsId(Long, SearchCriteria)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetPreviousNews() throws DAOException, ParseException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Long newsId = 1L;
		Long expectedNewsId = 3L;
		Long actualNewsId = newsDAO.getPreviousNewsId(newsId, searchCriteria);
		assertEquals(expectedNewsId, actualNewsId);
	}
	
	/**
	 * Tests {@link NewsDAO#getNextNewsId(Long, SearchCriteria)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetNextNews() throws DAOException, ParseException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Long newsId = 3L;
		Long expectedNewsId = 1L;
		Long actualNewsId = newsDAO.getNextNewsId(newsId, searchCriteria);
		assertEquals(expectedNewsId, actualNewsId);
	}
	
	/**
	 * Asserts that two NewsTO objects are equal.
	 * @param expected Expected NewsTO object
	 * @param actual Actual NewsTO object
	 */
	private void assertNewsEquals(NewsTO expected, NewsTO actual){
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getModificationDate(), actual.getModificationDate());
	}
	
	/**
	 * Creates new NewsTO object and sets its fields with values.
	 * @param newsId News id
	 * @param title News title
	 * @param shortText Short text
	 * @param fullText Full text
	 * @param creationDate Creation date
	 * @param modificationDate Modification date
	 * @return Created NewsTO object
	 * @throws ParseException
	 */
	private NewsTO createNews(
			Long newsId,
			String title,
			String shortText,
			String fullText,
			String creationDate,
			String modificationDate) throws ParseException{
		NewsTO news = new NewsTO();
		news.setNewsId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(DateParser.parse(creationDate, "yyyy-MM-dd HH:mm:ss"));
		news.setModificationDate(DateParser.parse(modificationDate, "yyyy-MM-dd HH:mm:ss"));
		return news;
	}
	
}
