package by.epam.newsapp.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;

/**
 * Class is used for {@link CommentService} testing.
 */
@ContextConfiguration(locations = {"classpath:/test-application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {
	
	@InjectMocks
	@Autowired
	private CommentService commentService;
	
	@Mock
	private CommentDAO commentDAO;
	
	private CommentTO testComment;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testComment = new CommentTO();
		testComment.setCommentId(1L);
		testComment.setNewsId(1L);
		testComment.setCommentText("Great Job!");
		testComment.setCreationDate(new Date());
	}

	/**
	 * Tests {@link CommentService#getCommentById(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetCommentById() throws ServiceException, DAOException {
		Long commentId = testComment.getCommentId();
		when(commentDAO.getCommentById(commentId)).thenReturn(testComment);
		CommentTO actualComment = commentService.getCommentById(commentId);
		assertCommentEquals(testComment, actualComment);
		verify(commentDAO).getCommentById(commentId);
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Tests {@link CommentService#addComment(CommentTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testAddComment() throws ServiceException, DAOException {
		when(commentDAO.addComment(testComment)).thenReturn(testComment.getCommentId());
		Long actualCommentId = commentService.addComment(testComment);
		assertEquals(testComment.getCommentId(), actualCommentId);
		verify(commentDAO).addComment(testComment);
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Tests {@link CommentService#deleteComment(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testDeleteComment() throws ServiceException, DAOException {
		commentService.deleteComment(testComment.getCommentId());
		verify(commentDAO).deleteComment(testComment.getCommentId());
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Tests {@link CommentService#updateComment(CommentTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testUpdateComment() throws ServiceException, DAOException {
		commentService.updateComment(testComment);
		verify(commentDAO).updateComment(testComment);
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Tests {@link CommentService#getCommentsByNewsId(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetCommentsByNewsId() throws ServiceException, DAOException {
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		commentList.add(testComment);
		when(commentDAO.getCommentsByNewsId(testComment.getNewsId())).thenReturn(commentList);
		List<CommentTO> actualCommentList = commentService.getCommentsByNewsId(testComment.getNewsId());
		assertEquals(commentList.size(),actualCommentList.size());
		assertCommentEquals(testComment, actualCommentList.get(0));
		verify(commentDAO).getCommentsByNewsId(testComment.getNewsId());
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Tests {@link CommentService#deleteCommentsByNewsId(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testDeleteCommentsByNewsId() throws ServiceException, DAOException {
		commentService.deleteCommentsByNewsId(testComment.getNewsId());
		verify(commentDAO).deleteCommentsByNewsId(testComment.getNewsId());
		verifyNoMoreInteractions(commentDAO);
	}
	
	/**
	 * Asserts that two CommentTO objects are equal.
	 * @param expected Expected CommentTO object
	 * @param actual Actual CommentTO object
	 */
	private void assertCommentEquals(CommentTO expected, CommentTO actual){
		assertEquals(expected.getCommentId(), actual.getCommentId());
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getCommentText(), actual.getCommentText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
	}
}
