package by.epam.newsapp.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.DateParser;

/**
 * Class is used for {@link CommentDAO} testing.
 */
@RunWith(UnitilsJUnit4TestClassRunner.class)
@SpringApplicationContext("test-application-context.xml")
@DataSet("dao/CommentDAOTest.xml")
public class CommentDAOTest {
	
	@SpringBean("commentDAO")
	private CommentDAO commentDAO;
	
	/**
	 * Tests {@link CommentDAO#getCommentById(Long)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetCommentById() throws DAOException, ParseException{
		CommentTO expectedComment = createComment(1L, 1L, "Great Job!", "2015-06-16 22:30:45");
		CommentTO actualComment = commentDAO.getCommentById(expectedComment.getCommentId());
		assertCommentEquals(expectedComment, actualComment);
	}
	
	/**
	 * Tests {@link CommentDAO#addComment(CommentTO)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testAddComment() throws DAOException, ParseException{
		CommentTO expectedComment = createComment(null, 2L, "Whyyy?", "2015-06-30 00:00:00");
		expectedComment.setCommentId(commentDAO.addComment(expectedComment));
		CommentTO actualComment = commentDAO.getCommentById(expectedComment.getCommentId());
		assertCommentEquals(expectedComment, actualComment);
	}
	/**
	 * Tests {@link CommentDAO#deleteComment(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteComment() throws DAOException{
		Long commentId = 1L;
		commentDAO.deleteComment(commentId);
		CommentTO actualComment = commentDAO.getCommentById(commentId);
		assertNull(actualComment);
	}
	
	/**
	 * Tests {@link CommentDAO#updateComment(CommentTO)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test 
	public void testUpdateComment() throws DAOException, ParseException{
		CommentTO expectedComment = createComment(1L, 1L, "Really?", "2001-03-23 23:45:59");
		commentDAO.updateComment(expectedComment);
		CommentTO actualComment = commentDAO.getCommentById(expectedComment.getCommentId());
		assertCommentEquals(expectedComment, actualComment);
	}

	/**
	 * Tests {@link CommentDAO#getCommentsByNewsId(Long)} for positive case.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetCommentsByNewsId() throws DAOException, ParseException{
		Long newsId = 3L;
		List<CommentTO> comments = commentDAO.getCommentsByNewsId(newsId);
		int expectedCommentCount = 2;
		int actualCommentCount = comments.size();
		assertEquals(expectedCommentCount, actualCommentCount);
		CommentTO expectedComment = createComment(4L, 3L, "������!", "2015-03-23 00:00:00");
		CommentTO actualComment = comments.get(0);
		assertCommentEquals(expectedComment, actualComment);
	}
	
	/**
	 * Tests {@link CommentDAO#deleteCommentsByNewsId(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteCommentsByNewsId() throws DAOException{
		Long newsId = 3L;
		commentDAO.deleteCommentsByNewsId(newsId);
		List<CommentTO> comments = commentDAO.getCommentsByNewsId(newsId);
		int expectedCommentCount = 0;
		int actualCommentCount = comments.size();
		assertEquals(expectedCommentCount, actualCommentCount);
	}
	
	/**
	 * Asserts that two CommentTO objects are equal.
	 * @param expected Expected CommentTO object
	 * @param actual Actual CommentTO object
	 */
	private void assertCommentEquals(CommentTO expected, CommentTO actual){
		assertEquals(expected.getCommentId(), actual.getCommentId());
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getCommentText(), actual.getCommentText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
	}
	
	/**
	 * Creates new CommentTO object and sets its fields with values.
	 * @param commentId Comment id
	 * @param newsId Commented news id
	 * @param commentText Comment text
	 * @param creationDate Comment creation date
	 * @return Create CommentTO object
	 * @throws ParseException
	 */
	private CommentTO createComment(
			Long commentId,
			Long newsId,
			String commentText,
			String creationDate) throws ParseException{
		CommentTO comment = new CommentTO();
		comment.setCommentId(commentId);
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(DateParser.parse(creationDate, "yyyy-MM-dd HH:mm:ss"));
		return comment;
	}
}
