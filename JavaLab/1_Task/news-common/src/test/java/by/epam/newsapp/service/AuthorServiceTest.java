package by.epam.newsapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;

/**
 * Class is used for {@link AuthorService} testing.
 */
@ContextConfiguration(locations = {"classpath:/test-application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
	
	@InjectMocks
	@Autowired
	private AuthorService authorService;
	
	@Mock
	private AuthorDAO authorDAO;
	
	private AuthorTO testAuthor;
		
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testAuthor = new AuthorTO();
		testAuthor.setAuthorId(1L);
		testAuthor.setAuthorName("AUTO.TUT.BY");
	}
	
	/**
	 * Tests {@link AuthorService#getAllAuthors()} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetAllAuthors() throws ServiceException, DAOException {
		List<AuthorTO> authorList = new ArrayList<AuthorTO>();
		authorList.add(testAuthor);
		when(authorDAO.getAllAuthors()).thenReturn(authorList);
		List<AuthorTO> actualAuthorList = authorService.getAllAuthors();
		assertEquals(authorList.size(),actualAuthorList.size());
		assertAuthorEquals(testAuthor, actualAuthorList.get(0));
		verify(authorDAO).getAllAuthors();
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Tests {@link AuthorService#addAuthor(AuthorTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testAddAuthor() throws ServiceException, DAOException{
		when(authorDAO.addAuthor(testAuthor)).thenReturn(testAuthor.getAuthorId());
		Long actualAuthorId = authorService.addAuthor(testAuthor);
		assertEquals(testAuthor.getAuthorId(),actualAuthorId);
		verify(authorDAO).addAuthor(testAuthor);
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Tests {@link AuthorService#getAuthorById(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetAuthorById() throws ServiceException, DAOException {
		Long authorId = testAuthor.getAuthorId();
		when(authorDAO.getAuthorById(authorId)).thenReturn(testAuthor);
		AuthorTO actualAuthor = authorService.getAuthorById(authorId);
		assertAuthorEquals(testAuthor, actualAuthor);
		verify(authorDAO).getAuthorById(authorId);
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Tests {@link AuthorService#updateAuthor(AuthorTO)} for positive case. 
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testUpdateAuthor() throws ServiceException, DAOException {
		authorService.updateAuthor(testAuthor);
		verify(authorDAO).updateAuthor(testAuthor);
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Tests {@link AuthorService#deleteAuthor(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testDeleteAuthor() throws ServiceException, DAOException {
		authorService.deleteAuthor(testAuthor.getAuthorId());
		verify(authorDAO).deleteAuthor(testAuthor.getAuthorId());
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Tests {@link AuthorService#deleteAuthor(Long)} for throwing an exception.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test(expected = ServiceException.class)
	public void testDeleteAuthorForException() throws ServiceException, DAOException {
		doThrow(new DAOException()).when(authorDAO).deleteAuthor(testAuthor.getAuthorId());
		authorService.deleteAuthor(testAuthor.getAuthorId());
	}
	
	/**
	 * Tests {@link AuthorService#setExpired(Long, Date)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSetExpired() throws ServiceException, DAOException {
		Long authorId = 1L;
		Date expirationDate = new Date();
		authorService.setExpired(authorId, expirationDate);
		verify(authorDAO).setExpired(authorId, expirationDate);
		verifyNoMoreInteractions(authorDAO);
	}
	
	/**
	 * Asserts that two AuthorTO objects are equal.
	 * @param expected Expected AuthorTO object
	 * @param actual Actual AuthorTO object
	 */
	private void assertAuthorEquals(AuthorTO expected, AuthorTO actual){
		assertEquals(expected.getAuthorId(), actual.getAuthorId());
		assertEquals(expected.getAuthorName(), actual.getAuthorName());
		assertEquals(expected.getExpired(), actual.getExpired());
	}
}
