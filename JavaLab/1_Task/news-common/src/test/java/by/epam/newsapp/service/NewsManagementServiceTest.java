package by.epam.newsapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for {@link NewsManagementService} testing.
 */
@ContextConfiguration(locations = {"classpath:/test-application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManagementServiceTest {
	
	@InjectMocks
	@Autowired
	private NewsManagementService newsManagementService;
	
	@Mock
	private CommentService commentService;
	
	@Mock
	private NewsService newsService;
	
	private NewsTO testNews;
	private Long testAuthorId;
	private List<Long> testTagIdList;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		testNews = new NewsTO();
		testNews.setNewsId(1L);
		testNews.setTitle("������������ �������");
		testNews.setShortText("������������ ������� �������� �� �������� ��������� �������");
		testNews.setFullText("������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...");
		testNews.setCreationDate(new Date());
		testNews.setModificationDate(new Date());
		testAuthorId = 1L;
		testTagIdList = new ArrayList<Long>();
		testTagIdList.add(1L);
		testTagIdList.add(3L);
	}
	
	/**
	 * Tests {@link NewsManagementService#addNews(NewsTO, Long, List)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testAddNews() throws ServiceException {
		when(newsService.addNews(testNews)).thenReturn(testNews.getNewsId());
		Long actualNewsId = newsManagementService.addNews(testNews, testAuthorId, testTagIdList);
		assertEquals(testNews.getNewsId(),actualNewsId);
		verify(newsService).addNews(testNews);
		verify(newsService).attachNewsAuthor(testNews.getNewsId(), testAuthorId);
		verify(newsService).attachNewsTags(testNews.getNewsId(), testTagIdList);
		verifyNoMoreInteractions(newsService);
	}
	
	/**
	 * Tests {@link NewsManagementService#addNews(NewsTO, Long, List)} for throwing an exception.
	 * @throws ServiceException
	 */
	@Test(expected = ServiceException.class)
	public void testAddNewsForException() throws ServiceException {
		when(newsService.addNews(testNews)).thenThrow(new ServiceException());
		newsManagementService.addNews(testNews, testAuthorId, testTagIdList);
	}
	
	/**
	 * Tests {@link NewsManagementService#deleteNews(Long)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testUpdateNews() throws ServiceException {
		Long testNewsId = testNews.getNewsId();
		newsManagementService.updateNews(testNews, testAuthorId, testTagIdList);
		verify(newsService).updateNews(testNews);
		verify(newsService).updateNewsAuthor(testNewsId, testAuthorId);
		verify(newsService).deleteNewsTags(testNewsId);
		verify(newsService).attachNewsTags(testNewsId, testTagIdList);
		verifyNoMoreInteractions(newsService);
	}
	
	/**
	 * Tests {@link NewsManagementService#updateNews(NewsTO, Long, List)} for throwing an exception.
	 * @throws ServiceException
	 */
	@Test(expected = ServiceException.class)
	public void testUpdateNewsForException() throws ServiceException {
		doThrow(new ServiceException()).when(newsService).updateNewsAuthor(testNews.getNewsId(), testAuthorId);
		newsManagementService.updateNews(testNews, testAuthorId, testTagIdList);
	}
	
	/**
	 * Tests {@link NewsManagementService#deleteNews(Long)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testDeleteNews() throws ServiceException {
		Long testNewsId = testNews.getNewsId();
		newsManagementService.deleteNews(testNewsId);
		verify(commentService).deleteCommentsByNewsId(testNewsId);
		verify(newsService).deleteNewsAuthor(testNewsId);
		verify(newsService).deleteNewsTags(testNewsId);
		verify(newsService).deleteNews(testNewsId);
		verifyNoMoreInteractions(commentService, newsService);
	}
	
	/**
	 * Tests {@link NewsManagementService#deleteNews(Long)} for throwing an exception.
	 * @throws ServiceException
	 */
	@Test(expected = ServiceException.class)
	public void testDeleteNewsForException() throws ServiceException {
		doThrow(new ServiceException()).when(newsService).deleteNewsAuthor(testNews.getNewsId());;
		newsManagementService.deleteNews(testNews.getNewsId());
	}
	
	/**
	 * Tests {@link NewsManagementService#getNewsPart(Long, Long)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testGetNewsPart() throws ServiceException {
		Long minRow = 1L;
		Long maxRow = 3L;
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(testNews);
		when(newsService.getNewsPart(minRow, maxRow)).thenReturn(newsList);
		newsManagementService.getNewsPart(minRow, maxRow);
		verify(newsService).getNewsPart(minRow, maxRow);
		verify(newsService).getAuthorByNewsId(testNews.getNewsId());
		verify(newsService).getTagListByNewsId(testNews.getNewsId());
		verify(commentService).getCommentsByNewsId(testNews.getNewsId());
		verifyNoMoreInteractions(newsService, commentService);
	}
	
	/**
	 * Tests {@link NewsManagementService#search(SearchCriteria, Long, Long)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testSearchNews() throws ServiceException {
		Long minRow = 1L;
		Long maxRow = 3L;
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(testNews);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(testAuthorId);
		searchCriteria.setTagIdList(testTagIdList);
		when(newsService.search(searchCriteria, minRow, maxRow)).thenReturn(newsList);
		newsManagementService.search(searchCriteria, minRow, maxRow);
		verify(newsService).search(searchCriteria, minRow, maxRow);
		verify(newsService).getAuthorByNewsId(testNews.getNewsId());
		verify(newsService).getTagListByNewsId(testNews.getNewsId());
		verify(commentService).getCommentsByNewsId(testNews.getNewsId());
		verifyNoMoreInteractions(newsService, commentService);
	}
	
	/**
	 * Tests {@link NewsManagementService#getNews(Long)} for positive case.
	 * @throws ServiceException
	 */
	@Test
	public void testGetNews() throws ServiceException {
		when(newsService.getNewsById(testNews.getNewsId())).thenReturn(testNews);
		newsManagementService.getNews(testNews.getNewsId());
		verify(newsService).getNewsById(testNews.getNewsId());
		verify(newsService).getAuthorByNewsId(testNews.getNewsId());
		verify(newsService).getTagListByNewsId(testNews.getNewsId());
		verify(commentService).getCommentsByNewsId(testNews.getNewsId());
		verifyNoMoreInteractions(newsService, commentService);
	}
	
	/**
	 * Tests {@link NewsManagementService#saveNews(NewsTO, Long, List)} for news update.
	 * @throws ServiceException
	 */
	@Test
	public void testSaveNewsForUpdate() throws ServiceException {
		Long testNewsId = testNews.getNewsId();
		newsManagementService.saveNews(testNews, testAuthorId, testTagIdList);
		verify(newsService).updateNews(testNews);
		verify(newsService).updateNewsAuthor(testNewsId, testAuthorId);
		verify(newsService).deleteNewsTags(testNewsId);
		verify(newsService).attachNewsTags(testNewsId, testTagIdList);
		verifyNoMoreInteractions(newsService);
	}
	
	/**
	 * Tests {@link NewsManagementService#saveNews(NewsTO, Long, List)} for news addition.
	 * @throws ServiceException
	 */
	@Test
	public void testSaveNewsForAddition() throws ServiceException {
		Long expectedNewsId = testNews.getNewsId();
		when(newsService.addNews(testNews)).thenReturn(expectedNewsId);
		testNews.setNewsId(null);
		Long actualNewsId = newsManagementService.saveNews(testNews, testAuthorId, testTagIdList);
		assertEquals(expectedNewsId,actualNewsId);
		verify(newsService).addNews(testNews);
		verify(newsService).attachNewsAuthor(expectedNewsId, testAuthorId);
		verify(newsService).attachNewsTags(expectedNewsId, testTagIdList);
		verifyNoMoreInteractions(newsService);
	}
}
