package by.epam.newsapp.dao;

import java.util.List;

import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Interface describes methods for interaction with Comments table.
 */
public interface CommentDAO {
		
	/**
	 * Adds new comment into Comments table and returns id of
	 * added comment.
	 * @param comment CommentTO entity to be added
	 * @return id of added comment
	 * @throws DAOException
	 */
	public Long addComment(CommentTO comment) throws DAOException;
	
	/**
	 * Gets comment from Comments table by id and returns selected
	 * CommentTO entity.
	 * @param commentId id of comment to be selected
	 * @return selected Comment entity
	 * @throws DAOException
	 */
	public CommentTO getCommentById(Long commentId) throws DAOException;
	
	/**
	 * Deletes comment from Comments table by id.
	 * @param commentId id of comment to be deleted
	 * @throws DAOException
	 */
	public void deleteComment(Long commentId) throws DAOException;
	
	/**
	 * Updates comment in Comments table.
	 * @param comment Comment entity to be updated
	 * @throws DAOException
	 */
	public void updateComment(CommentTO comment) throws DAOException;
	
	/**
	 * Gets comments from Comments table by news id.
	 * @param newsId News id of comments to be got
	 * @return CommentTO list
	 * @throws DAOException
	 */
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Deletes comments by news id.
	 * @param newsId News id of comments to be deleted
	 * @throws DAOException
	 */
	public void deleteCommentsByNewsId(Long newsId) throws DAOException;
}
