package by.epam.newsapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.NewsVO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.service.NewsManagementService;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction with {@link AuthorService},  {@link CommentService},
 * {@link NewsService} and {@link TagService} for News Management.
 */
@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService{
	
	private CommentService commentService;
	private NewsService newsService;
	
	public void setCommentService(CommentService commentService){
		this.commentService = commentService;
	}

	public void setNewsService(NewsService newsService){
		this.newsService = newsService;
	}

	/**
	 * @see NewsManagementService#addNews(NewsTO, Long, List)
	 */
	public Long addNews(NewsTO news, Long authorId, List<Long> tagIdList) throws ServiceException{
		Long newsId = newsService.addNews(news);
		newsService.attachNewsAuthor(newsId, authorId);
		if (tagIdList != null){
			newsService.attachNewsTags(newsId, tagIdList);
		}
		return newsId;
	}
	
	/**
	 * @see NewsManagementService#updateNews(NewsTO, Long, List)
	 */
	public void updateNews(NewsTO news, Long authorId, List<Long> tagIdList) throws ServiceException{
		newsService.updateNews(news);
		Long newsId = news.getNewsId();
		newsService.updateNewsAuthor(newsId, authorId);
		if (tagIdList != null){
			newsService.deleteNewsTags(newsId);
			newsService.attachNewsTags(newsId, tagIdList);
		}
	}

	/**
	 * @see NewsManagementService#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		commentService.deleteCommentsByNewsId(newsId);
		newsService.deleteNewsAuthor(newsId);
		newsService.deleteNewsTags(newsId);
		newsService.deleteNews(newsId);
	}

	/**
	 * @see NewsManagementService#getNewsPart(Long, Long)
	 */
	public List<NewsVO> getNewsPart(Long start, Long end) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		List<NewsTO> newsList = newsService.getNewsPart(start, end);
		for(NewsTO news : newsList){
			newsVOList.add(getNewsVOByNews(news));
		}
		return newsVOList;
	}
	
	/**
	 * @see NewsManagementService#search(SearchCriteria, Long, Long)
	 */
	public List<NewsVO> search(SearchCriteria searchCriteria, Long start, Long end) throws ServiceException{
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		List<NewsTO> newsList = newsService.search(searchCriteria, start, end);
		for(NewsTO news : newsList){
			newsVOList.add(getNewsVOByNews(news));
		}
		return newsVOList;
	}
	
	/**
	 * @see NewsManagementService#getNews(Long)
	 */
	public NewsVO getNews(Long newsId) throws ServiceException {
		return getNewsVOByNews(newsService.getNewsById(newsId));
	}
	
	/**
	 * @see NewsManagementService#saveNews(NewsTO, Long, List)
	 */
	public Long saveNews(NewsTO news, Long authorId, List<Long> tagIdList)
			throws ServiceException {
		if (news.getNewsId() == null){
			return addNews(news, authorId, tagIdList);
		} else {
			updateNews(news, authorId, tagIdList);
			return news.getNewsId();
		}
	}
	
	/**
	 * Generates NewsVO from NewsTO.
	 * @param news NewsTO
	 * @return NewsVO
	 * @throws ServiceException
	 */
	private NewsVO getNewsVOByNews(NewsTO news) throws ServiceException{
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(news);
		newsVO.setAuthor(newsService.getAuthorByNewsId(news.getNewsId()));
		newsVO.setTagList(newsService.getTagListByNewsId(news.getNewsId()));
		newsVO.setCommentList(commentService.getCommentsByNewsId(news.getNewsId()));
		return newsVO;
	}

	
	
}
