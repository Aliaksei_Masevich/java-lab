package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.ServiceException;

/**
 * Interface declares methods for interaction with Tag logic.
 */
public interface TagService {
	/**
	 * Gets all tags and returns TagTO entity list
	 * @return TagTO entity list
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Adds tag and returns id of added tag.
	 * @param tag TagTO to be added
	 * @return Id of added tag
	 * @throws ServiceException
	 */
	public Long addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Gets tag by id.
	 * @param tagId Id of tag to be got
	 * @return TagTO entity
	 * @throws ServiceException
	 */
	public TagTO getTagById(Long tagId) throws ServiceException;
	
	/**
	 * Deletes tag by id.
	 * @param tagId Id of tag to be deleted
	 * @throws ServiceException
	 */
	public void deleteTag(Long tagId) throws ServiceException;
	
	/**
	 * Updates tag.
	 * @param tag TagTO entity to be updated.
	 * @throws ServiceException
	 */
	public void updateTag(TagTO tag) throws ServiceException;
}
