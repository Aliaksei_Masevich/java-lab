package by.epam.newsapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class represents Comments database entity.
 */
public class CommentTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long commentId;
	
	@NotNull
	private Long newsId;
	
	@NotNull
	@Size(min=1,max=100)
	@Pattern(regexp="(.|\\t|\\r|\\n)*\\S(.|\\t|\\r|\\n)*")
	private String commentText;
	
	private Date creationDate;
	
	public CommentTO(){}
	
	public Long getCommentId(){
		return commentId;
	}
	
	public void setCommentId(Long commentId){
		this.commentId = commentId;
	}
	
	public Long getNewsId(){
		return newsId;
	}
	
	public void setNewsId(Long newsId){
		this.newsId = newsId;
	}
	
	public String getCommentText(){
		return commentText;
	}
	
	public void setCommentText(String commentText){
		this.commentText = commentText;
	}
	
	public Date getCreationDate(){
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate){
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId
				+ ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}
}
