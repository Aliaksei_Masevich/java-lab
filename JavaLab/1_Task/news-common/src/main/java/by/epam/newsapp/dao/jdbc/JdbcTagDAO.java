package by.epam.newsapp.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.ConnectionCloser;

/**
 * Class is used for interaction with Oracle DB Tag table.
 */
public class JdbcTagDAO implements TagDAO{
	
	private static final String SQL_SELECT_ALL_TAGS =
			"SELECT tag_id, tag_name FROM tag ORDER BY tag_name";
	private static final String SQL_INSERT_TAG =
			"INSERT INTO tag (tag_id, tag_name) VALUES (tag_sequence.nextval, ?)";
	private static final String SQL_SELECT_TAG_BY_ID =
			"SELECT tag_id, tag_name FROM tag WHERE tag_id = ?";
	private static final String SQL_DELETE_TAG =
			"DELETE FROM tag WHERE tag_id = ?";
	private static final String SQL_UPDATE_TAG =
			"UPDATE tag SET tag_name = ? WHERE tag_id = ?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}

	/**
	 * @see TagDAO#getAllTags()
	 */
	public List<TagTO> getAllTags() throws DAOException{
		List<TagTO> tagList = new ArrayList<TagTO>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ALL_TAGS);
			while (resultSet.next()){		
				tagList.add(getTagFromResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcTagDAO.getAllTags() method", e);
		} finally {
			ConnectionCloser.close(resultSet, statement, connection, dataSource);
		}
		return tagList;
	}

	/**
	 * @see TagDAO#addTag(TagTO)
	 */
	public Long addTag(TagTO tag) throws DAOException {
		Long tagId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_TAG, new String[]{"tag_id"});
			preparedStatement.setNString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet != null && resultSet.next()){
				tagId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcTagDAO.addTag(TagTO) method "
					+ "with parameter " + tag, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return tagId;
	}

	/**
	 * @see TagDAO#getTagById(Long)
	 */
	public TagTO getTagById(Long tagId) throws DAOException {
		TagTO tag = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_ID);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				tag = getTagFromResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcTagDAO.getTagById(Long) method "
					+ "with parameter " + tagId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return tag;
	}

	/**
	 * @see TagDAO#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcTagDAO.deleteTag(Long) method "
					+ "with parameter " + tagId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
		
	}

	/**
	 * @see TagDAO#updateTag(TagTO)
	 */
	public void updateTag(TagTO tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setNString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcTagDAO.updateTag(TagTO) method "
					+ "with parameter " + tag, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}

	/**
	 * Creates TagTO object and sets its fields with values from resultSet.
	 * @param resultSet Result Set containing tag information
	 * @return TagTO object
	 * @throws SQLException
	 */
	private TagTO getTagFromResultSet(ResultSet resultSet) throws SQLException{
		TagTO tag = new TagTO();
		tag.setTagId(resultSet.getLong("tag_id"));
		tag.setTagName(resultSet.getString("tag_name"));
		return tag;
	}
	
}
