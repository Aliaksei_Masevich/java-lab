package by.epam.newsapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction with {@link NewsDAO}
 */
public class NewsServiceImpl implements NewsService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsServiceImpl.class);
	
	private NewsDAO newsDAO;
	
	public void setNewsDAO(NewsDAO newsDAO){
		this.newsDAO = newsDAO;
	}

	/**
	 * @see NewsService#getAllNews()
	 */
	public List<NewsTO> getAllNews() throws ServiceException {
		try {
			return newsDAO.getAllNews();
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#addNews(NewsTO)
	 */
	public Long addNews(NewsTO news) throws ServiceException {
		try {
			return newsDAO.addNews(news);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNewsById(Long)
	 */
	public NewsTO getNewsById(Long newsId) throws ServiceException {
		try {
			return newsDAO.getNewsById(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#updateNews(NewsTO)
	 */
	public void updateNews(NewsTO news) throws ServiceException {
		try {
			newsDAO.updateNews(news);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getAuthorIdByNewsId(Long)
	 */
	public Long getAuthorIdByNewsId(Long newsId) throws ServiceException {
		try {
			return newsDAO.getAuthorIdByNewsId(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getTagIdListByNewsId(Long)
	 */
	public List<Long> getTagIdListByNewsId(Long newsId) throws ServiceException {
		try {
			return newsDAO.getTagIdListByNewsId(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#attachNewsAuthor(Long, Long)
	 */
	public void attachNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		try {
			newsDAO.attachNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#attachNewsTags(Long, List)
	 */
	public void attachNewsTags(Long newsId, List<Long> tagIdList)
			throws ServiceException {
		try {
			newsDAO.attachNewsTags(newsId, tagIdList);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#deleteNewsAuthor(Long)
	 */
	public void deleteNewsAuthor(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#deleteNewsTags(Long)
	 */
	public void deleteNewsTags(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteNewsTags(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNewsPart(Long, Long)
	 */
	public List<NewsTO> getNewsPart(Long start, Long end)
			throws ServiceException {
		try {
			return newsDAO.getNewsPart(start, end);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#search(SearchCriteria)
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long minRow, Long maxRow)
			throws ServiceException {
		try {
			return newsDAO.search(searchCriteria, minRow, maxRow);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNewsCount()
	 */
	public Long getNewsCount() throws ServiceException {
		try {
			return newsDAO.getNewsCount();
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getAuthorByNewsId(Long)
	 */
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		try {
			return newsDAO.getAuthorByNewsId(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getTagListByNewsId(Long)
	 */
	public List<TagTO> getTagListByNewsId(Long newsId) throws ServiceException {
		try {
			return newsDAO.getTagListByNewsId(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getSearchedNewsCount(SearchCriteria)
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getSearchedNewsCount(searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#updateNewsAuthor(Long, Long)
	 */
	public void updateNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		try {
			newsDAO.updateNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	/**
	 * @see NewsService#getSearchedNewsNumber(SearchCriteria, Long)
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws ServiceException{
		try {
			return newsDAO.getSearchedNewsNumber(searchCriteria, newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	/**
	 * @see NewsService#detachTagFromNews(Long)
	 */
	public void detachTagFromNews(Long tagId) throws ServiceException{
		try {
			newsDAO.detachTagFromNews(tagId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNextNews(Long, SearchCriteria)
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getNextNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getPreviousNews(Long, SearchCriteria)
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getPreviousNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
