package by.epam.newsapp.dao;

import java.util.List;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Interface describes methods for interaction with News table.
 */
public interface NewsDAO {
	
	/**
	 * Gets all news from News table and returns NewsTO entity
	 * list.
	 * @return NewsTO entity list
	 * @throws DAOException
	 */
	public List<NewsTO> getAllNews() throws DAOException;
	
	/**
	 * Adds new news into News table and returns id of added
	 * news.
	 * @param news NewsTO entity to be added
	 * @return Id of inserted news
	 * @throws DAOException
	 */
	public Long addNews(NewsTO news) throws DAOException;
	
	/**
	 * Gets news from News table by id and returns selected
	 * NewsTO entity.
	 * @param newsId Id of news to be got
	 * @return NewsTO entity
	 * @throws DAOException
	 */
	public NewsTO getNewsById(Long newsId) throws DAOException;
	
	/**
	 * Deletes news from News table by id.
	 * @param newsId Id of news to be deleted
	 * @throws DAOException
	 */
	public void deleteNews(Long newsId) throws DAOException;
	
	/**
	 * Updates news news in News table.
	 * @param news NewsTO entity to be updated
	 * @throws DAOException
	 */
	public void updateNews(NewsTO news) throws DAOException;
	
	/**
	 * Gets news author id by news id.
	 * @param newsId News id 
	 * @return News author id
	 * @throws DAOException
	 */
	public Long getAuthorIdByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Gets author by news id.
	 * @param newsId News id
	 * @return AuthorTO
	 * @throws DAOException
	 */
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Gets tag id list of news by news id.
	 * @param newsId News id
	 * @return News tag id list
	 * @throws DAOException
	 */
	public List<Long> getTagIdListByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Gets tag list by news id.
	 * @param newsId News id
	 * @return TagTO list
	 * @throws DAOException
	 */
	public List<TagTO> getTagListByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Attaches author to news.
	 * @param newsId News id
	 * @param authorId Id of author to be attached
	 * @throws DAOException
	 */
	public void attachNewsAuthor(Long newsId, Long authorId) throws DAOException;
	
	/**
	 * Attaches tags to news.
	 * @param newsId News id
	 * @param tagIdList List of tag id to be attached
	 * @throws DAOException
	 */
	public void attachNewsTags(Long newsId, List<Long> tagIdList) throws DAOException;
	
	/**
	 * Deletes news author reference from News_Author table.
	 * @param newsId News id
	 * @throws DAOException
	 */
	public void deleteNewsAuthor(Long newsId) throws DAOException;
	
	/**
	 * Deletes news tags references from News_Tag table.
	 * @param newsId News id
	 * @throws DAOException
	 */
	public void deleteNewsTags(Long newsId) throws DAOException;
	
	/**
	 * Gets news part starting from start and ending at end from
	 * News table and returns NewsTO entity list. 
	 * @param start Selection start
	 * @param end Selection end
	 * @throws DAOException
	 */
	public List<NewsTO> getNewsPart(Long start, Long end) throws DAOException;
	
	/**
	 * Gets news part according to search criteria.
	 * @param searchCriteria Criteria for search
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsTO list
	 * @throws DAOException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end) throws DAOException;
	
	/**
	 * Gets count of searched news
	 * @param searchCriteria Criteria for search
	 * @return Searched news count
	 * @throws DAOException
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Gets news count.
	 * @return News count
	 * @throws DAOException
	 */
	public Long getNewsCount() throws DAOException;
	
	/**
	 * Updates news author.
	 * @param newsId News id
	 * @param authorId New author id
	 * @throws DAOException
	 */
	public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException;
	
	/**
	 * Gets number of news in searched news list by news id.
	 * @param searchCriteria Criteria for search
	 * @param newsId News id
	 * @return News number in searched news list
	 * @throws DAOException
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws DAOException;
	
	/**
	 * Detaches tag from news.
	 * @param tagId Tag id
	 * @throws DAOException
	 */
	public void detachTagFromNews(Long tagId) throws DAOException;
	
	/**
	 * Gets previous news id by news id in search list.
	 * @param newsId Current news id
	 * @param SearchCriteria Search Criteria
	 * @return Previous news id
	 * @throws DAOException
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Gets next news id by news id in search list.
	 * @param newsId Current news id
	 * @param SearchCriteria Search Criteria
	 * @return Next news id
	 * @throws DAOException
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
}
