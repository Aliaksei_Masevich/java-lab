package by.epam.newsapp.dao;

import java.util.Date;
import java.util.List;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Interface describes methods for interaction with Author table.
 */
public interface AuthorDAO {
	
	/**
	 * Gets all authors from Author table and returns AuthorTO
	 * entity list. 
	 * @return AuthorTO entity list
	 * @throws DAOException
	 */
	public List<AuthorTO> getAllAuthors() throws DAOException;
	
	/**
	 * Adds new author to Author table and returns id of
	 * added author.
	 * @param author AuthorTO entity to be added
	 * @return Id of inserted author
	 * @throws DAOException
	 */
	public Long addAuthor(AuthorTO author) throws DAOException;
	
	/**
	 * Gets author from Author table by id and returns selected
	 * AuthorTO entity.
	 * @param AuthorId id of author to be got
	 * @return Selected Author entity
	 * @throws DAOException
	 */
	public AuthorTO getAuthorById(Long authorId) throws DAOException;
	
	/**
	 * Deletes Author from Author table by id.
	 * @param AuthorId id of author to be deleted
	 * @throws DAOException
	 */
	public void deleteAuthor(Long authorId) throws DAOException;
	
	/**
	 * Updates author in Author table. 
	 * @param Author AuthorTO entity to be updated.
	 * @throws DAOException
	 */
	public void updateAuthor(AuthorTO author) throws DAOException;
	
	
	/**
	 * Sets author's expiration date.
	 * @param AuthorId id of author to be expired
	 * @param expired Expiration date
	 * @throws DAOException
	 */
	public void setExpired(Long authorId, Date expired) throws DAOException;
}
