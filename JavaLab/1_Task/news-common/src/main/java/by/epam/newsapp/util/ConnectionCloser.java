package by.epam.newsapp.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Class is used for closing or releasing result sets, statements and database connections.
 */
public class ConnectionCloser {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionCloser.class);
	/**
	 * Closes result set, statement and database connection.
	 * @param resultSet Result set to be closed
	 * @param statement Statement to be closed
	 * @param connection Connection to be closed
	 */
	public static void close(ResultSet resultSet, Statement statement, Connection connection, DataSource dataSource){
		if (resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e){
				LOGGER.warn(e.getMessage());
			}
		}
		close(statement, connection, dataSource);
	}
	
	/**
	 * Closes statement and database connection.
	 * @param statement Statement to be closed
	 * @param connection Connection to be closed
	 */
	public static void close(Statement statement, Connection connection, DataSource dataSource){
		if (statement != null){
			try {
				statement.close();
			} catch (SQLException e){
				LOGGER.warn(e.getMessage());
			}
		}
		DataSourceUtils.releaseConnection(connection, dataSource);
	}
}
