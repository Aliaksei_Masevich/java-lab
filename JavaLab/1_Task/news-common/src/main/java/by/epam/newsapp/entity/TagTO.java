package by.epam.newsapp.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Class represents Tag database entity.
 */
public class TagTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long tagId;
	
	@NotNull
	@Size(min=1,max=30)
	@Pattern(regexp="(.|\\t|\\r|\\n)*\\S(.|\\t|\\r|\\n)*")
	private String tagName;
	
	public TagTO(){}
	
	public Long getTagId(){
		return tagId;
	}
	
	public void setTagId(Long tagId){
		this.tagId = tagId;
	}
	
	public String getTagName(){
		return tagName;
	}
	
	public void setTagName(String tagName){
		this.tagName = tagName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}
}
