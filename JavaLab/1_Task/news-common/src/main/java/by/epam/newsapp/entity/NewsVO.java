package by.epam.newsapp.entity;

import java.util.List;

/**
 * Class represents News content.
 */
public class NewsVO {
	
	private NewsTO news;
	
	private AuthorTO author;
	
	private List<TagTO> tagList;
	private List<CommentTO> commentList;
	
	public NewsVO(){}
	
	public void setNews(NewsTO news){
		this.news = news;
	}
	
	public NewsTO getNews(){
		return news;
	}
	
	public void setAuthor(AuthorTO author){
		this.author = author;
	}
	
	public AuthorTO getAuthor(){
		return author;
	}
	
	public void setTagList(List<TagTO> tagList){
		this.tagList = tagList;
	}
	
	public List<TagTO> getTagList(){
		return tagList;
	}
	
	public void setCommentList(List<CommentTO> commentList){
		this.commentList = commentList;
	}
	
	public List<CommentTO> getCommentList(){
		return commentList;
	}
	
	public int hashCode(){
		int prime = 31;
		int hash = 1;
		hash = hash*prime + (news != null ? news.hashCode() : 0);
		hash = hash*prime + (author != null ? author.hashCode() : 0);
		hash = hash*prime + (tagList != null ? tagList.hashCode() : 0);
		hash = hash*prime + (commentList != null ? commentList.hashCode() : 0);
		return hash;
	}
	
	public boolean equals(Object o){
		if (this == o){
			return true;
		}
		if (o == null){
			return false;
		}
		if (getClass() != o.getClass()){
			return false;
		}
		NewsVO newsVO = (NewsVO)o;
		if (news == null){
			if (newsVO.news != null){
				return false;
			}
		} else if (!news.equals(newsVO.news)){
			return false;
		}
		if (author == null){
			if (newsVO.author != null){
				return false;
			}
		} else if (!author.equals(newsVO.author)){
			return false;
		}
		if (tagList == null){
			if (newsVO.tagList != null){
				return false;
			}
		} else if (!tagList.equals(newsVO.tagList)){
			return false;
		}
		if (commentList == null){
			if (newsVO.commentList != null){
				return false;
			}
		} else if (!commentList.equals(newsVO.commentList)){
			return false;
		}
		return true;
	}
	
	
}

