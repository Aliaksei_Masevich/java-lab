package by.epam.newsapp.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.ConnectionCloser;

/**
 * Class is used for interaction with Oracle DB Author table.
 */
public class JdbcAuthorDAO implements AuthorDAO{
	
	private final static String SQL_SELECT_ALL_AUTHORS =
			"SELECT author_id, author_name, expired FROM author ORDER BY author_name";
	private final static String SQL_INSERT_AUTHOR =
			"INSERT INTO author (author_id, author_name, expired) "
			+ "VALUES (author_sequence.nextval, ?, ?)";
	private final static String SQL_SELECT_AUTHOR_BY_ID = 
			"SELECT author_id, author_name, expired FROM author "
			+ "WHERE author_id = ?";
	private final static String SQL_DELETE_AUTHOR =
			"DELETE FROM author WHERE author_id = ?";
	private final static String SQL_UPDATE_AUTHOR = 
			"UPDATE author SET author_name = ?, expired = ? WHERE author_id = ?";
	private final static String SQL_UPDATE_EXPIRED =
			"UPDATE author SET expired = ? WHERE author_id = ?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	/**
	 * @see AuthorDAO#getAllAuthors()
	 */
	public List<AuthorTO> getAllAuthors() throws DAOException {
		List<AuthorTO> authors = new ArrayList<AuthorTO>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ALL_AUTHORS);
			while (resultSet.next()){
				authors.add(getAuthorFromResultSet(resultSet));
			}
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.getAllAuthors() method", e);
		} finally {
			ConnectionCloser.close(resultSet, statement, connection, dataSource);
		}
		return authors;
	}
	
	/**
	 * @see AuthorDAO#addAuthor(AuthorTO)
	 */
	public Long addAuthor(AuthorTO author) throws DAOException {
		Long authorId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR, new String[]{"author_id"});
			preparedStatement.setNString(1, author.getAuthorName());
			preparedStatement.setTimestamp(2, author.getExpired() != null ?
					new Timestamp(author.getExpired().getTime()) : null);
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet != null && resultSet.next()){
				authorId = resultSet.getLong(1);
			}
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.getAllAuthors(AuthorTO) "
					+ "method with parameter " + author, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return authorId;
	}
	
	/**
	 * @see AuthorDAO#getAuthorById(Long)
	 */
	public AuthorTO getAuthorById(Long authorId) throws DAOException {
		AuthorTO author = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				author = getAuthorFromResultSet(resultSet);				
			}
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.getAuthorById(Long) method "
					+ "with parameter " + authorId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return author;
	}
	
	/**
	 * @see AuthorDAO#deleteAuthor(Long)
	 */
	public void deleteAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.deleteAuthor(Long) method "
					+ "with parameter " + authorId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
		
	}
	
	/**
	 * @see AuthorDAO#updateAuthor(AuthorTO)
	 */
	public void updateAuthor(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setNString(1, author.getAuthorName());
			preparedStatement.setTimestamp(2, author.getExpired() != null ?
					new Timestamp(author.getExpired().getTime()) : null);
			preparedStatement.setLong(3, author.getAuthorId());
			preparedStatement.executeUpdate();
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.updateAuthor(AuthorTO) method "
					+ "with parameter " + author, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
		
	}
	
	/**
	 * @see AuthorDAO#setExpired(Long, Date)
	 */
	public void setExpired(Long authorId, Date expired) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_EXPIRED);
			preparedStatement.setTimestamp(1, expired != null ? new Timestamp(expired.getTime()) : null);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e){
			throw new DAOException("Exception occurred while calling JdbcAuthorDAO.setExpired(Long, Date) method "
					+ "with parameters " + authorId + ", " + expired, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * Creates AuthorTO object and sets its fields with values from resultSet.
	 * @param resultSet Result Set containing author information
	 * @return AuthorTO object
	 * @throws SQLException
	 */
	private AuthorTO getAuthorFromResultSet(ResultSet resultSet) throws SQLException{
		AuthorTO author = new AuthorTO();
		author.setAuthorId(resultSet.getLong("author_id"));
		author.setAuthorName(resultSet.getString("author_name"));
		author.setExpired(resultSet.getTimestamp("expired"));
		return author;
	}


}
