package by.epam.newsapp.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.ConnectionCloser;

/**
 * Class is used for interaction with Oracle DB Comments table.
 */
public class JdbcCommentDAO implements CommentDAO{
	private static final String SQL_INSERT_COMMENT =
			"INSERT INTO comments (comment_id, news_id, comment_text, creation_date) "
			+ "VALUES (comments_sequence.nextval, ?, ?, ?)";
	private static final String SQL_SELECT_COMMENT_BY_ID = 
			"SELECT comment_id, news_id, comment_text, creation_date FROM comments "
			+ "WHERE comment_id = ?";
	private static final String SQL_DELETE_COMMENT = 
			"DELETE FROM comments WHERE comment_id = ?";
	private static final String SQL_UPDATE_COMMENT = 
			"UPDATE comments SET news_id = ?, comment_text = ?, "
			+ "creation_date = ? WHERE comment_id = ?";
	private static final String SQL_SELECT_COMMENTS_BY_NEWS_ID =
			"SELECT comment_id, news_id, comment_text, creation_date FROM comments "
			+ "WHERE news_id = ? ORDER BY creation_date ASC";
	private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID =
			"DELETE FROM comments WHERE news_id = ?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}

	/**
	 * @see CommentDAO#addComment(CommentTO)
	 */
	public Long addComment(CommentTO comment) throws DAOException{
		Long commentId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_COMMENT, new String[]{"comment_id"});
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setNString(2, comment.getCommentText());
			preparedStatement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet != null && resultSet.next()){
				commentId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.addComment(CommentTO) method "
					+ "with parameter " + comment, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return commentId;
	}

	/**
	 * @see CommentDAO#getCommentById(Long)
	 */
	public CommentTO getCommentById(Long commentId) throws DAOException{
		CommentTO comment = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_ID);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				comment = getCommentFromResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.getCommentById(Long) method "
					+ "with parameter " + commentId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return comment;
	}

	/**
	 * @see CommentDAO#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.deleteComment(Long) method "
					+ "with parameter " + commentId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}

	/**
	 * @see CommentDAO#updateComment(CommentTO)
	 */
	public void updateComment(CommentTO comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setNString(2, comment.getCommentText());
			preparedStatement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()));
			preparedStatement.setLong(4, comment.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.updateComment(CommentTO) method "
					+ "with parameter " + comment, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}		
	}
	
	/**
	 * @see CommentDAO#getCommentsByNewsId(Long)
	 */
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DAOException{
		List<CommentTO> comments = new ArrayList<CommentTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENTS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				comments.add(getCommentFromResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.getCommentsByNewsId(Long) method "
					+ "with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return comments;
	}
	
	/**
	 * @see CommentDAO#deleteCommentsByNewsId(Long)
	 */
	public void deleteCommentsByNewsId(Long newsId) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcCommentDAO.deleteCommentsByNewsId(Long) method "
					+ "with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}

	/**
	 * Creates CommentTO object and sets its fields with values from result set.
	 * @param resultSet Result Set containing comment information
	 * @return CommentTO object
	 * @throws SQLException
	 */
	private CommentTO getCommentFromResultSet(ResultSet resultSet) throws SQLException{
		CommentTO comment = new CommentTO();
		comment.setCommentId(resultSet.getLong("comment_id"));
		comment.setNewsId(resultSet.getLong("news_id"));
		comment.setCommentText(resultSet.getString("comment_text"));
		comment.setCreationDate(resultSet.getTimestamp("creation_date"));
		return comment;
	}
}
