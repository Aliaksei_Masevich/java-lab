package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.ServiceException;

/**
 * Interface declares methods for interaction with Comment logic.
 */
public interface CommentService {
	/**
	 * Adds new comment and returns id of added comment.
	 * @param comment CommentTO entity to be added
	 * @return Id of added comment
	 * @throws ServiceException
	 */
	public Long addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Gets comment by id.
	 * @param commentId Id of comment to be got
	 * @return CommentTO entity
	 * @throws ServiceException
	 */
	public CommentTO getCommentById(Long commentId) throws ServiceException;
	
	/**
	 * Deletes comment by id.
	 * @param commentId Id of comment to be deleted
	 * @throws ServiceException
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	/**
	 * Updates comment.
	 * @param comment CommentTO entity to be updated
	 * @throws ServiceException
	 */
	public void updateComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Gets comments from Comments table by news id.
	 * @param newsId News id of comments to be got
	 * @return CommentTO list
	 * @throws ServiceException
	 */
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Deletes comments by news id.
	 * @param newsId News id of comments to be deleted
	 * @throws ServiceException
	 */
	public void deleteCommentsByNewsId(Long newsId) throws ServiceException;
}
