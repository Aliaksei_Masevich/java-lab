package by.epam.newsapp.dao;

import java.util.List;

import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Interface describes methods for interaction with Tag table.
 */
public interface TagDAO {
	
	/**
	 * Gets all tags from Tag table and returns TagTO entity
	 * list.
	 * @return Tag entity list
	 * @throws DAOException
	 */
	public List<TagTO> getAllTags() throws DAOException;
	
	/**
	 * Adds new tag into Tag table and returns id of added
	 * tag.
	 * @param tag TagTO entity to be inserted
	 * @return Id of added tag
	 * @throws DAOException
	 */
	public Long addTag(TagTO tag) throws DAOException;
	
	/**
	 * Gets tag from Tag table by id and returns selected TagTO
	 * entity.
	 * @param tagId Id of tag to be got
	 * @return selected TagTO entity
	 * @throws DAOException
	 */
	public TagTO getTagById(Long tagId) throws DAOException;
	
	/**
	 * Deletes tag from Tag table by id.
	 * @param tagId Id of tag to be deleted
	 * @throws DAOException
	 */
	public void deleteTag(Long tagId) throws DAOException;
	
	/**
	 * Updates tag in Tag table.
	 * @param tag TagTO entity to be updated
	 * @throws DAOException
	 */
	public void updateTag(TagTO tag) throws DAOException;
}
