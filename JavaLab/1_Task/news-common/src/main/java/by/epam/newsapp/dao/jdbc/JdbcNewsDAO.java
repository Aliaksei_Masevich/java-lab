package by.epam.newsapp.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.ConnectionCloser;
import by.epam.newsapp.util.QueryBuilder;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction with Oracle DB News table.
 */
public class JdbcNewsDAO implements NewsDAO{
	private static final String SQL_SELECT_ALL_NEWS = 
			"SELECT a.news_id, a.title, a.short_text, a.full_text, a.creation_date, "
			+ "a.modification_date, COUNT(b.comment_id) AS comment_count FROM news a "
			+ "LEFT JOIN comments b ON a.news_id = b.news_id GROUP BY a.news_id, a.title, "
			+ "a.short_text, a.full_text, a.creation_date, a.modification_date ORDER BY "
			+ "comment_count DESC, a.modification_date DESC";
	private static final String SQL_INSERT_NEWS = 
			"INSERT INTO news (news_id, title, short_text, full_text, creation_date, "
			+ "modification_date) VALUES (news_sequence.nextval, ?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_NEWS_BY_ID = 
			"SELECT news_id, title, short_text, full_text, creation_date, "
			+ "modification_date FROM news WHERE news_id = ?";
	private static final String SQL_DELETE_NEWS =
			"DELETE FROM news WHERE news_id = ?";
	private static final String SQL_UPDATE_NEWS =
			"UPDATE news SET title = ?, short_text = ?, full_text = ?, "
			+ "creation_date = ?, modification_date = ? WHERE news_id = ?";
	private static final String SQL_SELECT_AUTHOR_ID_BY_NEWS_ID = 
			"SELECT author_id FROM news_author WHERE news_id = ?";
	private static final String SQL_SELECT_AUTHOR_BY_NEWS_ID = 
			"SELECT a.author_id, a.author_name, a.expired FROM author a INNER JOIN news_author b "
			+ "ON a.author_id = b.author_id WHERE b.news_id = ?";
	private static final String SQL_SELECT_TAG_IDS_BY_NEWS_ID =
			"SELECT tag_id FROM news_tag WHERE news_id = ?";
	private static final String SQL_SELECT_TAGS_BY_NEWS_ID =
			"SELECT a.tag_id, a.tag_name FROM tag a INNER JOIN news_tag b ON a.tag_id = b.tag_id "
			+ "WHERE b.news_id = ?";
	private static final String SQL_INSERT_NEWS_AUTHOR_REFERENCE =
			"INSERT INTO news_author (news_id, author_id) VALUES (?, ?)";
	private static final String SQL_INSERT_NEWS_TAG_REFERENCE =
			"INSERT INTO news_tag (news_id, tag_id) VALUES (?, ?)";
	private static final String SQL_DELETE_NEWS_AUTHOR = 
			"DELETE FROM news_author WHERE news_id = ?";
	private static final String SQL_DELETE_NEWS_TAGS =
			"DELETE FROM news_tag WHERE news_id = ?";
	private static final String SQL_DETACH_TAG_FROM_NEWS =
			"DELETE FROM news_tag WHERE tag_id = ?";
	private static final String SQL_SELECT_NEWS_PART =
			"SELECT * FROM (SELECT c.*, ROWNUM rnum FROM (SELECT a.news_id, a.title, a.short_text,"
			+ " a.full_text, a.creation_date, a.modification_date, COUNT(b.comment_id) AS comment_count"
			+ " FROM news a LEFT JOIN comments b ON a.news_id = b.news_id GROUP BY a.news_id, a.title, "
			+ "a.short_text, a.full_text, a.creation_date, a.modification_date ORDER BY comment_count "
			+ "DESC, TO_DATE(a.modification_date) DESC, a.news_id DESC) c WHERE ROWNUM <= ?) WHERE rnum >= ?";
	private static final String SQL_NEWS_COUNT =
			"SELECT COUNT(news_id) AS news_count FROM news";
	private static final String SQL_UPDATE_NEWS_AUTHOR =
			"UPDATE news_author SET author_id = ? WHERE news_id = ?";
	

	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	/**
	 * @see NewsDAO#getAllNews()
	 */
	public List<NewsTO> getAllNews() throws DAOException {
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ALL_NEWS);
			while (resultSet.next()){
				newsList.add(getNewsFromResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getAllNews() method", e);
		} finally {
			ConnectionCloser.close(resultSet, statement, connection, dataSource);
		}
		return newsList;
	}

	/**
	 * @see NewsDAO#addNews(NewsTO)
	 */
	public Long addNews(NewsTO news) throws DAOException {
		Long newsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS, new String[]{"news_id"});
			preparedStatement.setNString(1, news.getTitle());
			preparedStatement.setNString(2, news.getShortText());
			preparedStatement.setNString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			preparedStatement.setTimestamp(5, new Timestamp(news.getModificationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet != null && resultSet.next()){
				newsId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.addNews(NewsTO) "
					+ "method with parameter " + news, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return newsId;
	}

	/**
	 * @see NewsDAO#getNewsById(Long)
	 */
	public NewsTO getNewsById(Long newsId) throws DAOException {
		NewsTO news = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				news = getNewsFromResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getNewsById(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return news;
	}

	/**
	 * @see NewsDAO#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.deleteNews(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}

	/**
	 * @see NewsDAO#updateNews(NewsTO)
	 */
	public void updateNews(NewsTO news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setNString(1, news.getTitle());
			preparedStatement.setNString(2, news.getShortText());
			preparedStatement.setNString(3, news.getFullText());
			preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			preparedStatement.setTimestamp(5, new Timestamp(news.getModificationDate().getTime()));
			preparedStatement.setLong(6, news.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.updateNews(NewsTO) "
					+ "method with parameter " + news, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#getAuthorIdByNewsId(Long)
	 */
	public Long getAuthorIdByNewsId(Long newsId) throws DAOException {
		Long authorId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_ID_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				authorId = resultSet.getLong("author_id");
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getAuthorIdByNewsId(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return authorId;
	}
	
	/**
	 * @see NewsDAO#getAuthorByNewsId(Long)
	 */
	public AuthorTO getAuthorByNewsId(Long newsId) throws DAOException {
		AuthorTO author = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				author = new AuthorTO();
				author.setAuthorId(resultSet.getLong("author_id"));
				author.setAuthorName(resultSet.getString("author_name"));
				author.setExpired(resultSet.getTimestamp("expired"));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getAuthorByNewsId(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return author;
	}
	
	/**
	 * @see NewsDAO#getTagIdListByNewsId(Long)
	 */
	public List<Long> getTagIdListByNewsId(Long newsId) throws DAOException {
		List<Long> tagIdList = new ArrayList<Long>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_IDS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				tagIdList.add(resultSet.getLong("tag_id"));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getTagIdListByNewsId(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return tagIdList;
	}
	
	/**
	 * @see NewsDAO#getTagListByNewsId(Long)
	 */
	public List<TagTO> getTagListByNewsId(Long newsId) throws DAOException {
		List<TagTO> tagList = new ArrayList<TagTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAGS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				TagTO tag = new TagTO();
				tag.setTagId(resultSet.getLong("tag_id"));
				tag.setTagName(resultSet.getString("tag_name"));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getTagListByNewsId(Long) "
					+ "method with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return tagList;
	}
	
	/**
	 * @see NewsDAO#attachNewsAuthor(Long, Long)
	 */
	public void attachNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR_REFERENCE);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.attachNewsAuthor(Long, Long) "
					+ "method with parameters " + newsId + ", " + authorId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#attachNewsTags(Long, List)
	 */
	public void attachNewsTags(Long newsId, List<Long> tagIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS_TAG_REFERENCE);
			for (Long tagId : tagIdList){
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.attachNewsTags(Long, List<Long>) "
					+ "method with parameters " + newsId + ", " + tagIdList, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#deleteNewsAuthor(Long)
	 */
	public void deleteNewsAuthor(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.deleteNewsAuthor(Long) method "
					+ "with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#deleteNewsTags(Long)
	 */
	public void deleteNewsTags(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAGS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.deleteNewsTags(Long) method "
					+ "with parameter " + newsId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#getNewsPart(Long, Long)
	 */
	public List<NewsTO> getNewsPart(Long start, Long end) throws DAOException{
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_PART);
			preparedStatement.setLong(1, end);
			preparedStatement.setLong(2, start);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				newsList.add(getNewsFromResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getNewsPart(Long, Long) method "
					+ "with parameters " + start + ", " + end, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return newsList;
	}
	
	/**
	 * @see NewsDAO#search(SearchCriteria, Long, Long)
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end) throws DAOException{
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = getPreparedStatementForSearch(connection, searchCriteria, start, end);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				newsList.add(getNewsFromResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.search(SearchCriteria, Long, Long) "
					+ "method with parameters " + searchCriteria + ", " + start + ", " + end, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return newsList;
	}
	
	/**
	 * @see NewsDAO#getNewsCount()
	 */
	public Long getNewsCount() throws DAOException{
		Long newsCount = 0L;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_NEWS_COUNT);
			if (resultSet.next()){
				newsCount = resultSet.getLong("news_count"); 
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getNewsCount() method", e);
		} finally {
			ConnectionCloser.close(resultSet, statement, connection, dataSource);
		}
		return newsCount;
	}
	
	/**
	 * @see NewsDAO#getSearchedNewsCount(SearchCriteria)
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria) throws DAOException {
		Long newsCount = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = getPreparedStatementForSearchCount(connection, searchCriteria);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				newsCount = resultSet.getLong("news_count");
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getSearchedNewsCount(SearchCriteria) "
					+ "method with parameter " + searchCriteria, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return newsCount;
	}
	
	/**
	 * @see NewsDAO#updateNewsAuthor(Long, Long)
	 */
	public void updateNewsAuthor(Long newsId, Long authorId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.setLong(2, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.updateNewsAuthor(Long, Long) method "
					+ "with parameters " + newsId + ", " + authorId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}	
	}
	
	/**
	 * @see NewsDAO#getSearchedNewsNumber(SearchCriteria, Long)
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Long newsNumber = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = getPreparedStatementForSearchedNewsNumber(connection, searchCriteria, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				newsNumber = resultSet.getLong("news_number");
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getSearchedNewsNumber(SearchCriteria, Long) "
					+ "method with parameters " + searchCriteria + ", " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return newsNumber;
	}
	
	/**
	 * @see NewsDAO#detachTagFromNews(Long)
	 */
	public void detachTagFromNews(Long tagId) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DETACH_TAG_FROM_NEWS);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Exception occured while calling JdbcNewsDAO.detachTagFromNews(Long) method with "
					+ "parameter " + tagId, e);
		} finally {
			ConnectionCloser.close(preparedStatement, connection, dataSource);
		}
	}
	
	/**
	 * @see NewsDAO#getPreviousNewsId(Long)
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException{
		Long previousNewsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = getPreparedStatementForPreviousNewsSearch(connection, searchCriteria, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				previousNewsId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getPreviousNews(Long, SearchCriteria) "
					+ "method with parameters " + searchCriteria + ", " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return previousNewsId;
	}
	
	/**
	 * @see NewsDAO#getNextNewsId(Long)
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException{
		Long nextNewsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = getPreparedStatementForNextNewsSearch(connection, searchCriteria, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				nextNewsId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException("Exception occurred while calling JdbcNewsDAO.getNextNews(Long, SearchCriteria) "
					+ "method with parameters " + searchCriteria + ", " + newsId, e);
		} finally {
			ConnectionCloser.close(resultSet, preparedStatement, connection, dataSource);
		}
		return nextNewsId;
	}
	
	/**
	 * Creates new NewsTO object and sets its fields with values from result set.
	 * @param resultSet Result set
	 * @return NewsTO object
	 * @throws SQLException
	 */
	private NewsTO getNewsFromResultSet(ResultSet resultSet) throws SQLException{
		NewsTO news = new NewsTO();
		news.setNewsId(resultSet.getLong("news_id"));
		news.setTitle(resultSet.getString("title"));
		news.setShortText(resultSet.getString("short_text"));
		news.setFullText(resultSet.getString("full_text"));
		news.setCreationDate(resultSet.getTimestamp("creation_date"));
		news.setModificationDate(resultSet.getTimestamp("modification_date"));
		return news;
	}
	
	/**
	 * Gets prepared statement for news search.
	 * @param connection Connection
	 * @param searchCriteria Search criteria
	 * @param start Selection start
	 * @param end Selection end
	 * @return Prepared statement for search
	 * @throws SQLException
	 */
	private PreparedStatement getPreparedStatementForSearch(Connection connection, 
			SearchCriteria searchCriteria, Long start, Long end) throws SQLException{
		PreparedStatement preparedStatement = connection.prepareStatement(
				QueryBuilder.buildSearchNewsPartQuery(searchCriteria));
		int parameterIndex = setPreparedStatementWithSearchCriteria(searchCriteria, preparedStatement, 1);
		preparedStatement.setLong(parameterIndex++, end);
		preparedStatement.setLong(parameterIndex, start);
		return preparedStatement;
	}
	
	/**
	 * Gets prepared statement for getting searched news count.
	 * @param connection Connection
	 * @param searchCriteria SearchCriteria
	 * @return Prepared statement for getting searched news count
	 * @throws SQLException
	 */
	private PreparedStatement getPreparedStatementForSearchCount(Connection connection, 
			SearchCriteria searchCriteria) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(
				QueryBuilder.buildSearchedNewsCountQuery(searchCriteria));
		setPreparedStatementWithSearchCriteria(searchCriteria, preparedStatement, 1);
		return preparedStatement;
	}
	
	/**
	 * Gets prepared statement for getting news number in searched news list.
	 * @param connection Connection
	 * @param searchCriteria Search criteria
	 * @param newsId News id
	 * @return Prepared statement for getting searched news number
	 * @throws SQLException
	 */
	private PreparedStatement getPreparedStatementForSearchedNewsNumber(Connection connection,
			SearchCriteria searchCriteria, Long newsId) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(
				QueryBuilder.buildSearchedNewsNumberQuery(searchCriteria));
		int parameterIndex = setPreparedStatementWithSearchCriteria(searchCriteria, preparedStatement, 1);
		preparedStatement.setLong(parameterIndex, newsId);
		return preparedStatement;
	}
	
	/**
	 * Gets prepared statement for getting previous news id in searched news list.
	 * @param connection Connection
	 * @param searchCriteria Search criteria
	 * @param newsId News id
	 * @return Prepared statement for getting previous news id
	 * @throws SQLException
	 */
	private PreparedStatement getPreparedStatementForPreviousNewsSearch(Connection connection,
			SearchCriteria searchCriteria, Long newsId) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(
				QueryBuilder.buildGetPreviousNewsQuery(searchCriteria));
		int parameterIndex = 1;
		preparedStatement.setLong(parameterIndex++, newsId);
		parameterIndex = setPreparedStatementWithSearchCriteria(searchCriteria, preparedStatement, parameterIndex);
		preparedStatement.setLong(parameterIndex, newsId);
		return preparedStatement;
	}
	
	/**
	 * Gets prepared statement for getting next news id in searched news list.
	 * @param connection Connection
	 * @param searchCriteria Search criteria
	 * @param newsId News id
	 * @return Prepared statement for getting next news id
	 * @throws SQLException
	 */
	private PreparedStatement getPreparedStatementForNextNewsSearch(Connection connection,
			SearchCriteria searchCriteria, Long newsId) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(
				QueryBuilder.buildGetNextNewsQuery(searchCriteria));
		int parameterIndex = 1;
		preparedStatement.setLong(parameterIndex++, newsId);
		parameterIndex = setPreparedStatementWithSearchCriteria(searchCriteria, preparedStatement, parameterIndex);
		preparedStatement.setLong(parameterIndex, newsId);
		return preparedStatement;
	}
	
	/**
	 * Sets PreparedStatement with values from search criteria.
	 * @param searchCriteria Search criteria
	 * @param preparedStatement Prepared statement to be set
	 * @param parameterIndex Current parameter index
	 * @return Last parameter index
	 * @throws SQLException
	 */
	private int setPreparedStatementWithSearchCriteria(SearchCriteria searchCriteria, 
			PreparedStatement preparedStatement, int parameterIndex) throws SQLException{
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null) {
			preparedStatement.setLong(parameterIndex++, authorId);
		}
		if (tagIdList != null && !tagIdList.isEmpty()){
			for (Long tagId : tagIdList){
				preparedStatement.setLong(parameterIndex++, tagId);
			}
		}
		return parameterIndex;
	}
}
