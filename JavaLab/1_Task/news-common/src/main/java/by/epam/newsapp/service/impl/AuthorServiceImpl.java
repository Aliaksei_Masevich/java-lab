package by.epam.newsapp.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.AuthorService;

/**
 * Class is used for interaction with {@link AuthorDAO}
 */
public class AuthorServiceImpl implements AuthorService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorServiceImpl.class);
	
	private AuthorDAO authorDAO;
	
	public void setAuthorDAO(AuthorDAO authorDAO){
		this.authorDAO = authorDAO;
	}

	/**
	 * @see AuthorService#getAllAuthors()
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		try {
			return authorDAO.getAllAuthors();
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see AuthorService#addAuthor(AuthorTO)
	 */
	public Long addAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorDAO.addAuthor(author);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see AuthorService#getAuthorById(Long)
	 */
	public AuthorTO getAuthorById(Long authorId) throws ServiceException {
		try {
			return authorDAO.getAuthorById(authorId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see AuthorService#updateAuthor(AuthorTO)
	 */
	public void updateAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDAO.updateAuthor(author);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see AuthorService#deleteAuthor(Long)
	 */
	public void deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDAO.deleteAuthor(authorId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see AuthorService#setExpired(Long, Date)
	 */
	public void setExpired(Long authorId, Date expired) throws ServiceException {
		try {
			authorDAO.setExpired(authorId, expired);;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
}
