package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Interface declares methods for interaction with News logic.
 */
public interface NewsService {
	
	/**
	 * Gets all news and returns NewsTO entity list.
	 * @return NewsTO entity list
	 * @throws ServiceException
	 */
	public List<NewsTO> getAllNews() throws ServiceException;
	
	/**
	 * Adds news and returns id of added news.
	 * @param news NewsTO to be added
	 * @return Id of added news
	 * @throws ServiceException
	 */
	public Long addNews(NewsTO news) throws ServiceException;
	
	/**
	 * Gets news by id.
	 * @param newsId Id of news to be got
	 * @return NewsTO entity
	 * @throws ServiceException
	 */
	public NewsTO getNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Deletes news by id.
	 * @param newsId Id of news to be deleted
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Updates news.
	 * @param news NewsTO entity to be updated
	 * @throws ServiceException
	 */
	public void updateNews(NewsTO news) throws ServiceException;
	
	/**
	 * Gets news author id by news id.
	 * @param newsId News id
	 * @return News author id
	 * @throws ServiceException
	 */
	public Long getAuthorIdByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets news tag id list by news id.
	 * @param newsId News id
	 * @return News tag id list
	 * @throws ServiceException
	 */
	public List<Long> getTagIdListByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Attaches author to news.
	 * @param newsId News id
	 * @param authorId Id of author to be attached
	 * @throws ServiceException
	 */
	public void attachNewsAuthor(Long newsId, Long authorId) throws ServiceException;
	
	/**
	 * Attaches tags to news.
	 * @param newsId News id
	 * @param tagIdList List of tag id to be attached
	 * @throws DAOException
	 */
	public void attachNewsTags(Long newsId, List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Deletes news author reference.
	 * @param newsId News id
	 * @throws ServiceException
	 */
	public void deleteNewsAuthor(Long newsId) throws ServiceException;
	
	/**
	 * Deletes news tag references.
	 * @param newsId News id
	 * @throws ServiceException
	 */
	public void deleteNewsTags(Long newsId) throws ServiceException;
	
	/**
	 * Gets news part starting from start and ending at end.
	 * @param start Selection start
	 * @param end Selection end
	 * @return
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsPart(Long start, Long end) throws ServiceException;
	
	/**
	 * Gets news part according to search criteria.
	 * @param searchCriteria Criteria for search
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsTO list
	 * @throws ServiceException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end) throws ServiceException;
	
	/**
	 * Gets news count.
	 * @return News count
	 * @throws ServiceException
	 */
	public Long getNewsCount() throws ServiceException;
	
	/**
	 * Gets author by news id.
	 * @param newsId News id
	 * @return AuthorTO
	 * @throws ServiceException
	 */
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets tag list by news id.
	 * @param newsId News id
	 * @return TagTO list
	 * @throws ServiceException
	 */
	public List<TagTO> getTagListByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets count of searched news
	 * @param searchCriteria Criteria for search
	 * @return Searched news count
	 * @throws ServiceException
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Updates news author.
	 * @param newsId News id
	 * @param authorId New author id
	 * @throws ServiceException
	 */
	public void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException;
	
	/**
	 * Gets number of news in searched news list.
	 * @param searchCriteria Criteria for search
	 * @param newsId News id
	 * @return News number in searched news list
	 * @throws ServiceException
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
	
	/**
	 * Detaches tag from news.
	 * @param tagId Tag id
	 * @throws ServiceException
	 */
	public void detachTagFromNews(Long tagId) throws ServiceException;
	
	/**
	 * Gets next news id by news id in search list.
	 * @param newsId News id
	 * @param searchCriteria Search criteria
	 * @return Next news id
	 * @throws ServiceException
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets previous news id by news id in search list.
	 * @param newsId News id
	 * @param searchCriteria Search criteria
	 * @return Previous news id
	 * @throws ServiceException
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException;
}
