package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.NewsVO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Interface declares methods for interaction with News Management logic.
 */
public interface NewsManagementService {
	/**
	 * Adds news with author and tag list.
	 * @param news News to be added
	 * @param authorId News author id
	 * @param tagIdList News tag id list
	 */
	public Long addNews(NewsTO news, Long authorId, List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Updates news with author and tag list.
	 * @param news NewsTO to be updated
	 * @param authorId Author id
	 * @param tagIdList Tag id list
	 * @throws ServiceException
	 */
	public void updateNews(NewsTO news, Long authorId, List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Deletes news.
	 * @param newsId Id of news to be deleted
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Gets news part starting from minRow and ending at maxRow.
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsVO list
	 * @throws ServiceException
	 */
	public List<NewsVO> getNewsPart(Long start, Long end) throws ServiceException;
	
	/**
	 * Gets a part starting from minRow and ending at maxRow from news searched by search criteria.
	 * @param searchCriteria Criteria for search
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsVO list
	 * @throws ServiceException
	 */
	public List<NewsVO> search(SearchCriteria searchCriteria, Long start, Long end) throws ServiceException;
	
	/**
	 * Gets news by news id.
	 * @param newsId News id
	 * @return NewsVO
	 * @throws ServiceException
	 */
	public NewsVO getNews(Long newsId) throws ServiceException;
	
	/**
	 * Adds or updates news with author and tag list.
	 * @param news NewsTO to be saved
	 * @param authorId Author id
	 * @param tagIdList Tag id list
	 * @return NewsTO id
	 * @throws ServiceException
	 */
	public Long saveNews(NewsTO news, Long authorId, List<Long> tagIdList) throws ServiceException;
}
