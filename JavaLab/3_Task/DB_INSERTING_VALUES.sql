DECLARE
 news_count NUMBER := 10000;
 author_count NUMBER := 100;
 tag_count NUMBER := 100;
 max_tag_count NUMBER := 5;
 max_comment_count NUMBER := 3;
 tags_per_news NUMBER;
 comments_per_news NUMBER;
BEGIN
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date, news_version)
SELECT       LEVEL news_id,
             DBMS_RANDOM.string ('P', DBMS_RANDOM.value(10,15)) title,
             DBMS_RANDOM.string ('P', DBMS_RANDOM.value(20,40)) short_text,
             DBMS_RANDOM.string ('P', DBMS_RANDOM.value(20,40)) full_text,
             TO_TIMESTAMP('2015-01-01 00:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF')
             + TRUNC (DBMS_RANDOM.VALUE (150, 200), 0)
               creation_date,
             TO_DATE ('2015-07-21', 'yyyy-mm-dd')
             + TRUNC (DBMS_RANDOM.VALUE (1, 50), 0)
               modification_date,
             '0' news_version
FROM         DUAL
CONNECT BY   LEVEL <= news_count;

INSERT INTO tag (tag_id, tag_name)
SELECT       LEVEL tag_id,
             DBMS_RANDOM.string ('L', DBMS_RANDOM.value(5,10)) tag_name
FROM         DUAL
CONNECT BY   LEVEL <= tag_count;

INSERT INTO author (author_id, author_name, expired)
SELECT       LEVEL author_id,
             DBMS_RANDOM.string ('U', DBMS_RANDOM.value(10,20)) author_name,
             NULL expired
FROM         DUAL
CONNECT BY   LEVEL <= author_count;

FOR i IN 1..news_count LOOP
  INSERT INTO news_author (news_id, author_id) VALUES (i, TRUNC (DBMS_RANDOM.VALUE (1, author_count), 0));
  tags_per_news := TRUNC (DBMS_RANDOM.VALUE (1, max_tag_count), 0);
  FOR k IN 1..tags_per_news LOOP
    INSERT INTO news_tag (news_id, tag_id) VALUES (i, TRUNC (DBMS_RANDOM.VALUE (1, tag_count), 0));
  END LOOP;
  comments_per_news := TRUNC (DBMS_RANDOM.VALUE (1, max_comment_count), 0);
  FOR k IN 1..comments_per_news LOOP
    INSERT INTO comments (comment_id, news_id, comment_text, creation_date)
    VALUES (comments_sequence.nextval, i, 
            DBMS_RANDOM.string ('P', DBMS_RANDOM.value(20,40)),  
            TO_TIMESTAMP('2015-07-21 00:00:00.000000000', 'YYYY-MM-DD HH24:MI:SS.FF')
             + TRUNC (DBMS_RANDOM.VALUE (1, 100), 0));
  END LOOP;
END LOOP;

INSERT INTO "User" VALUES (User_Sequence.nextval, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');
INSERT INTO "User" VALUES (User_Sequence.nextval, 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee');
INSERT INTO "User" VALUES (User_Sequence.nextval, 'admin1', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda');

INSERT INTO Roles VALUES (1, 'ROLE_ADMIN');
INSERT INTO Roles VALUES (2, 'ROLE_USER');
INSERT INTO Roles VALUES (3, 'ROLE_ADMIN');

END;