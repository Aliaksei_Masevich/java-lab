<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<fmt:setLocale value="${requestScope.response.locale}" />

<div class="b-content__container">
	<div class="b-content">
		<div class="b-filer__container">
			<c:url  var="filterAction" value="/newslist"/>
			<sf:form method="POST" action="${filterAction}" commandName="searchCriteria">
				<div class="b-filter">
					<s:message  var="authorPlaceholder" code="newslist.select.author"/>
					<sf:select class="b-authorSelect" path="authorId">
						<option value="" selected>${authorPlaceholder}</option>
						<c:forEach var="author" items="${authorList}">
							<sf:option value="${author.authorId}">
								<c:out value="${author.authorName}"/>
							</sf:option>
						</c:forEach>
					</sf:select>
					<s:message var="tagPlaceholder" code="newslist.select.tags"/>
					<sf:select class="b-tagListSelect" multiple="multiple" path="tagIdList">
						<option value="" selected disabled>${tagPlaceholder}</option>
						<c:forEach var="tag" items="${tagList}">
							<sf:option value="${tag.tagId}" >
								<c:out value="${tag.tagName}"/>
							</sf:option>
						</c:forEach>
					</sf:select>
				</div>
				<s:message var="filterButton" code="newslist.button.filter"/>
				<input class="b-filter__button" type="submit" value="${filterButton}" />
				<c:url var="resetUrl" value="/newslist/reset"/>
				<s:message var="resetButton" code="newslist.button.reset"/>
				<input class="b-filter__button" form="resetForm" type="button" 
					value="${resetButton}" onclick="location.href='${resetUrl}';"/>
			</sf:form>			
		</div>
		<div class="b-news__container">
			<div class="b-content__news">
				<c:forEach var="newsVO" items="${newsVOList}">
					<div class="b-news__item">
						<c:url var="newsViewUrl" value="/newsview/${newsVO.newsId}"/>
						<div class="b-news__title">
							<c:out value="${newsVO.title}"/>
						</div>
						<div class="b-news__author">
							<c:forEach var="author" items="${newsVO.authorList}">
								<s:message code="newslist.label.author" />
								<c:out value="${author.authorName}"/>
								<s:message code="newslist.label.authorend"/>
							</c:forEach>
						</div>
						<div class="b-news__date">
							<fmt:formatDate dateStyle="short" value="${newsVO.modificationDate}" />
						</div>
						<div class="b-news__shorttext">
							<c:out value="${newsVO.shortText}"/>
						</div>
						<div class="b-news__tags">
							<c:forEach var="tag" items="${newsVO.tagList}">
								<c:out value="${tag.tagName}"/>
							</c:forEach>
						</div>
						<div class="b-news__comments">
							<s:message code="newslist.label.comments" />
							<c:out value="${newsVO.commentCountHolder.commentCount}"/>
							<s:message code="newslist.label.commentsend"/>
						</div>
						<div class="b-news__view__button">
							<c:url var="viewNewsUrl" value="/newsview/${newsVO.newsId}"/>
							<a href="${viewNewsUrl}">
								<s:message code="newslist.reference.view"/>
							</a>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="b-pagination">
			<div class="b-pagination__container">
				<c:forEach var="i" begin="${startPage}" end="${endPage}">
					<c:choose>
						<c:when test="${i ne currentPage}">
							<c:url var="pageUrl" value="/newslist?currentPage=${i}"/>
							<a href="${pageUrl}" class="b-page">
								<c:out value="${i}"/>
							</a>
						</c:when>
						<c:otherwise>
							<span class="b-page b-page__active">
								<c:out value="${i}"/>
							</span>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
