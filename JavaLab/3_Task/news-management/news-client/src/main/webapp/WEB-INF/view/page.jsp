<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tex" uri="http://tiles.apache.org/tags-tiles-extras"%>

<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<tex:useAttribute name="cssList"/>
		<c:forEach var="cssStyle" items="${cssList}">
			<c:url var="cssUrl" value="${cssStyle}"/>
			<link href="${cssUrl}" rel="stylesheet">
		</c:forEach>
		<tex:useAttribute name="jsList"/>
		<c:forEach var="jsFile" items="${jsList}">
			<c:url var="jsUrl" value="${jsFile}"/>
			<script type="text/javascript" src="${jsUrl}"></script>
		</c:forEach>
		<title><t:getAsString name="title" /></title>
	</head>
	<body>
		<div class="b-body__container">
			<t:insertAttribute name="header" />
			<t:insertAttribute name="body" />
			<t:insertAttribute name="footer" />
		</div>
	</body>
</html>