<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<fmt:setLocale value="${requestScope.response.locale}" />

<div class="b-content__container">
	<div class="b-content">
		<c:url  var="goBackUrl" value="/newslist/back?newsId=${newsVO.newsId}"/>
		<div class="b-newsview__navigation__back">
			<a href="${goBackUrl}">
				<s:message code="newsview.reference.back"/>
			</a>
		</div>
		<div class="b-newsview__news__container">
			<div class="b-newsview__news__title">
				<c:out value="${newsVO.title}"/>
			</div>
			<div class="b-newsview__news__author">
				<c:forEach var="author" items="${newsVO.authorList}">
					<s:message code="newsview.label.author" />
					<c:out value="${author.authorName}"/>
					<s:message code="newsview.label.authorend"/>
				</c:forEach>
			</div>
			<div class="b-newsview__news__date">
				<fmt:formatDate dateStyle="short" value="${newsVO.modificationDate}"/>
			</div>
			<div class="b-newsview__news__fulltext">
				<c:out value="${newsVO.fullText}"/>
			</div>
			<div class="b-newsview__news__comment__container">
				<c:url var="deleteComment" value="/newsview/deletecomment"/>
				<c:forEach var="comment" items="${newsVO.commentList}">
					<div class="b-newsview__news__comment__item">
						<div class="b-newsview__news__comment__date">
							<fmt:formatDate dateStyle="short" value="${comment.creationDate}" />
						</div>
						<div class="b-newsview__news__comment__body">
							<div class="b-newsview__news__comment__text">
								<c:out value="${comment.commentText}"/>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		<c:url var="postComment" value="/newsview/postcomment" />
		<div class="b-newview__postform">
			<sf:form method="POST" action="${postComment}" commandName="commentTO">
				<sf:textarea class="b-newsview__news__comment__textarea" path="commentText"></sf:textarea>
				<s:message var="postButton" code="newsview.button.post"/>
				<sf:input type="hidden" path="newsId" value="${newsVO.newsId}" />
				<div class="b-newsview__news__comment__post">
					<input type="submit" value="${postButton}">
				</div>
				<sf:errors path="*" cssClass="b-errorblock" element="div" />
			</sf:form>
		</div>
		<div class="b-newsvview__navigation__previous">
			<c:if test="${newsVO.newsId ne previousNewsId}">
				<c:url var="previousNewsUrl" value="/newsview/${previousNewsId}" />
				<a href="${previousNewsUrl}">
					<s:message code="newsview.reference.previous" />
				</a>
			</c:if>
		</div>
		<div class="b-newsvview__navigation__next">
			<c:if test="${newsVO.newsId ne nextNewsId}">
				<c:url var="nextNewsUrl" value="/newsview/${nextNewsId}" />
				<a href="${nextNewsUrl}">
					<s:message code="newsview.reference.next" />
				</a>
			</c:if>
		</div>
	</div>
</div>
