function checkDeleteForm(form, confirmMessage, noItemSelectedMessage) {
		if (form.newsId == null){
			alert(noItemSelectedMessage);
			return false;
		}
		for (var i=0; i<form.newsId.length; i++){
			if(form.newsId[i].checked){
				return confirm(confirmMessage);
			}
		}
		if (form.newsId.checked){
			return confirm(confirmMessage);
		}
		alert(noItemSelectedMessage);
		return false;
}