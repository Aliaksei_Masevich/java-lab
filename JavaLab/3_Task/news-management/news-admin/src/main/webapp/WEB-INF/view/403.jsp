<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>

<div class="b-accessdenied__container">
	<div class="b-accessdenied__title">
		<s:message code="403.label.accessdenied"/>
	</div>
	<div class="b-accessdenied__message">
		<s:message code="403.label.nopermission"/>
	</div>
	<c:url var="backToLoginUrl" value="/login"/>
	<div class="b-accessdenied__login">
		<a href="${backToLoginUrl}">
			<s:message code="403.reference.back"/>
		</a>
	</div>
</div>