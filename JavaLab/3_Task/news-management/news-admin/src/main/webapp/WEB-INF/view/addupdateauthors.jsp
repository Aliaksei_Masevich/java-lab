<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<div class="b-content__container">
	<div class="b-content">
		<s:message var="editButton" code="addupdateauthors.button.edit"/>
		<s:message var="updateButton" code="addupdateauthors.button.update"/>
		<s:message var="expireButton" code="addupdateauthors.button.expire"/>
		<s:message var="cancelButton" code="addupdateauthors.button.cancel"/>
		<c:url var="updateAuthorUrl" value="/addupdateauthors/updateauthor"/>
		<c:url var="expireAuthorUrl" value="/addupdateauthors/expireauthor/"/>
		<div class="b-addupdateauthors__container">
			<sf:form commandName="authorTO">
				<sf:errors path="authorName" cssClass="b-author__container_error" element="div"/>
			</sf:form>
			<c:forEach var="author" items="${authorList}">
				<div class="b-author__container">
					<sf:form method="POST" action="${updateAuthorUrl}" commandName="authorTO">
						<sf:input type="hidden" path="authorId" value="${author.authorId}"/>
						<label class="b-author__label">
							<s:message code="addupdateauthors.label.author"/>
						</label>
						<sf:input class="b-author__input" type="text" path="authorName" 
							value="${author.authorName}" disabled="true" maxlength="30"/> 
						<input type="hidden" name="hiddenName" value="${author.authorName}"/>
						<sf:input type="hidden" path="expired" value="${author.expired}"/>
						<input class="b-button__reference" type="button" name="updateButton" 
							value="${editButton}" onclick="startEdit(this.form)" />
						<input class="b-button__reference" type="hidden" name="saveButton" value="${updateButton}"/> 
						<c:if test="${empty author.expired}">
							<input class="b-button__reference" type="hidden" value="${expireButton}" 
							name="deleteButton" onclick="location.href='${expireAuthorUrl}${author.authorId}';"/>
						</c:if>
						<input class="b-button__reference" type="hidden" name="cancelButton" 
							value="${cancelButton}" onclick="cancelEdit(this.form)"/> 
					</sf:form>
				</div>
			</c:forEach>
			<c:url  var="addAuthorUrl" value="/addupdateauthors/addauthor"/>
			<sf:form method="POST" commandName="authorTO" action="${addAuthorUrl}">
				<div class="b-author__container">
					<label class="b-author__label">
						<s:message code="addupdateauthors.label.addauthor"/>
					</label>
					<sf:input class="b-author__input" type="text" path="authorName" 
						required="true" maxlength="30"/> 
					<s:message  var="saveButton" code="addupdateauthors.button.save"/>
					<input class="b-button__reference" type="submit" value="${saveButton}"/>
				</div>
			</sf:form>
		</div>
	</div>
</div>