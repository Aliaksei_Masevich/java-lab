<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<div class="b-login__container">
	<c:url var="loginUrl" value="/login"/>
	<s:message var="loginButton" code="login.button.login"/>
	<form action="${loginUrl}" method="POST">
		<div class="b-login">
			<div class="b-login__field">
				<label class="b-login__label" for="username">
					<s:message code="login.label.login"/>
				</label>
				<input id="username" type="text" name="username" />
			</div>
			<div class="b-login__field">
				<label class="b-login__label" for="password">
					<s:message code="login.label.password" />
				</label>
				<input id="password" type="password" name="password" />
			</div>
			<input class="b-login__button" name="submit" type="submit" value="${loginButton}" />
			<c:if test="${error}">
				<div class="b-errorblock">
					<s:message code="login.label.error"/>
				</div>
			</c:if>
		</div>
	</form>
</div>