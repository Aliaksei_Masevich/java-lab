<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<div class="b-content__container">
	<div class="b-content">
		<s:message var="editButton" code="addupdatetags.button.edit"/>
		<s:message var="updateButton" code="addupdatetags.button.update"/>
		<s:message var="deleteButton" code="addupdatetags.button.delete"/>
		<s:message var="cancelButton" code="addupdatetags.button.cancel"/>
		<s:message var="confirmMessage" code="addupdatetags.confirm.delete"/>
		<c:url var="updateTagUrl" value="/addupdatetags/updatetag"/>
		<c:url var="deleteTagUrl" value="/addupdatetags/deletetag"/>
		<div class="b-addupdatetags__container">
			<sf:form commandName="tagTO">
				<sf:errors path="tagName" cssClass="b-tag__container_error" element="div"/>
			</sf:form>
			<c:forEach var="tag" items="${tagList}">
				<div class="b-tag__container">
					<sf:form method="POST" commandName="tagTO">
						<sf:input type="hidden" path="tagId" value="${tag.tagId}"/>
						<label class="b-tag__label">
							<s:message code="addupdatetags.label.tag"/>
						</label>
						<sf:input class="b-tag__input" type="text" path="tagName" 
							value="${tag.tagName}" disabled="true" maxlength="30"/> 
						<input type="hidden" name="hiddenName" value="${tag.tagName}"/>
						<input class="b-button__reference" type="button" name="updateButton" 
							value="${editButton}" onclick="startEdit(this.form)" />
						<input class="b-button__reference" type="hidden" name="saveButton" 
							value="${updateButton}" onclick="this.form.action='${updateTagUrl}'"/> 
						<input class="b-button__reference" type="hidden" value="${deleteButton}" name="deleteButton" 
							onclick="confirmDelete(this.form, '${deleteTagUrl}','${confirmMessage}')"/>
						<input class="b-button__reference" type="hidden" name="cancelButton" 
							value="${cancelButton}" onclick="cancelEdit(this.form)"/> 
					</sf:form>
				</div>
			</c:forEach>
			<c:url  var="addTagUrl" value="/addupdatetags/addtag"/>
			<sf:form method="POST" commandName="tagTO" action="${addTagUrl}">
				<div class="b-tag__container">
					<label class="b-tag__label">
						<s:message code="addupdatetags.label.addtag"/>
					</label>
					<sf:input required="true" class="b-tag__input" type="text" path="tagName" maxlength="30"/> 
					<s:message var="saveButton" code="addupdatetags.button.save"/>
					<input class="b-button__reference" type="submit" value="${saveButton}"/>
				</div>
			</sf:form>
		</div>
	</div>
</div>