<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<c:url var="newsList" value="/newslist"/> 
<c:url var="addNews" value="/news/add"/> 
<c:url var="addUpdateAuthors" value="/addupdateauthors"/> 
<c:url var="addUpdateTags" value="/addupdatetags"/> 
<c:url var="menuItemImageUrl" value="/resources/images/menu_item.png"/>


<div class="b-menu__container">
	<div class="b-menu">
		<div class="${current eq 'newslist' ? 'b-menu__item_active' : 'b-menu__item'}">
			<img class="b-menu__item__image" src="${menuItemImageUrl}"/>
			<a class="b-menu__item__text" href="${newsList}">
				<s:message code="menu.reference.newslist" />
			</a>
		</div>
		<div class="${current eq 'addnews' ? 'b-menu__item_active' : 'b-menu__item'}">
			<img class="b-menu__item__image" src="${menuItemImageUrl}"/>
			<a class="b-menu__item__text" href="${addNews}">
				<s:message code="menu.reference.addnews" />
			</a>
		</div>
		<div class="${current eq 'addupdateauthors' ? 'b-menu__item_active' : 'b-menu__item'}">
			<img class="b-menu__item__image" src="${menuItemImageUrl}"/>
			<a class="b-menu__item__text" href="${addUpdateAuthors}">
				<s:message code="menu.reference.addupdateauthors" />
			</a>
		</div>
		<div class="${current eq 'addupdatetags' ? 'b-menu__item_active' : 'b-menu__item'}">
			<img class="b-menu__item__image" src="${menuItemImageUrl}"/>
			<a class="b-menu__item__text" href="${addUpdateTags}">
				<s:message code="menu.reference.addupdatetags" />
			</a>
		</div>
	</div>
</div>