<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<script>$(document).ready(function() {setDateTimePicker('${pageContext.response.locale}');});</script>
<fmt:setLocale value="${requestContext.response.locale}"/>

<div class="b-content__container">
	<div class="b-content">
		<div class="b-addnews__container">
			<c:url var="saveNews" value="/news/save"/>
			<s:message var="notNullAuthorMessage" code="NotNull.authorId"/>
			<sf:form method="POST" commandName="newsTO" 
				action="${saveNews}" onsubmit="return validateSelect(this, '${notNullAuthorMessage}');">
				<sf:input type="hidden" path="newsId" value="${newsTO.newsId}"/>
				<sf:input type="hidden" path="version" value="${newsTO.version}"/>
				<div class="b-addnews__title__container">
					<div class="b-addnews__title__label">
						<label for="title">
							<s:message code="addupdatenews.label.title" />
						</label>
					</div>
					<sf:input class="b-addnews__title__input" id="title" path="title" 
						value="${newsTO.title}" required="required" maxlength="30"/>
				</div>
				<div class="b-addnews__date__container">
					<div class="b-addnews__date__label">
						<label>
							<s:message code="addupdatenews.label.date" />
						</label>
					</div>
					<c:choose>
						<c:when test="${empty newsTO.newsId}">
							<sf:input type="text" path="creationDate" class="b-addnews__date__input b-datetimepicker" readonly="true"/>
						</c:when>
						<c:otherwise>
							<s:message var="datePattern" code="addupdatenews.dateformat"/>
							<fmt:formatDate pattern="${datePattern}" value="${newsTO.creationDate}" var="formattedCreationDate"/>
							<sf:input class="b-addnews__date__input" type="text" 
								path="creationDate" readonly="true" value="${formattedCreationDate}"/>
						</c:otherwise>
					</c:choose>
					
				</div>
				<div class="b-addnews__brief__container">
					<div class="b-addnews__brief__label">
						<label for="shortText">
							<s:message code="addupdatenews.label.brief" />
						</label>
					</div>
					<sf:textarea class="b-addnews__brief__textarea" id="shortText" path="shortText" 
						value="${newsTO.shortText}" required="required" />
				</div>
				<div class="b-addnews__content__container">
					<div class="b-addnews__content__label">
						<label for="fullText">
							<s:message code="addupdatenews.label.content"/>
						</label>
					</div>
					<sf:textarea id="fullText" class="b-addnews__content__textarea" path="fullText" 
						value="${newsTO.fullText}" required="required"/>
				</div>
				<div class="b-addnews__select__container">
					<div class="b-addnews__select">
						<select class="b-authorSelect" name="selectedAuthorId">
							<s:message var="authorPlaceholder" code="addupdatenews.select.author"/>
							<option value="" disabled selected>${authorPlaceholder}</option>
							<c:forEach var="author" items="${authorList}">
								<c:choose>
									<c:when test="${author.authorId eq selectedAuthorId}">
										<option selected="selected" value="${author.authorId}">
											<c:out value="${author.authorName}"/>
										</option>
									</c:when>
									<c:otherwise>
										<c:if test="${empty author.expired}">
											<option value="${author.authorId}">
												<c:out value="${author.authorName}"/>
											</option>
										</c:if>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						<s:message var="tagPlaceholder" code="addupdatenews.select.tags"/>
						<select class="b-tagListSelect" multiple="multiple" name="selectedTagIds">
							<option value="" disabled selected>${tagPlaceholder}</option>
							<c:forEach var="tag" items="${selectedTagList}">
								<option selected="selected" value="${tag.tagId}">
									<c:out value="${tag.tagName}"/>
								</option>
							</c:forEach>
							<c:forEach var="tag" items="${unselectedTagList}">
								<option value="${tag.tagId}">
									<c:out value="${tag.tagName}"/>
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<s:message var="saveButton" code="addupdatenews.button.save"/>
				<input class="b-addnews__button__save" type="submit" value="${saveButton}" />
				<sf:errors path="*" cssClass="b-errorblock" element="div"/>
			</sf:form>
			<c:if test="${not empty optimisticLockError}">
				<div class="b-errorblock">
					<s:message code="addupdatenews.label.optimisticlock"/>
				</div>
			</c:if>			
		</div>
	</div>
</div>
<script>

</script>



