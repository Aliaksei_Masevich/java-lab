package by.epam.newsapp.util;

import org.springframework.ui.Model;

/**
 * Class is used for setting pagination.
 */
public class PaginationUtil {

	private final Long elementsPerPage;

	private final Long maxPageCount;

	public PaginationUtil(Long elementsPerPage, Long maxPageCount) {
		this.elementsPerPage = elementsPerPage;
		this.maxPageCount = maxPageCount;
	}

	/**
	 * Sets model attributes for pagination.
	 * 
	 * @param currentPage
	 *            Current page
	 * @param elementCount
	 *            Element count
	 * @param model
	 *            Model attribute holder
	 * @return Modified current page
	 */
	public Long setPagination(Long currentPage, Long elementCount, Model model) {
		Long pageCount = (long) Math.ceil((double) elementCount
				/ elementsPerPage);
		if (currentPage > pageCount) {
			currentPage = pageCount;
		}
		if (currentPage < 1L) {
			currentPage = 1L;
		}
		if (pageCount > maxPageCount) {
			Long pageDifference = getPageDifference(currentPage, pageCount);
			model.addAttribute("startPage", currentPage - pageDifference);
			model.addAttribute("endPage", currentPage - pageDifference
					+ maxPageCount - 1);
		} else {
			model.addAttribute("startPage", 1L);
			model.addAttribute("endPage", pageCount);
		}
		model.addAttribute("currentPage", currentPage);
		return currentPage;
	}

	/**
	 * Gets start element number for current page.
	 * 
	 * @param currentPage
	 *            Current page
	 * @return Start element number
	 */
	public Long getStartElementNumber(Long currentPage) {
		return elementsPerPage * (currentPage - 1) + 1;
	}

	/**
	 * Gets end element number for current page.
	 * 
	 * @param currentPage
	 *            Current page
	 * @return End element number
	 */
	public Long getEndElementNumber(Long currentPage) {
		return elementsPerPage * currentPage;
	}

	/**
	 * Gets current page by element number.
	 * 
	 * @param elementNumber
	 *            Element number
	 * @return Current page
	 */
	public Long getCurrentPageByElementNumber(Long elementNumber) {
		return elementNumber != null ? (long) Math.ceil((double) elementNumber
				/ elementsPerPage) : 1L;
	}

	/**
	 * Gets difference between current and start pages.
	 * 
	 * @param currentPage
	 *            Current page
	 * @param pageCount
	 *            Total page count
	 * @return Page difference between current and start page
	 */
	private Long getPageDifference(Long currentPage, Long pageCount) {
		for (Long i = maxPageCount / 2; i < maxPageCount; i++) {
			Long j = maxPageCount - i;
			if (currentPage - i >= 1L
					&& currentPage + maxPageCount - i - 1 <= pageCount) {
				return i;
			}
			if (currentPage - j >= 1L
					&& currentPage + maxPageCount - j - 1 <= pageCount) {
				return j;
			}
		}
		return 0L;
	}
}
