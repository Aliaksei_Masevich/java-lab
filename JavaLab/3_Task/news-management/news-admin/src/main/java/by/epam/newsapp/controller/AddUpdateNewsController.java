package by.epam.newsapp.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for handling requests for /news
 */
@Controller
@SessionAttributes("searchCriteria")
@RequestMapping(value = "/news")
public class AddUpdateNewsController {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Registers custom editor for String to Date conversion.
	 * 
	 * @param binder
	 *            Web Data Binder
	 * @param locale
	 *            Current locale
	 */
	@InitBinder
	public void initDateBinder(WebDataBinder binder, Locale locale) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				messageSource.getMessage("addupdatenews.dateformat", null,
						locale));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}

	/**
	 * Handles GET requests for /news/update/{newsId}. Sets model attributes for
	 * addupdatenews view.
	 * 
	 * @param newsId
	 *            News id
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/update/{newsId}", method = RequestMethod.GET)
	public String showUpdateNewsForm(
			@PathVariable(value = "newsId") Long newsId, Model model)
			throws ServiceException {
		NewsTO news = null;
		if (!model.containsAttribute("newsTO")) {
			news = newsService.getNewsById(newsId);
			model.addAttribute("newsTO", news);
		}
		setAuthorSelect(news, authorService.getAllAuthors(), model);
		setTagSelect(news, tagService.getAllTags(), model);
		return "addupdatenews";
	}

	/**
	 * Handles GET requests for /news/update/{newsId}. Sets model attributes for
	 * addupdatenews view.
	 * 
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showAddNewsForm(Model model) throws ServiceException {
		if (!model.containsAttribute("newsTO")) {
			NewsTO newsTO = new NewsTO();
			newsTO.setCreationDate(new Date());
			model.addAttribute("newsTO", newsTO);
		}
		setAuthorSelect(null, authorService.getAllAuthors(), model);
		setTagSelect(null, tagService.getAllTags(), model);
		return "addupdatenews";
	}

	/**
	 * Handles POST requests for /addnews. Adds new News if passed parameters
	 * are valid. Otherwise sets redirect attributes with validation error
	 * messages.
	 * 
	 * @param newsTO
	 *            NewsTO object to be added
	 * @param result
	 *            Binding result
	 * @param authorId
	 *            News author id
	 * @param tagIds
	 *            News tag id array
	 * @param searchCriteria
	 *            Search criteria
	 * @param redirectionAttributes
	 *            Redirect attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveNews(
			@Valid NewsTO newsTO,
			BindingResult result,
			@RequestParam(value = "selectedAuthorId", required = true) List<Long> authorIdList,
			@RequestParam(value = "selectedTagIds", required = false) List<Long> tagIdList,
			@ModelAttribute(value = "searchCriteria") SearchCriteria searchCriteria,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes
					.addFlashAttribute(result.getObjectName(), newsTO);
			redirectAttributes.addFlashAttribute("selectedAuthorId",
					authorIdList.get(0));
			redirectAttributes.addFlashAttribute("selectedTagIds", tagIdList);
			if (newsTO.getNewsId() != null) {
				return "redirect:/news/update/" + newsTO.getNewsId();
			} else {
				return "redirect:/news/add";
			}
		} else {
			try {
				newsTO.setModificationDate(newsTO.getNewsId() != null ? new Date()
						: newsTO.getCreationDate());
				newsTO.setAuthorList(authorIdList != null ? authorService
						.getAuthorListByIdList(authorIdList) : null);
				newsTO.setTagList(tagIdList != null ? tagService.getTagListByIdList(tagIdList) : null);
				Long newsId = newsService.saveNews(newsTO);
				searchCriteria.setAuthorId(null);
				searchCriteria.setTagIdList(null);
				return "redirect:/newsview/" + newsId;
			} catch (OptimisticLockModificationException e) {
				redirectAttributes.addFlashAttribute("optimisticLockError", e);
				return "redirect:/news/update/" + newsTO.getNewsId();
			}

		}
	}

	/**
	 * Sets model attributes for author select.
	 * 
	 * @param newsId
	 *            News id
	 * @param authorList
	 *            Author list
	 * @param model
	 *            Model attribute holder
	 * @throws ServiceException
	 */
	private void setAuthorSelect(NewsTO news, List<AuthorTO> authorList,
			Model model) throws ServiceException {
		if (!model.containsAttribute("selectedAuthorId") && news != null) {
			model.addAttribute("selectedAuthorId", news.getAuthorList().get(0)
					.getAuthorId());
		}
		model.addAttribute("authorList", authorList);
	}

	/**
	 * Sets model attributes for tag select.
	 * 
	 * @param newsId
	 *            News id
	 * @param unselectedTagList
	 *            Author list
	 * @param model
	 *            Model attribute holder
	 * @throws ServiceException
	 */
	private void setTagSelect(NewsTO news, List<TagTO> unselectedTagList,
			Model model) throws ServiceException {
		List<TagTO> selectedTagList = new ArrayList<TagTO>();
		if (model.asMap().get("selectedTagIds") != null) {
			Long[] tagIds = (Long[]) model.asMap().get("selectedTagIds");
			for (Long tagId : tagIds) {
				for (TagTO tag : unselectedTagList) {
					if (tagId.equals(tag.getTagId())) {
						selectedTagList.add(tag);
					}
				}
			}
		} else if (news != null) {
			selectedTagList = news.getTagList();
		}
		unselectedTagList.removeAll(selectedTagList);
		model.addAttribute("selectedTagList", selectedTagList);
		model.addAttribute("unselectedTagList", unselectedTagList);
	}
}
