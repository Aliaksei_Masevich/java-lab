package by.epam.newsapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for handling requests for /addupdatetags.
 */
@Controller
@SessionAttributes("searchCriteria")
@RequestMapping(value = "/addupdatetags")
public class AddUpdateTagsController {

	@Autowired
	private TagService tagService;

	/**
	 * Handles GET requests for /addupdatetags. Sets model attributes for
	 * addupdatetags view.
	 * 
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showTagForm(Model model) throws ServiceException {
		model.addAttribute("tagList", tagService.getAllTags());
		if (!model.containsAttribute("tagTO")) {
			model.addAttribute("tagTO", new TagTO());
		}
		return "addupdatetags";
	}

	/**
	 * Handles POST requests for /addupdatetags/updatetag. Updates tag if passed
	 * parameters are valid. Otherwise sets redirect attributes with validation
	 * error messages.
	 * 
	 * @param tagTO
	 *            TagTO to be updates
	 * @param result
	 *            Binding result
	 * @param redirectAttributes
	 *            Redirect attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/updatetag", method = RequestMethod.POST)
	public String updateTag(@Valid TagTO tagTO, BindingResult result,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes.addFlashAttribute(result.getObjectName(), tagTO);
		} else {
			tagService.updateTag(tagTO);
		}
		return "redirect:/addupdatetags";
	}

	/**
	 * Handles POST requests for /addupdatetags/deletetag. Deletes tag by id and
	 * changes search criteria if search criteria contains this tag id.
	 * 
	 * @param tagTO
	 *            TagTO to be deleted
	 * @param searchCriteria
	 *            Search criteria
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deletetag", method = RequestMethod.POST)
	public String deleteTag(
			TagTO tagTO,
			@ModelAttribute(value = "searchCriteria") SearchCriteria searchCriteria)
			throws ServiceException {
		tagService.deleteTag(tagTO.getTagId());
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (tagIdList != null && tagIdList.contains(tagTO.getTagId())) {
			tagIdList.remove(tagTO.getTagId());
		}
		return "redirect:/addupdatetags";
	}

	/**
	 * Handles POST requests for /addupdatetags/addtag. Adds new tag if passed
	 * parameters are valid. Otherwise sets redirect attributes with validation
	 * error messages.
	 * 
	 * @param tagTO
	 *            TagTO to be updated
	 * @param result
	 *            Binding result
	 * @param redirectAttributes
	 *            Redirect attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addtag", method = RequestMethod.POST)
	public String addTag(@Valid TagTO tagTO, BindingResult result,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes.addFlashAttribute(result.getObjectName(), tagTO);
		} else {
			tagService.addTag(tagTO);
		}
		return "redirect:/addupdatetags";
	}
}
