package by.epam.newsapp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.AuthorService;

/**
 * Class is used for handling requests for /addupdateauthors.
 *
 */
@Controller
@RequestMapping("/addupdateauthors")
public class AddUpdateAuthorsController {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Registers custom editor for String to Date conversion.
	 * 
	 * @param binder
	 *            Web data binder
	 * @param locale
	 *            Current locale
	 */
	@InitBinder
	public void initDateBinder(WebDataBinder binder, Locale locale) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				messageSource.getMessage("addupdateauthors.dateformat", null,
						locale));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}

	/**
	 * Handles GET requests for /addupdateauthors. Sets model attributes for
	 * addupdateauthors view.
	 * 
	 * @param model
	 *            Model attribute holder
	 * @return View name
	 * @throws ServiceException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showAuthorForm(Model model) throws ServiceException {
		model.addAttribute("authorList", authorService.getAllAuthors());
		if (!model.containsAttribute("authorTO")) {
			model.addAttribute("authorTO", new AuthorTO());
		}
		return "addupdateauthors";
	}

	/**
	 * Handles POST requests for /addupdateauthors/updateauthor. Updates author
	 * if passed parameters are valid. Otherwise sets redirect attributes with
	 * validation error messages.
	 * 
	 * @param authorTO
	 *            AuthorTO to be updated
	 * @param result
	 *            Binding result
	 * @param redirectAttributes
	 *            Redirect attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/updateauthor", method = RequestMethod.POST)
	public String updateAuthor(@Valid AuthorTO authorTO, BindingResult result,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes.addFlashAttribute(result.getObjectName(),
					authorTO);
		} else {
			authorService.updateAuthor(authorTO);
		}
		return "redirect:/addupdateauthors";
	}

	/**
	 * Handles GET requests for /addupdateauthors/expireauthor/{authorId}. Sets
	 * author expiration date by id.
	 * 
	 * @param authorId
	 *            Id of author to be expired
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/expireauthor/{authorId}", method = RequestMethod.GET)
	public String expireAuthor(@PathVariable(value = "authorId") Long authorId)
			throws ServiceException {
		authorService.setExpired(authorId, new Date());
		return "redirect:/addupdateauthors";
	}

	/**
	 * Handles POST requests for /addupdateauthors/addauthor. Adds new author if
	 * passed parameters are valid. Otherwise sets redirection attributes with
	 * validation error messages.
	 * 
	 * @param authorTO
	 *            AuthorTO to be added
	 * @param result
	 *            Binding result
	 * @param redirectAttributes
	 *            Redirection attributes
	 * @return Redirection URL
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addauthor", method = RequestMethod.POST)
	public String addAuthor(@Valid AuthorTO authorTO, BindingResult result,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX
					+ result.getObjectName(), result);
			redirectAttributes.addFlashAttribute(result.getObjectName(),
					authorTO);
		} else {
			authorService.addAuthor(authorTO);
		}
		return "redirect:/addupdateauthors";
	}
}
