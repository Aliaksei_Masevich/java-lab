package by.epam.newsapp.dao.hibernate;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.dialect.Dialect;
import org.hibernate.sql.JoinType;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.util.QueryBuilder;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction between NewsTO and News table with
 * Hibernate framework.
 */
@SuppressWarnings("unchecked")
public class HibernateNewsDAO implements NewsDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @see NewsDAO#saveNews(NewsTO, List, List)
	 */
	@Override
	public Long saveNews(NewsTO news) throws DAOException, OptimisticLockModificationException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(news);
			transaction.commit();
		}catch (StaleObjectStateException e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new OptimisticLockModificationException(e);
		}catch (HibernateException e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateNewsDAO.saveNews(NewsTO, List, List) method with parameter " 
					+ news, e);
		} finally {
			session.close();
		}
		return news.getNewsId();
	}

	/**
	 * @see NewsDAO#getNewsById(Long)
	 */
	@Override
	public NewsTO getNewsById(Long newsId) throws DAOException {
		Session session = sessionFactory.openSession();
		NewsTO news = null;
		try {
			news = (NewsTO) session.get(NewsTO.class, newsId);
			if (news != null) {
				news.getAuthorList().size();
				news.getTagList().size();
				news.getCommentList().size();
			}
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.getNewsById(Long) method with parameter "
							+ newsId, e);
		} finally {
			session.close();
		}
		return news;
	}

	/**
	 * @see NewsDAO#deleteNews(List)
	 */
	@Override
	public void deleteNews(List<Long> newsIdList) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			int count = 0;
			int batchSize = Integer.valueOf(Dialect.DEFAULT_BATCH_SIZE);
			transaction = session.beginTransaction();
			for (Long newsId : newsIdList) {
				NewsTO news = (NewsTO) session.get(NewsTO.class, newsId);
				session.delete(news);
				if (++count % batchSize == 0) {
					session.flush();
					session.clear();
				}
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.deleteNews(Long) method with parameter "
							+ newsIdList, e);
		} finally {
			session.close();
		}
	}

	/**
	 * @see NewsDAO#saveNews(NewsTO, List, List)
	 */
	@Override
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start,
			Long end) throws DAOException {
		Session session = sessionFactory.openSession();
		List<NewsTO> newsList = null;
		try {
			Criteria criteria = createCriteriaForNewsSearch(session,
					searchCriteria, start, end);
			newsList = criteria.list();
			for (NewsTO news : newsList) {
				news.getAuthorList().size();
				news.getTagList().size();
			}
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.search(SearchCriteria, Long, Long) method with parameters "
							+ searchCriteria + ", " + start + ", " + end, e);
		} finally {
			session.close();
		}
		return newsList;
	}

	/**
	 * @see NewsDAO#getSearchedNewsCount(SearchCriteria)
	 */
	@Override
	public Long getSearchedNewsCount(SearchCriteria searchCriteria)
			throws DAOException {
		Session session = sessionFactory.openSession();
		Long newsCount = null;
		try {
			Criteria criteria = createCriteriaForGettinNewsCount(session,
					searchCriteria);
			newsCount = (Long) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.getSearchedNewsCount(SearchCriteria) method with parameter "
							+ searchCriteria, e);
		} finally {
			session.close();
		}
		return newsCount;
	}

	/**
	 * @see NewsDAO#getSearchedNewsNumber(SearchCriteria, Long)
	 */
	@Override
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId)
			throws DAOException {
		Session session = sessionFactory.openSession();
		Long newsNumber = null;
		try {
			Query query = session.createSQLQuery(QueryBuilder
					.buildSearchedNewsNumberQuery(searchCriteria));
			setQueryForGettingNewsNumber(query, searchCriteria, newsId);
			newsNumber = ((BigDecimal) query.uniqueResult()).longValue();
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.getPreviousNewsId(Long) method with parameters "
							+ newsId + ", " + searchCriteria, e);
		} finally {
			session.close();
		}
		return newsNumber;
	}

	/**
	 * @see NewsDAO#getPreviousNewsId(Long, SearchCriteria)
	 */
	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Session session = sessionFactory.openSession();
		Long previousNewsId = null;
		try {
			Query query = session.createSQLQuery(QueryBuilder
					.buildGetPreviousNewsQuery(searchCriteria));
			setQueryForGettingPreviousNextNewsId(query, searchCriteria, newsId);
			previousNewsId = ((BigDecimal) query.uniqueResult()).longValue();
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.getPreviousNewsId(Long) method with parameters "
							+ newsId + ", " + searchCriteria, e);
		} finally {
			session.close();
		}
		return previousNewsId;
	}

	/**
	 * @see NewsDAO#getNextNewsId(Long, SearchCriteria)
	 */
	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Session session = sessionFactory.openSession();
		Long nextNewsId = null;
		try {
			Query query = session.createSQLQuery(QueryBuilder
					.buildGetNextNewsQuery(searchCriteria));
			setQueryForGettingPreviousNextNewsId(query, searchCriteria, newsId);
			nextNewsId = ((BigDecimal) query.uniqueResult()).longValue();
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateNewsDAO.getPreviousNewsId(Long) method with parameters "
							+ newsId + ", " + searchCriteria, e);
		} finally {
			session.close();
		}
		return nextNewsId;
	}

	/**
	 * Sets query for getting news number.
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param newsId
	 *            News id
	 */
	private void setQueryForGettingNewsNumber(Query query,
			SearchCriteria searchCriteria, Long newsId) {
		int parameterIndex = 0;
		parameterIndex = setQueryWithSearchCriteria(query, searchCriteria,
				parameterIndex);
		query.setParameter(parameterIndex, newsId);
	}

	/**
	 * Sets query for getting previous or next news id.
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param newsId
	 *            News id
	 */
	private void setQueryForGettingPreviousNextNewsId(Query query,
			SearchCriteria searchCriteria, Long newsId) {
		int parameterIndex = 0;
		query.setParameter(parameterIndex++, newsId);
		parameterIndex = setQueryWithSearchCriteria(query, searchCriteria,
				parameterIndex);
		query.setParameter(parameterIndex, newsId);
	}

	/**
	 * Sets query with values from search criteria
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param parameterIndex
	 *            Start parameter index
	 * @return Current parameter index
	 */
	private int setQueryWithSearchCriteria(Query query,
			SearchCriteria searchCriteria, int parameterIndex) {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null) {
			query.setParameter(parameterIndex++, authorId);
		}
		if (tagIdList != null && !tagIdList.isEmpty()) {
			for (Long tagId : tagIdList) {
				query.setParameter(parameterIndex++, tagId);
			}
		}
		return parameterIndex;
	}

	/**
	 * Creates criteria for news search.
	 * 
	 * @param session
	 *            Session
	 * @param searchCriteria
	 *            Search criteria
	 * @param start
	 *            Selection start
	 * @param end
	 *            Selection end
	 * @return Criteria for news search
	 */
	private Criteria createCriteriaForNewsSearch(Session session,
			SearchCriteria searchCriteria, Long start, Long end) {
		DetachedCriteria detachedCriteria = DetachedCriteria
				.forClass(NewsTO.class);
		detachedCriteria.createAlias("authorList", "a",
				JoinType.LEFT_OUTER_JOIN);
		detachedCriteria.createAlias("tagList", "t", JoinType.LEFT_OUTER_JOIN);
		detachedCriteria.add(getDisjunctionForSearch(searchCriteria));
		detachedCriteria.setProjection(Projections.distinct(Projections
				.property("newsId")));
		detachedCriteria.setProjection(Projections.property("newsId"));
		Criteria criteria = session.createCriteria(NewsTO.class);
		criteria.createAlias("commentCountHolder", "c");
		criteria.add(Subqueries.propertyIn("newsId", detachedCriteria));
		criteria.addOrder(Order.desc("c.commentCount"));
		criteria.addOrder(Order.desc("modificationDate"));
		criteria.addOrder(Order.desc("newsId"));
		criteria.setFirstResult(start.intValue() - 1);
		criteria.setMaxResults(end.intValue() - start.intValue() + 1);
		return criteria;
	}

	/**
	 * Creates criteria for getting news count.
	 * 
	 * @param session
	 *            Session
	 * @param searchCriteria
	 *            Search criteria
	 * @return Criteria for getting news count
	 */
	private Criteria createCriteriaForGettinNewsCount(Session session,
			SearchCriteria searchCriteria) {
		Criteria criteria = session.createCriteria(NewsTO.class);
		criteria.createAlias("authorList", "a", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tagList", "t", JoinType.LEFT_OUTER_JOIN);
		criteria.add(getDisjunctionForSearch(searchCriteria));
		criteria.setResultTransformer(Criteria.PROJECTION);
		criteria.setProjection(Projections.countDistinct("newsId"));
		return criteria;
	}

	/**
	 * Gets Disjunction object for search.
	 * 
	 * @param searchCriteria
	 *            Search criteria
	 * @return Disjunction object
	 */
	private Disjunction getDisjunctionForSearch(SearchCriteria searchCriteria) {
		Disjunction disjunction = Restrictions.disjunction();
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null) {
			disjunction.add(Restrictions.eq("a.authorId", authorId));
		}
		if (tagIdList != null && !tagIdList.isEmpty()) {
			disjunction.add(Restrictions.in("t.tagId", tagIdList));
		}
		return disjunction;
	}

}
