package by.epam.newsapp.dao;

import java.util.List;

import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Interface describes methods for interaction with News table.
 */
public interface NewsDAO {
	
	/**
	 * Adds new news into News table and returns id of added
	 * news or updates news.
	 * @param news NewsTO entity to be added or updated
	 * @return News id
	 * @throws DAOException
	 * @throws OptimisticLockModificationException
	 */
	public Long saveNews(NewsTO news) throws DAOException, OptimisticLockModificationException;
	
	/**
	 * Gets NewsTO entity by id.
	 * @param newsId Id of news to be got
	 * @return NewsTO entity
	 * @throws DAOException
	 */
	public NewsTO getNewsById(Long newsId) throws DAOException;
	
	/**
	 * Deletes news by id.
	 * @param newsIdList Id list of news to be deleted
	 * @throws DAOException
	 */
	public void deleteNews(List<Long> newsIdList) throws DAOException;
	
	/**
	 * Gets news part according to search criteria.
	 * @param searchCriteria Criteria for search
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsTO list
	 * @throws DAOException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end) throws DAOException;
	
	/**
	 * Gets count of searched news
	 * @param searchCriteria Criteria for search
	 * @return Searched news count
	 * @throws DAOException
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Gets number of news in searched news list by news id.
	 * @param searchCriteria Criteria for search
	 * @param newsId News id
	 * @return News number in searched news list
	 * @throws DAOException
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws DAOException;
	
	/**
	 * Gets previous news id by news id in search list.
	 * @param newsId Current news id
	 * @param SearchCriteria Search Criteria
	 * @return Previous news id
	 * @throws DAOException
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Gets next news id by news id in search list.
	 * @param newsId Current news id
	 * @param SearchCriteria Search Criteria
	 * @return Next news id
	 * @throws DAOException
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
}
