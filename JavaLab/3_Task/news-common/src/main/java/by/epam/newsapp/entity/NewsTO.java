package by.epam.newsapp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import by.epam.newsapp.util.RegularExpression;

/**
 * Class represents News content.
 */
@Entity
@Table(name="news")
public class NewsTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "News_Sequence", sequenceName = "News_Sequence", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "News_Sequence")
	@Column(name = "news_id")
	private Long newsId;
	
	@NotNull
	@Size(min=1,max=30)
	@Pattern(regexp=RegularExpression.AT_LEAST_ONE_CHARACTER)
	@Column(name="title")
	private String title;
	
	@NotNull
	@Size(min=1,max=100)
	@Pattern(regexp=RegularExpression.AT_LEAST_ONE_CHARACTER)
	@Column(name="short_text")
	private String shortText;
	
	@NotNull
	@Size(min=1,max=2000)
	@Pattern(regexp=RegularExpression.AT_LEAST_ONE_CHARACTER)
	@Column(name="full_text")
	private String fullText;
	
	@NotNull
	@Column(name="creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@Column(name="modification_date")
	@Temporal(TemporalType.DATE)
	private Date modificationDate;
	
	@ManyToMany
	@JoinTable(
			name="news_author",
			joinColumns=@JoinColumn(name="news_id"),
			inverseJoinColumns=@JoinColumn(name="author_id")
	)
	private List<AuthorTO> authorList;
	
	@ManyToMany
	@JoinTable(
			name="news_tag",
			joinColumns=@JoinColumn(name="news_id"),
			inverseJoinColumns=@JoinColumn(name="tag_id")
	)
	private List<TagTO> tagList;
	
	@OneToMany(cascade=CascadeType.REMOVE, orphanRemoval=true)
	@JoinColumn(name="news_id", updatable=false, referencedColumnName="news_id")
	@OrderBy("creationDate ASC")
	private List<CommentTO> commentList;
	
	@OneToOne
	@JoinColumn(name="news_id", referencedColumnName="news_id", insertable=false, updatable=false)
	private CommentCountHolder commentCountHolder;
	
	@Version
	@Column(name="news_version")
	private Long version;
	
	public NewsTO(){}
	
	public Long getNewsId(){
		return newsId;
	}
	
	public void setNewsId(Long newsId){
		this.newsId = newsId;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getShortText(){
		return shortText;
	}
	
	public void setShortText(String shortText){
		this.shortText = shortText;
	}
	
	public String getFullText(){
		return fullText;
	}
	
	public void setFullText(String fullText){
		this.fullText = fullText;
	}
	
	public Date getCreationDate(){
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate){
		this.creationDate = creationDate;
	}
	
	public Date getModificationDate(){
		return modificationDate;
	}
	
	public void setModificationDate(Date modificationDate){
		this.modificationDate = modificationDate;
	}
	
	public void setAuthorList(List<AuthorTO> authorList){
		this.authorList = authorList;
	}
	
	public List<AuthorTO> getAuthorList(){
		return authorList;
	}
	
	public void setTagList(List<TagTO> tagList){
		this.tagList = tagList;
	}
	
	public List<TagTO> getTagList(){
		return tagList;
	}
	
	public void setCommentList(List<CommentTO> commentList){
		this.commentList = commentList;
	}
	
	public List<CommentTO> getCommentList(){
		return commentList;
	}
	
	public void setVersion(Long version){
		this.version = version;
	}
	
	public Long getVersion(){
		return version;
	}
	
	public void setCommentCountHolder(CommentCountHolder commentCountHolder){
		this.commentCountHolder = commentCountHolder;
	}
	
	public CommentCountHolder getCommentCountHolder(){
		return commentCountHolder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorList == null) ? 0 : authorList.hashCode());
		result = prime
				* result
				+ ((commentCountHolder == null) ? 0 : commentCountHolder
						.hashCode());
		result = prime * result
				+ ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTO other = (NewsTO) obj;
		if (authorList == null) {
			if (other.authorList != null)
				return false;
		} else if (!authorList.equals(other.authorList))
			return false;
		if (commentCountHolder == null) {
			if (other.commentCountHolder != null)
				return false;
		} else if (!commentCountHolder.equals(other.commentCountHolder))
			return false;
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsTO [newsId=" + newsId + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", authorList=" + authorList + ", tagList=" + tagList
				+ ", commentList=" + commentList + ", commentCountHolder="
				+ commentCountHolder + ", version=" + version + "]";
	}
	
	
}

