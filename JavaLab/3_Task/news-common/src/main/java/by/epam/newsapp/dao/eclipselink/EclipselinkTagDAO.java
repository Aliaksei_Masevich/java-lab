package by.epam.newsapp.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between TagTO and Tag table with
 * the Eclipselink JPA.
 */
public class EclipselinkTagDAO implements TagDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @see TagDAO#getAllTags()
	 */
	@Override
	public List<TagTO> getAllTags() throws DAOException {
		List<TagTO> tagList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<TagTO> criteriaQuery = criteriaBuilder
					.createQuery(TagTO.class);
			Root<TagTO> from = criteriaQuery.from(TagTO.class);
			CriteriaQuery<TagTO> selectQuery = criteriaQuery.select(from);
			selectQuery.orderBy(criteriaBuilder.asc(from.get("tagName")));
			TypedQuery<TagTO> typedQuery = entityManager
					.createQuery(selectQuery);
			tagList = typedQuery.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkTagDAO.getAllTags() method", e);
		}
		return tagList;
	}

	/**
	 * @see TagDAO#addTag(TagTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public Long addTag(TagTO tag) throws DAOException {
		Long tagId = null;
		try {
			entityManager.persist(tag);
			tagId = tag.getTagId();
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkTagDAO.addTag(TagTO) method with parameter "
					+ tag, e);
		} finally {
			entityManager.clear();
		}
		return tagId;
	}

	/**
	 * @see TagDAO#getTagById(Long)
	 */
	@Override
	public TagTO getTagById(Long tagId) throws DAOException {
		TagTO tag = null;
		try {
			tag = entityManager.find(TagTO.class, tagId);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkTagDAO.getTagById(Long) method with parameter "
							+ tagId, e);
		} finally {
			entityManager.clear();
		}
		return tag;
	}

	/**
	 * @see TagDAO#deleteTag(Long)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void deleteTag(Long tagId) throws DAOException {
		try {
			TagTO tag = entityManager.find(TagTO.class, tagId);
			entityManager.remove(tag);
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkTagDAO.deleteTag(Long) method with parameter "
					+ tagId, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see TagDAO#updateTag(TagTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void updateTag(TagTO tag) throws DAOException {
		try {
			TagTO updatedTag = entityManager.find(TagTO.class, tag.getTagId());
			updatedTag.setTagName(tag.getTagName());
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkTagDAO.updateTag(TagTO) method with parameter "
							+ tag, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see TagDAO#getTagListByIdList(List)
	 */
	@Override
	public List<TagTO> getTagListByIdList(List<Long> tagIdList)
			throws DAOException {
		List<TagTO> tagList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<TagTO> criteriaQuery = criteriaBuilder
					.createQuery(TagTO.class);
			Root<TagTO> from = criteriaQuery.from(TagTO.class);
			CriteriaQuery<TagTO> selectQuery = criteriaQuery.select(from);
			selectQuery.where(from.get("tagId").in(tagIdList));
			TypedQuery<TagTO> typedQuery = entityManager.createQuery(selectQuery);
			tagList = typedQuery.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkTagDAO.getTagListByIdList(List) method with parameter "
							+ tagIdList, e);
		} finally {
			entityManager.clear();
		}
		return tagList;
	}

}
