package by.epam.newsapp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.CommentService;

/**
 * Class is used for interaction with {@link CommentDAO}
 */
public class CommentServiceImpl implements CommentService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommentServiceImpl.class);

	private CommentDAO commentDAO;
	
	public void setCommentDAO(CommentDAO commentDAO){
		this.commentDAO = commentDAO;
	}
	/**
	 * @see CommentService#addComment(CommentTO)
	 */
	public Long addComment(CommentTO comment) throws ServiceException {
		try {
			return commentDAO.addComment(comment);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see CommentService#getCommentById(Long)
	 */
	public CommentTO getCommentById(Long commentId) throws ServiceException {
		try {
			return commentDAO.getCommentById(commentId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see CommentService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentDAO.deleteComment(commentId);;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see CommentService#updateComment(CommentTO)
	 */
	public void updateComment(CommentTO comment) throws ServiceException {
		try {
			commentDAO.updateComment(comment);;;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
}
