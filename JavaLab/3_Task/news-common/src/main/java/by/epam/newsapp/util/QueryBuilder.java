package by.epam.newsapp.util;

import java.util.List;

/**
 * Class is used for building SQL queries.
 */
public class QueryBuilder {
	
	private static final String SQL_SEARCH_START =
			"SELECT * FROM (SELECT e.*, ROWNUM rnum FROM (SELECT a.news_id, a.title, a.short_text, a.full_text, "
			+ "a.creation_date, a.modification_date, COUNT(b.comment_id) AS comment_count FROM news a LEFT JOIN "
			+ "comments b ON a.news_id = b.news_id";
	private static final String SQL_SEARCH_COUNT_START =
			"SELECT COUNT(*) AS news_count FROM (SELECT a.news_id FROM news a";
	private static final String SQL_SEARCH_NUMBER_START =
			"SELECT f.news_number FROM (SELECT ROWNUM AS news_number, e.news_id FROM (SELECT a.news_id, "
			+ "a.modification_date, COUNT(b.comment_id) AS comment_count FROM news a LEFT JOIN comments "
			+ "b ON a.news_id = b.news_id";
	private static final String SQL_GET_PREVIOUS_NEWS_START = 
			"SELECT prev_news_number FROM (SELECT news_id, LAG(news_id, 1, ?) OVER (ORDER BY news_number) AS "
			+ "prev_news_number FROM (SELECT ROWNUM AS news_number, news_id FROM (SELECT a.news_id, a.modification_date, "
			+ "COUNT(b.comment_id) AS comment_count FROM news a LEFT JOIN comments b ON a.news_id = b.news_id";
	private static final String SQL_GET_NEXT_NEWS_START =
			"SELECT next_news_number FROM (SELECT news_id, LEAD(news_id, 1, ?) OVER (ORDER BY news_number) AS "
			+ "next_news_number FROM (SELECT ROWNUM AS news_number, news_id FROM (SELECT a.news_id, a.modification_date, "
			+ "COUNT(b.comment_id) AS comment_count FROM news a LEFT JOIN comments b ON a.news_id = b.news_id";
	private static final String SQL_GET_PREVIOUS_NEXT_NEWS_END =
			" GROUP BY a.news_id, a.modification_date ORDER BY comment_count DESC, TO_DATE(a.modification_date) DESC, "
			+ "a.news_id DESC))) WHERE news_id = ?";
	private static final String SQL_SEARCH_JOIN_NEWS_AUTHOR_TABLE =
			" INNER JOIN news_author c ON a.news_id = c.news_id";
	private static final String SQL_SEARCH_JOIN_NEWS_TAG_TABLE = 
			" LEFT JOIN news_tag d ON a.news_id = d.news_id";
	private static final String SQL_SEARCH_WHERE_CLAUSE =
			" WHERE 1 = 2";
	private static final String SQL_SEARCH_AUTHOR_CRITERIA =
			" OR c.author_id = ?";
	private static final String SQL_SEARCH_TAG_CRITERIA_START =
			" OR d.tag_id IN (";
	private static final String SQL_SEARCH_TAG_CRITERIA =
			"?,";
	private static final String SQL_SEARCH_TAG_CRITERIA_END =
			")";
	private static final String SQL_SEARCH_END =
			" GROUP BY a.news_id, a.title, a.short_text, a.full_text, a.creation_date, a.modification_date "
			+ "ORDER BY comment_count DESC, TO_DATE(a.modification_date) DESC, a.news_id DESC) e WHERE ROWNUM <= ?) WHERE rnum >= ?";
	private static final String SQL_SEARCH_COUNT_END =
			" GROUP BY a.news_id)";
	private static final String SQL_SEARCH_NUMBER_END =
			" GROUP BY a.news_id, a.modification_date ORDER BY comment_count DESC, TO_DATE(a.modification_date) DESC, a.news_id DESC) "
			+ "e ) f WHERE f.news_id = ?";
	
	/**
	 * Builds query for selecting a part of searched news.
	 * @param searchCriteria Search criteria
	 * @return Built query string
	 */
	public static String buildSearchNewsPartQuery(SearchCriteria searchCriteria){
		StringBuilder searchQueryBuilder = new StringBuilder(SQL_SEARCH_START);
		buildSearchQuery(searchQueryBuilder, searchCriteria);
		searchQueryBuilder.append(SQL_SEARCH_END);
		return searchQueryBuilder.toString();
	}
	
	/**
	 * Builds query for getting news number in the searched news list.
	 * @param searchCriteria Search criteria
	 * @return Built query string
	 */
	public static String buildSearchedNewsNumberQuery(SearchCriteria searchCriteria){
		StringBuilder searchQueryBuilder = new StringBuilder(SQL_SEARCH_NUMBER_START);
		buildSearchQuery(searchQueryBuilder, searchCriteria);
		searchQueryBuilder.append(SQL_SEARCH_NUMBER_END);
		return searchQueryBuilder.toString();
	}
	
	/**
	 * Builds query for getting searched news count.
	 * @param searchCriteria Search criteria
	 * @return Built query string
	 */
	public static String buildSearchedNewsCountQuery(SearchCriteria searchCriteria){
		StringBuilder searchQueryBuilder = new StringBuilder(SQL_SEARCH_COUNT_START);
		buildSearchQuery(searchQueryBuilder, searchCriteria);
		searchQueryBuilder.append(SQL_SEARCH_COUNT_END);
		return searchQueryBuilder.toString();
	}
	
	/**
	 * Builds query for getting previous news id in searched news list.
	 * @param searchCriteria Search criteria
	 * @return Built query string
	 */
	public static String buildGetPreviousNewsQuery(SearchCriteria searchCriteria){
		StringBuilder searchQueryBuilder = new StringBuilder(SQL_GET_PREVIOUS_NEWS_START);
		buildSearchQuery(searchQueryBuilder, searchCriteria);
		searchQueryBuilder.append(SQL_GET_PREVIOUS_NEXT_NEWS_END);
		return searchQueryBuilder.toString();
	}
	
	/**
	 * Builds query for getting next news id in searched news list.
	 * @param searchCriteria Search criteria
	 * @return Built query string
	 */
	public static String buildGetNextNewsQuery(SearchCriteria searchCriteria){
		StringBuilder searchQueryBuilder = new StringBuilder(SQL_GET_NEXT_NEWS_START);
		buildSearchQuery(searchQueryBuilder, searchCriteria);
		searchQueryBuilder.append(SQL_GET_PREVIOUS_NEXT_NEWS_END);
		return searchQueryBuilder.toString();
	}
	
	/**
	 * Builds query part for searching by search criteria.
	 * @param searchQueryBuilder String builder for query building
	 * @param searchCriteria Search criteria
	 */
	private static void buildSearchQuery(StringBuilder searchQueryBuilder, SearchCriteria searchCriteria){
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null || (tagIdList != null && !tagIdList.isEmpty())){
			StringBuilder joinBuilder = new StringBuilder();
			StringBuilder whereClauseBuilder = new StringBuilder(SQL_SEARCH_WHERE_CLAUSE);
			if (authorId != null) {
				joinBuilder.append(SQL_SEARCH_JOIN_NEWS_AUTHOR_TABLE);
				whereClauseBuilder.append(SQL_SEARCH_AUTHOR_CRITERIA);
			}
			if (tagIdList != null && !tagIdList.isEmpty()){
				joinBuilder.append(SQL_SEARCH_JOIN_NEWS_TAG_TABLE);
				whereClauseBuilder.append(SQL_SEARCH_TAG_CRITERIA_START);
				for (int i=0; i < tagIdList.size(); i++){
					whereClauseBuilder.append(SQL_SEARCH_TAG_CRITERIA);
				}
				whereClauseBuilder.setLength(whereClauseBuilder.length() - 1);
				whereClauseBuilder.append(SQL_SEARCH_TAG_CRITERIA_END);
			}
			searchQueryBuilder.append(joinBuilder);
			searchQueryBuilder.append(whereClauseBuilder);
		}
	} 
}
