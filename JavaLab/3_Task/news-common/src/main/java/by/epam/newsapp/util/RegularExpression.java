package by.epam.newsapp.util;

/**
 * Interface is used for holding regular expressions.
 */
public interface RegularExpression {
	String AT_LEAST_ONE_CHARACTER =
			"(.|\\t|\\r|\\n)*\\S(.|\\t|\\r|\\n)*";
}
