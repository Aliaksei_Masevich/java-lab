package by.epam.newsapp.dao.hibernate;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between CommentTO and Comments table with
 * Hibernate framework.
 */
public class HibernateCommentDAO implements CommentDAO{

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * @see CommentDAO#addComment(CommentTO)
	 */
	@Override
	public Long addComment(CommentTO comment) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(comment);
			transaction.commit();
		} catch (HibernateException e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateCommentDAO.addComment(CommentTO) method with parameter " 
					+ comment, e);
		} finally {
			session.close();
		}
		return comment.getCommentId();
	}

	/**
	 * @see CommentDAO#getCommentById(Long)
	 */
	@Override
	public CommentTO getCommentById(Long commentId) throws DAOException {
		Session session = sessionFactory.openSession();
		CommentTO comment = null;
		try {
			comment = (CommentTO) session.get(CommentTO.class, commentId);
		} catch (HibernateException e){
			throw new DAOException("Exception occurred while calling "
					+ "HibernateCommentDAO.getCommentById(Long) method with parameter " 
					+ commentId, e);
		} finally {
			session.close();
		}
		return comment;
	}

	/**
	 * @see CommentDAO#deleteComment(Long)
	 */
	@Override
	public void deleteComment(Long commentId) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			CommentTO comment = (CommentTO) session.get(CommentTO.class, commentId);
			session.delete(comment);
			transaction.commit();
		} catch (HibernateException e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateCommentDAO.deleteComment(Long) method with parameter " 
					+ commentId, e);
		} finally {
			session.close();
		}
	}

	/**
	 * @see CommentDAO#updateComment(CommentTO)
	 */
	@Override
	public void updateComment(CommentTO comment) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(comment);
			transaction.commit();
		} catch (HibernateException e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateCommentDAO.updateComment(CommentTO) method with parameter " 
					+ comment, e);
		} finally {
			session.close();
		}
		
	}

}
