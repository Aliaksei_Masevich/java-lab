package by.epam.newsapp.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between AuthorTO and Author table with
 * Hibernate framework.
 */
@SuppressWarnings("unchecked")
public class HibernateAuthorDAO implements AuthorDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @see AuthorDAO#getAllAuthors()
	 */
	@Override
	public List<AuthorTO> getAllAuthors() throws DAOException {
		Session session = sessionFactory.openSession();
		List<AuthorTO> authorList = null;
		try {
			authorList = session.createCriteria(AuthorTO.class)
					.addOrder(Order.asc("authorName")).list();
		} catch (HibernateException e) {
			throw new DAOException("Exception occurred while calling "
					+ "HibernateAuthorDAO.getAllAuthors() method", e);
		} finally {
			session.close();
		}
		return authorList;
	}

	/**
	 * @see AuthorDAO#addAuthor(AuthorTO)
	 */
	@Override
	public Long addAuthor(AuthorTO author) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(author);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.addAuthor(AuthorTO) method with parameter "
							+ author, e);
		} finally {
			session.close();
		}
		return author.getAuthorId();
	}

	/**
	 * @see AuthorDAO#getAuthorById(Long)
	 */
	@Override
	public AuthorTO getAuthorById(Long authorId) throws DAOException {
		Session session = sessionFactory.openSession();
		AuthorTO author = null;
		try {
			author = (AuthorTO) session.get(AuthorTO.class, authorId);
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.getAuthorById(Long) method with parameter "
							+ authorId, e);
		} finally {
			session.close();
		}
		return author;
	}

	/**
	 * @see AuthorDAO#deleteAuthor(Long)
	 */
	@Override
	public void deleteAuthor(Long authorId) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			AuthorTO author = (AuthorTO) session.get(AuthorTO.class, authorId);
			session.delete(author);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.deleteAuthor(Long) method with parameter "
							+ authorId, e);
		} finally {
			session.close();
		}

	}

	/**
	 * @see AuthorDAO#updateAuthor(AuthorTO)
	 */
	@Override
	public void updateAuthor(AuthorTO author) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(author);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.updateAuthor(AuthorTO) method with parameter "
							+ author, e);
		} finally {
			session.close();
		}

	}

	/**
	 * @see AuthorDAO#setExpired(Long, Date)
	 */
	@Override
	public void setExpired(Long authorId, Date expired) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			AuthorTO author = (AuthorTO) session.get(AuthorTO.class, authorId);
			author.setExpired(expired);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.setExpired(Long, Date) method with parameters "
							+ authorId + ", " + expired, e);
		} finally {
			session.close();
		}
	}

	/**
	 * @see AuthorDAO#getAuthorListByIdList(List)
	 */
	@Override
	public List<AuthorTO> getAuthorListByIdList(List<Long> authorIdList)
			throws DAOException {
		Session session = sessionFactory.openSession();
		List<AuthorTO> authorList = null;
		try {
			authorList = session.createCriteria(AuthorTO.class)
					.add(Restrictions.in("authorId", authorIdList)).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "HibernateAuthorDAO.getAuthorListByIdList(Long) method with parameter "
							+ authorIdList, e);
		} finally {
			session.close();
		}
		return authorList;
	}

}
