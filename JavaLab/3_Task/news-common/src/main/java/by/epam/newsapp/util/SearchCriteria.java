package by.epam.newsapp.util;

import java.io.Serializable;
import java.util.List;

/**
 *	Class represents criteria for search
 */
public class SearchCriteria implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long authorId;
	private List<Long> tagIdList;
	
	public SearchCriteria(){}

	public Long getAuthorId(){
		return authorId;
	}
	
	public void setAuthorId(Long authorId){
		this.authorId = authorId;
	}
	
	public List<Long> getTagIdList(){
		return tagIdList;
	}
	
	public void setTagIdList(List<Long> tagIdList){
		this.tagIdList = tagIdList;
	}
	
	@Override
	public String toString(){
		return "SearchCriteria [authorId = " + authorId + ", tagIdList = " + tagIdList + "]";
	}
	
}
