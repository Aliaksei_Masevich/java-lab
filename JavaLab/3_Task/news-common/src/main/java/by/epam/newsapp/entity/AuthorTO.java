package by.epam.newsapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import by.epam.newsapp.util.RegularExpression;

/**
 * Class represents Author database entity.
 */
@Entity
@Table(name="author")
public class AuthorTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "Author_Sequence", sequenceName = "Author_Sequence", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Author_Sequence")
	@Column(name="author_id")
	private Long authorId;
	
	@NotNull
	@Size(min=1,max=30)
	@Pattern(regexp=RegularExpression.AT_LEAST_ONE_CHARACTER)
	@Column(name="author_name")
	private String authorName;
	
	@Column(name="expired")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expired;
	
	public AuthorTO(){}
	
	public Long getAuthorId(){
		return authorId;
	}
	
	public void setAuthorId(Long authorId){
		this.authorId = authorId;
	}
	
	public String getAuthorName(){
		return authorName;
	}
	
	public void setAuthorName(String authorName){
		this.authorName = authorName;
	}
	
	public Date getExpired(){
		return expired;
	}
	
	public void setExpired(Date expired){
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName
				+ ", expired=" + expired + "]";
	}

		
}
