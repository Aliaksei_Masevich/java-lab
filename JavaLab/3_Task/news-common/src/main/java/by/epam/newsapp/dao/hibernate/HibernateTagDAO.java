package by.epam.newsapp.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between TagTO and Tag table with Hibernate
 * framework.
 */
public class HibernateTagDAO implements TagDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @see TagDAO#getAllTags()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TagTO> getAllTags() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<TagTO> tagList = null;
		try {
			tagList = session.createCriteria(TagTO.class)
					.addOrder(Order.asc("tagName")).list();
			;
		} catch (HibernateException e) {
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.getAllTags() method", e);
		} finally {
			session.close();
		}
		return tagList;
	}

	/**
	 * @see TagDAO#addTag(TagTO)
	 */
	@Override
	public Long addTag(TagTO tag) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(tag);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.addTag(TagTO) method with parameter "
					+ tag, e);
		} finally {
			session.close();
		}
		return tag.getTagId();
	}

	/**
	 * @see TagDAO#getTagById(Long)
	 */
	@Override
	public TagTO getTagById(Long tagId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		TagTO tag = null;
		try {
			tag = (TagTO) session.get(TagTO.class, tagId);
		} catch (HibernateException e) {
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.getTagById(Long) method with parameter "
					+ tagId, e);
		} finally {
			session.close();
		}
		return tag;
	}

	/**
	 * @see TagDAO#deleteTag(Long)
	 */
	@Override
	public void deleteTag(Long tagId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			TagTO tag = (TagTO) session.get(TagTO.class, tagId);
			session.delete(tag);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.deleteTag(Long) method with parameter "
					+ tagId, e);
		} finally {
			session.close();
		}
	}

	/**
	 * @see TagDAO#updateTag(TagTO)
	 */
	@Override
	public void updateTag(TagTO tag) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			TagTO updatedTag = (TagTO) session.get(TagTO.class, tag.getTagId());
			updatedTag.setTagName(tag.getTagName());
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.updateTag(TagTO) method with parameter "
					+ tag, e);
		} finally {
			session.close();
		}
	}

	/**
	 * @see TagDAO#getTagListByIdList(List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TagTO> getTagListByIdList(List<Long> tagIdList)
			throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<TagTO> tagList = null;
		try {
			tagList = session.createCriteria(TagTO.class)
					.add(Restrictions.in("tagId", tagIdList))
					.list();
		} catch (HibernateException e){
			throw new DAOException("Exception occurred while calling "
					+ "HibernateTagDAO.getTagListByIdList(List) method with parameter " + tagIdList, e);
		} finally {
			session.close();
		}
		return tagList;
	}

}
