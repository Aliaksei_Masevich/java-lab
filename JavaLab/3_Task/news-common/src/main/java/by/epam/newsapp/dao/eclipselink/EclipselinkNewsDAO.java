package by.epam.newsapp.dao.eclipselink;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.entity.CommentCountHolder;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.util.QueryBuilder;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction between NewsTO and News table with the
 * Eclipselink JPA.
 */
public class EclipselinkNewsDAO implements NewsDAO {

	private static final int DELETE_BATCH_SIZE = 49;
	
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @see NewsDAO#saveNews(NewsTO, List, List)
	 */
	@Transactional(rollbackFor = {DAOException.class, OptimisticLockModificationException.class})
	@Override
	public Long saveNews(NewsTO news) throws DAOException,
			OptimisticLockModificationException {
		Long newsId = news.getNewsId();
		try {
			if (newsId == null) {
				entityManager.persist(news);
				newsId = news.getNewsId();
			} else {
				news.setCommentList(getCommentsByNewsId(newsId));
				entityManager.merge(news);
			}
			entityManager.flush();
		} catch (OptimisticLockException e) {
			throw new OptimisticLockModificationException(e);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.saveNews(Long) method with parameter "
							+ newsId, e);
		} finally {
			entityManager.clear();
		}
		return newsId;
	}

	/**
	 * @see NewsDAO#getNewsById(Long)
	 */
	@Override
	public NewsTO getNewsById(Long newsId) throws DAOException {
		NewsTO news = null;
		try {
			news = entityManager.find(NewsTO.class, newsId);
			if (news != null) {
				news.getAuthorList().size();
				news.getTagList().size();
				news.getCommentList().size();
			}
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.getNewsById(Long) method with parameter "
							+ newsId, e);
		} finally {
			entityManager.clear();
		}
		return news;
	}

	/**
	 * @see NewsDAO#deleteNews(List)
	 */
	@Transactional(rollbackFor = DAOException.class)
	@Override
	public void deleteNews(List<Long> newsIdList) throws DAOException {
		try {
			int count = 0;
			for (Long newsId : newsIdList) {
				NewsTO news = entityManager.find(NewsTO.class, newsId);
				entityManager.remove(news);
				if (++count % DELETE_BATCH_SIZE == 0) {
					entityManager.flush();
					entityManager.clear();
				}
			}
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.deleteNews(List) method with parameter "
							+ newsIdList, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see NewsDAO#search(SearchCriteria, Long, Long)
	 */
	@Override
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start,
			Long end) throws DAOException {
		List<NewsTO> newsList = null;
		try {
			TypedQuery<NewsTO> typedQuery = getTypedQueryForSearch(
					searchCriteria, start, end);
			newsList = typedQuery.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.search(SearchCriteria) method with parameter "
							+ searchCriteria, e);
		} finally {
			entityManager.clear();
		}
		return newsList;
	}

	/**
	 * @see NewsDAO#getSearchedNewsCount(SearchCriteria)
	 */
	@Override
	public Long getSearchedNewsCount(SearchCriteria searchCriteria)
			throws DAOException {
		Long newsCount = null;
		try {
			TypedQuery<Long> typedQuery = getTypedQueryForSearchedNewsCount(searchCriteria);
			newsCount = typedQuery.getSingleResult();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.getSearchedNewsCount(SearchCriteria) method with parameter "
							+ searchCriteria, e);
		} finally {
			entityManager.clear();
		}
		return newsCount;
	}

	/**
	 * @see NewsDAO#getSearchedNewsNumber(SearchCriteria, Long)
	 */
	@Override
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId)
			throws DAOException {
		Long newsNumber = null;
		try {
			Query query = entityManager.createNativeQuery(QueryBuilder
					.buildSearchedNewsNumberQuery(searchCriteria));
			setQueryForGettingNewsNumber(query, searchCriteria, newsId);
			newsNumber = ((BigDecimal) query.getSingleResult()).longValue();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.getSearchedNewsNumber(SearchCriteria, Long, Long) method with parameters "
							+ searchCriteria + ", " + newsId, e);
		} finally {
			entityManager.clear();
		}
		return newsNumber;
	}

	/**
	 * @see NewsDAO#getPreviousNewsId(Long, SearchCriteria)
	 */
	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Long previousNewsId = null;
		try {
			Query query = entityManager.createNativeQuery(QueryBuilder
					.buildGetPreviousNewsQuery(searchCriteria));
			setQueryForGettingPreviousNextNewsId(query, searchCriteria, newsId);
			previousNewsId = ((BigDecimal) query.getSingleResult()).longValue();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.getPreviousNewsId(Long, SearchCriteria) method with parameters "
							+ newsId + ", " + searchCriteria, e);
		} finally {
			entityManager.clear();
		}
		return previousNewsId;
	}

	/**
	 * @see NewsDAO#getNextNewsId(Long, SearchCriteria)
	 */
	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Long nextNewsId = null;
		try {
			Query query = entityManager.createNativeQuery(QueryBuilder
					.buildGetNextNewsQuery(searchCriteria));
			setQueryForGettingPreviousNextNewsId(query, searchCriteria, newsId);
			nextNewsId = ((BigDecimal) query.getSingleResult()).longValue();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkNewsDAO.getNextNewsId(Long, SearchCriteria) method with parameters "
							+ newsId + ", " + searchCriteria, e);
		} finally {
			entityManager.clear();
		}
		return nextNewsId;
	}

	/**
	 * Sets query for getting news number.
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param newsId
	 *            News id
	 */
	private void setQueryForGettingNewsNumber(Query query,
			SearchCriteria searchCriteria, Long newsId) {
		int parameterIndex = 1;
		parameterIndex = setQueryWithSearchCriteria(query, searchCriteria,
				parameterIndex);
		query.setParameter(parameterIndex, newsId);
	}

	/**
	 * Sets query for getting previous or next news id.
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param newsId
	 *            News id
	 */
	private void setQueryForGettingPreviousNextNewsId(Query query,
			SearchCriteria searchCriteria, Long newsId) {
		int parameterIndex = 1;
		query.setParameter(parameterIndex++, newsId);
		parameterIndex = setQueryWithSearchCriteria(query, searchCriteria,
				parameterIndex);
		query.setParameter(parameterIndex, newsId);
	}

	/**
	 * Sets query with values from search criteria
	 * 
	 * @param query
	 *            Query to be set
	 * @param searchCriteria
	 *            Search criteria
	 * @param parameterIndex
	 *            Start parameter index
	 * @return Current parameter index
	 */
	private int setQueryWithSearchCriteria(Query query,
			SearchCriteria searchCriteria, int parameterIndex) {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null) {
			query.setParameter(parameterIndex++, authorId);
		}
		if (tagIdList != null && !tagIdList.isEmpty()) {
			for (Long tagId : tagIdList) {
				query.setParameter(parameterIndex++, tagId);
			}
		}
		return parameterIndex;
	}

	/**
	 * Creates TypedQuery for getting search news list.
	 * 
	 * @param searchCriteria
	 *            Search criteria
	 * @param start
	 *            Selection start
	 * @param end
	 *            Selection end
	 * @return TypedQuery object
	 */
	private TypedQuery<NewsTO> getTypedQueryForSearch(
			SearchCriteria searchCriteria, Long start, Long end) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<NewsTO> criteriaQuery = criteriaBuilder
				.createQuery(NewsTO.class);
		Root<NewsTO> from = criteriaQuery.from(NewsTO.class);
		setCriteriaQueryWithSearchCriteria(criteriaBuilder, criteriaQuery,
				from, searchCriteria);
		CriteriaQuery<NewsTO> selectQuery = criteriaQuery.select(from)
				.distinct(true);
		Join<NewsTO, CommentCountHolder> commentJoin = from
				.join("commentCountHolder");
		selectQuery.orderBy(
				criteriaBuilder.desc(commentJoin.get("commentCount")),
				criteriaBuilder.desc(from.get("modificationDate")),
				criteriaBuilder.desc(from.get("newsId")));
		TypedQuery<NewsTO> typedQuery = entityManager.createQuery(selectQuery);
		typedQuery.setFirstResult(start.intValue() - 1);
		typedQuery.setMaxResults(end.intValue() - start.intValue() + 1);
		return typedQuery;
	}

	/**
	 * Creates TypedQuery for getting searched news count.
	 * 
	 * @param searchCriteria
	 *            Search criteria
	 * @return TypedQuery object
	 */
	private TypedQuery<Long> getTypedQueryForSearchedNewsCount(
			SearchCriteria searchCriteria) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder
				.createQuery(Long.class);
		Root<NewsTO> from = criteriaQuery.from(NewsTO.class);
		setCriteriaQueryWithSearchCriteria(criteriaBuilder, criteriaQuery,
				from, searchCriteria);
		CriteriaQuery<Long> selectQuery = criteriaQuery.select(criteriaBuilder
				.countDistinct(from));
		TypedQuery<Long> typedQuery = entityManager.createQuery(selectQuery);
		return typedQuery;
	}

	/**
	 * Sets criteria query for search.
	 * 
	 * @param criteriaBuilder
	 *            Criteria builder
	 * @param criteriaQuery
	 *            Criteria query
	 * @param from
	 *            Criteria root
	 * @param searchCriteria
	 *            Search criteria
	 */
	private <T> void setCriteriaQueryWithSearchCriteria(
			CriteriaBuilder criteriaBuilder, CriteriaQuery<T> criteriaQuery,
			Root<NewsTO> from, SearchCriteria searchCriteria) {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		if (authorId != null || (tagIdList != null && !tagIdList.isEmpty())) {
			Predicate whereClause = criteriaBuilder.disjunction();
			if (authorId != null) {
				Join<NewsTO, AuthorTO> authorJoin = from.join("authorList",
						JoinType.LEFT);
				whereClause = criteriaBuilder.or(whereClause, criteriaBuilder
						.equal(authorJoin.get("authorId"), authorId));
			}
			if (tagIdList != null && !tagIdList.isEmpty()) {
				Join<NewsTO, TagTO> tagJoin = from.join("tagList",
						JoinType.LEFT);
				whereClause = criteriaBuilder.or(whereClause,
						tagJoin.get("tagId").in(tagIdList));
			}
			criteriaQuery.where(whereClause);
		}
	}
	
	/**
	 * Gets news comment list
	 * @param newsId News id
	 * @return News comment list
	 */
	private List<CommentTO> getCommentsByNewsId(Long newsId){
		List<CommentTO> commentList = null;
		CriteriaBuilder criteriaBuilder = entityManager
				.getCriteriaBuilder();
		CriteriaQuery<CommentTO> criteriaQuery = criteriaBuilder
				.createQuery(CommentTO.class);
		Root<CommentTO> from = criteriaQuery.from(CommentTO.class);
		CriteriaQuery<CommentTO> selectQuery = criteriaQuery.select(from);
		selectQuery.where(criteriaBuilder.equal(from.get("newsId"), newsId));
		TypedQuery<CommentTO> typedQuery = entityManager.createQuery(selectQuery);
		commentList = typedQuery.getResultList();
		return commentList;
	}

}
