package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Interface declares methods for interaction with News logic.
 */
public interface NewsService {
	
	/**
	 * Adds new news into News table and returns id of added
	 * news or updates news.
	 * @param news NewsTO entity to be added or updated
	 * @return News id
	 * @throws ServiceException
	 * @throws OptimisticLockModificationException
	 */
	public Long saveNews(NewsTO news) throws ServiceException, OptimisticLockModificationException;
	
	/**
	 * Gets news by id.
	 * @param newsId Id of news to be got
	 * @return NewsTO entity
	 * @throws ServiceException
	 */
	public NewsTO getNewsById(Long newsId) throws ServiceException;
	
	/**
	 * Deletes news by id.
	 * @param newsIdList Id list of news to be deleted
	 * @throws ServiceException
	 */
	public void deleteNews(List<Long> newsIdList) throws ServiceException;
	
	/**
	 * Gets news part according to search criteria.
	 * @param searchCriteria Criteria for search
	 * @param start Selection start
	 * @param end Selection end
	 * @return NewsTO list
	 * @throws ServiceException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end) throws ServiceException;
	
	/**
	 * Gets count of searched news
	 * @param searchCriteria Criteria for search
	 * @return Searched news count
	 * @throws ServiceException
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets number of news in searched news list.
	 * @param searchCriteria Criteria for search
	 * @param newsId News id
	 * @return News number in searched news list
	 * @throws ServiceException
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
	
	/**
	 * Gets next news id by news id in search list.
	 * @param newsId News id
	 * @param searchCriteria Search criteria
	 * @return Next news id
	 * @throws ServiceException
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets previous news id by news id in search list.
	 * @param newsId News id
	 * @param searchCriteria Search criteria
	 * @return Previous news id
	 * @throws ServiceException
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException;
}
