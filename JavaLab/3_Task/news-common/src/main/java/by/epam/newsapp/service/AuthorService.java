package by.epam.newsapp.service;

import java.util.Date;
import java.util.List;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.ServiceException;

/**
 * Interface declares methods for interaction with Author logic.
 */
public interface AuthorService {
	
	/**
	 * Gets all authors.
	 * @return AuthorTO entity list
	 * @throws ServiceException
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException;
	
	/**
	 * Adds new author and returns id of added author.
	 * @param Author AuthorTO entity to be added
	 * @return Id of added author
	 * @throws ServiceException
	 */
	public Long addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Gets author by id.
	 * @param authorId Id of author to be got
	 * @return AuthorTO entity
	 * @throws ServiceException
	 */
	public AuthorTO getAuthorById(Long authorId) throws ServiceException;
	
	/**
	 * Updates author.
	 * @param author AuthorTO entity to be updated
	 * @throws ServiceException
	 */
	public void updateAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Deletes author by id.
	 * @param authorId Id of author to be deleted
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Sets author's expiration date.
	 * @param authorId Id of author to be expired
	 * @param expired Expiration date
	 * @throws ServiceException
	 */
	public void setExpired(Long authorId, Date expired) throws ServiceException;
	
	/**
	 * Gets author list by id list.
	 * @param authorIdList Author id list
	 * @return Author list
	 * @throws ServiceException
	 */
	public List<AuthorTO> getAuthorListByIdList(List<Long> authorIdList) throws ServiceException;
}
