package by.epam.newsapp.dao.eclipselink;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.transaction.annotation.Transactional;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.entity.CommentTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between CommentTO and Comments table with
 * the Eclipselink JPA.
 */
public class EclipselinkCommentDAO implements CommentDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void setEntityManager(EntityManager entityManager){
		this.entityManager = entityManager;
	}

	/**
	 * @see CommentDAO#addComment(CommentTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public Long addComment(CommentTO comment) throws DAOException {
		Long commentId = null;
		try {
			entityManager.persist(comment);
			commentId = comment.getCommentId();
			entityManager.flush();
		} catch (PersistenceException e){
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkCommentDAO.addComment(CommentTO) method with parameter " 
					+ comment, e);
		} finally {
			entityManager.clear();
		}
		return commentId;
	}

	/**
	 * @see CommentDAO#getCommentById(Long)
	 */
	@Override
	public CommentTO getCommentById(Long commentId) throws DAOException {
		CommentTO comment = null;
		try {
			comment = entityManager.find(CommentTO.class, commentId);
		} catch (PersistenceException e){
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkCommentDAO.getCommentById(Long) method with parameter " 
					+ commentId, e);
		} finally {
			entityManager.clear();
		}
		return comment;
	}

	/**
	 * @see CommentDAO#deleteComment(Long)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void deleteComment(Long commentId) throws DAOException {
		try {
			CommentTO comment = entityManager.find(CommentTO.class, commentId);
			entityManager.remove(comment);
			entityManager.flush();
		} catch (PersistenceException e){
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkCommentDAO.deleteComment(Long) method with parameter " 
					+ commentId, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see CommentDAO#updateComment(CommentTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void updateComment(CommentTO comment) throws DAOException {
		try {
			entityManager.merge(comment);
			entityManager.flush();
		} catch (PersistenceException e){
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkCommentDAO.updateComment(CommentTO) method with parameter " 
					+ comment, e);
		} finally {
			entityManager.clear();
		}
	}

}
