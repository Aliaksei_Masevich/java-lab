package by.epam.newsapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.TagService;

/**
 * Class is used for interaction with {@link TagDAO}
 */
public class TagServiceImpl implements TagService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TagServiceImpl.class);

	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/**
	 * @see TagService#getAllTags()
	 */
	public List<TagTO> getAllTags() throws ServiceException {
		try {
			return tagDAO.getAllTags();
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see TagService#addTag(TagTO)
	 */
	public Long addTag(TagTO tag) throws ServiceException {
		try {
			return tagDAO.addTag(tag);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see TagService#getTagById(Long)
	 */
	public TagTO getTagById(Long tagId) throws ServiceException {
		try {
			return tagDAO.getTagById(tagId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see TagService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteTag(tagId);
			;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}

	}

	/**
	 * @see TagService#updateTag(TagTO)
	 */
	public void updateTag(TagTO tag) throws ServiceException {
		try {
			tagDAO.updateTag(tag);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see TagService#getTagListByIdList(List)
	 */
	@Override
	public List<TagTO> getTagListByIdList(List<Long> tagIdList)
			throws ServiceException {
		try {
			return tagDAO.getTagListByIdList(tagIdList);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
