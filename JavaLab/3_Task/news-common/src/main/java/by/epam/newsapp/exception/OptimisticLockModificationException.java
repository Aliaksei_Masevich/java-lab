package by.epam.newsapp.exception;

/**
 *	Exception provides information about optimistic lock errors.
 */
public class OptimisticLockModificationException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public OptimisticLockModificationException() {
		super();
	}

	public OptimisticLockModificationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public OptimisticLockModificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public OptimisticLockModificationException(String message) {
		super(message);
	}

	public OptimisticLockModificationException(Throwable cause) {
		super(cause);
	}

}
