package by.epam.newsapp.dao.eclipselink;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for interaction between AuthorTO and Author table with
 * the Eclipselink JPA.
 */
public class EclipselinkAuthorDAO implements AuthorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @see AuthorDAO#getAllAuthors()
	 */
	@Override
	public List<AuthorTO> getAllAuthors() throws DAOException {
		List<AuthorTO> authorList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<AuthorTO> criteriaQuery = criteriaBuilder
					.createQuery(AuthorTO.class);
			Root<AuthorTO> from = criteriaQuery.from(AuthorTO.class);
			CriteriaQuery<AuthorTO> selectQuery = criteriaQuery.select(from);
			selectQuery.orderBy(criteriaBuilder.asc(from.get("authorName")));
			TypedQuery<AuthorTO> typedQuery = entityManager
					.createQuery(selectQuery);
			authorList = typedQuery.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException("Exception occurred while calling "
					+ "EclipselinkAuthorDAO.getAllAuthors() method", e);
		} finally {
			entityManager.clear();
		}
		return authorList;
	}

	/**
	 * @see AuthorDAO#addAuthor(AuthorTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public Long addAuthor(AuthorTO author) throws DAOException {
		Long authorId = null;
		try {
			entityManager.persist(author);
			authorId = author.getAuthorId();
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.addAuthor(AuthorTO) method with parameter "
							+ author, e);
		} finally {
			entityManager.clear();
		}
		return authorId;
	}

	/**
	 * @see AuthorDAO#getAuthorById(Long)
	 */
	@Override
	public AuthorTO getAuthorById(Long authorId) throws DAOException {
		AuthorTO author = null;
		try {
			author = entityManager.find(AuthorTO.class, authorId);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.getAuthorById(Long) method with parameter "
							+ authorId, e);
		} finally {
			entityManager.clear();
		}
		return author;
	}

	/**
	 * @see AuthorDAO#deleteAuthor(Long)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void deleteAuthor(Long authorId) throws DAOException {
		try {
			AuthorTO author = entityManager.find(AuthorTO.class, authorId);
			entityManager.remove(author);
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.deleteAuthor(Long) method with parameter "
							+ authorId, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see AuthorDAO#updateAuthor(AuthorTO)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void updateAuthor(AuthorTO author) throws DAOException {
		try {
			entityManager.merge(author);
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.updateAuthor(AuthorTO) method with parameter "
							+ author, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see AuthorDAO#setExpired(Long, Date)
	 */
	@Transactional(rollbackFor=DAOException.class)
	@Override
	public void setExpired(Long authorId, Date expired) throws DAOException {
		try {
			AuthorTO author = entityManager.find(AuthorTO.class, authorId);
			author.setExpired(expired);
			entityManager.flush();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.setExpired(Long, Date) method with parameters "
							+ authorId + ", " + expired, e);
		} finally {
			entityManager.clear();
		}
	}

	/**
	 * @see AuthorDAO#getAuthorListByIdList(List)
	 */
	@Override
	public List<AuthorTO> getAuthorListByIdList(List<Long> authorIdList)
			throws DAOException {
		List<AuthorTO> authorList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<AuthorTO> criteriaQuery = criteriaBuilder
					.createQuery(AuthorTO.class);
			Root<AuthorTO> from = criteriaQuery.from(AuthorTO.class);
			CriteriaQuery<AuthorTO> selectQuery = criteriaQuery.select(from);
			selectQuery.where(from.get("authorId").in(authorIdList));
			TypedQuery<AuthorTO> typedQuery = entityManager
					.createQuery(selectQuery);
			authorList = typedQuery.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Exception occurred while calling "
							+ "EclipselinkAuthorDAO.getAuthorListByIdList(List) method with parameter "
							+ authorIdList, e);
		} finally {
			entityManager.clear();
		}
		return authorList;
		
	}

}
