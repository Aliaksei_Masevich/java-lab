package by.epam.newsapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.annotations.Immutable;

/**
 * Class is used for holding news comment count.
 */
@Entity
@Table(name="comment_count")
@Immutable
@ReadOnly
public class CommentCountHolder implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="news_id")
	private Long newsId;
	
	@Column(name="comment_count")
	private Long commentCount;
	
	public void setNewsId(Long newsId){
		this.newsId = newsId;
	}
	
	public Long getNewsId(){
		return newsId;
	}
	
	public void setCommentCount(Long commentCount){
		this.commentCount = commentCount;
	}

	public Long getCommentCount(){
		return commentCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentCount == null) ? 0 : commentCount.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentCountHolder other = (CommentCountHolder) obj;
		if (commentCount == null) {
			if (other.commentCount != null)
				return false;
		} else if (!commentCount.equals(other.commentCount))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentCountHolder [newsId=" + newsId + ", commentCount="
				+ commentCount + "]";
	}
	
}

