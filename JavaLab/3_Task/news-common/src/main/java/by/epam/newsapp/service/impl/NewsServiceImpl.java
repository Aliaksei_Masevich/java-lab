package by.epam.newsapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for interaction with {@link NewsDAO}
 */
public class NewsServiceImpl implements NewsService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsServiceImpl.class);
	
	private NewsDAO newsDAO;
	
	public void setNewsDAO(NewsDAO newsDAO){
		this.newsDAO = newsDAO;
	}

	/**
	 * @see NewsService#saveNews(NewsTO)
	 */
	public Long saveNews(NewsTO news) throws ServiceException, OptimisticLockModificationException {
		try {
			return newsDAO.saveNews(news);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNewsById(Long)
	 */
	public NewsTO getNewsById(Long newsId) throws ServiceException {
		try {
			return newsDAO.getNewsById(newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#deleteNews(Long)
	 */
	public void deleteNews(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteNews(newsIdList);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#search(SearchCriteria)
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, Long start, Long end)
			throws ServiceException {
		try {
			return newsDAO.search(searchCriteria, start, end);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getSearchedNewsCount(SearchCriteria)
	 */
	public Long getSearchedNewsCount(SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getSearchedNewsCount(searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
	
	/**
	 * @see NewsService#getSearchedNewsNumber(SearchCriteria, Long)
	 */
	public Long getSearchedNewsNumber(SearchCriteria searchCriteria, Long newsId) throws ServiceException{
		try {
			return newsDAO.getSearchedNewsNumber(searchCriteria, newsId);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getNextNewsId(Long, SearchCriteria)
	 */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getNextNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}

	/**
	 * @see NewsService#getPreviousNewsId(Long, SearchCriteria)
	 */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws ServiceException {
		try {
			return newsDAO.getPreviousNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException(e);
		}
	}
}
