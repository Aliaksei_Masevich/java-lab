package by.epam.newsapp.dao;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.util.DateParser;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for {@link NewsDAO} testing.
 */
@RunWith(UnitilsJUnit4TestClassRunner.class)
@SpringApplicationContext("test-application-context.xml")
@DataSet("dao/NewsDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class NewsDAOTest {

	@SpringBean("newsDAO")
	private NewsDAO newsDAO;
	
	/**
	 * Sets spring profiles with value from unitils.properties
	 */
	@BeforeClass
	public static void setSystemProperty(){
		Properties systemProperties = System.getProperties();
		Properties unitilsProperties = new Properties();
		InputStream input = null;
		try {
			String fileName = "unitils.properties";
			input = NewsDAOTest.class.getClassLoader().getResourceAsStream(fileName);
			unitilsProperties.load(input);
			systemProperties.setProperty("spring.profiles.active",unitilsProperties.getProperty("spring.profiles"));
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			if (input != null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Tests {@link NewsDAO#getNewsById(Long)} for positive case.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetNewsById() throws DAOException, ParseException {
		NewsTO expectedNews = createNews(
				1L,
				"��� � ������",
				"��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00", "2015-06-16");
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#saveNews(NewsTO)} for positive case.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 * @throws OptimisticLockModificationException 
	 */
	@Test
	public void testAddNews() throws DAOException, ParseException, OptimisticLockModificationException {
		NewsTO expectedNews = createNews(
				null,
				"IPhone 6",
				"� �������� ���������� ����������� ������� iPhone 6",
				"������ ������ ��� ����� ������ ������� iPhone ����������� ������� ������������ ���������� �������-�� ���������� � � ��������.",
				"2015-06-26 08:00:00", "2015-06-26");
		expectedNews.setNewsId(newsDAO.saveNews(expectedNews));
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#updateNews(NewsTO)} for positive case.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 * @throws OptimisticLockModificationException 
	 */

	@Test
	public void testUpdateNews() throws DAOException, ParseException, OptimisticLockModificationException {
		NewsTO expectedNews = newsDAO.getNewsById(1L);
		expectedNews.setTitle("����� ������������� ���������");
		expectedNews
				.setShortText("����� ������������� ���������: ������ � ������� ������ ��� ��������");
		newsDAO.saveNews(expectedNews);
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests  {@link NewsDAO#updateNews(NewsTO)} for optimistic locking.
	 * @throws DAOException
	 * @throws ParseException
	 * @throws OptimisticLockModificationException
	 */
	@Test(expected=OptimisticLockModificationException.class)
	public void testUpdateOptimisticLocking() throws DAOException, ParseException, OptimisticLockModificationException {
		NewsTO expectedNews = newsDAO.getNewsById(1L);
		NewsTO expectedNews1 = newsDAO.getNewsById(1L);
		expectedNews.setTitle("����� ������������� ���������");
		expectedNews
				.setShortText("����� ������������� ���������: ������ � ������� ������ ��� ��������");
		newsDAO.saveNews(expectedNews);
		expectedNews1.setTitle("����� ������������� ���������");
		newsDAO.saveNews(expectedNews1);
		NewsTO actualNews = newsDAO.getNewsById(expectedNews.getNewsId());
		assertNewsEquals(expectedNews, actualNews);
	}
	
	/**
	 * Tests {@link NewsDAO#deleteNews(Long)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@SuppressWarnings("serial")
	@Test
	public void testDeleteNews() throws DAOException {
		List<Long> newsIdList = new ArrayList<Long>() {
			{
				add(1L);
				add(2L);
			}
		};
		newsDAO.deleteNews(newsIdList);
		for (Long newsId : newsIdList) {
			NewsTO actualNews = newsDAO.getNewsById(newsId);
			assertNull(actualNews);
		}
	}

	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by author.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByAuthor() throws DAOException, ParseException {
		Long authorId = 2L;
		List<Long> tagIdList = new ArrayList<Long>();
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria, 1L, 3L);
		int expectedNewsCount = 1;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������",
				"������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59", "2015-03-22");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by tags.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByTags() throws DAOException, ParseException {
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(3L);
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria, 1L, 3L);
		int expectedNewsCount = 2;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������",
				"������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59", "2015-03-22");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search by author and
	 * tags.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchByAuthorAndTags() throws DAOException, ParseException {
		Long authorId = 2L;
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		List<NewsTO> newsList = newsDAO.search(searchCriteria, 1L, 3L);
		int expectedNewsCount = 2;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				1L,
				"��� � ������",
				"��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00", "2015-06-16");
		NewsTO actualNews = newsList.get(1);
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#search(SearchCriteria)} for search without search
	 * criteria.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testSearchWithoutSearchCriteria() throws DAOException,
			ParseException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsTO> newsList = newsDAO.search(searchCriteria, 1L, 3L);
		int expectedNewsCount = 3;
		int actualNewsCount = newsList.size();
		assertEquals(expectedNewsCount, actualNewsCount);
		NewsTO expectedNews = createNews(
				3L,
				"������������ �������",
				"������������ ������� �������� �� �������� ��������� �������",
				"������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...",
				"2015-03-22 23:59:59", "2015-03-22");
		NewsTO actualNews = newsList.get(0);
		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests {@link NewsDAO#getSearchedNewsCount(SearchCriteria)} for positive
	 * case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testGetSearchedNewsCount() throws DAOException {
		Long authorId = 1L;
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(3L);
		tagIdList.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagIdList(tagIdList);
		Long expectedNewsCount = 2L;
		Long actualNewsCount = newsDAO.getSearchedNewsCount(searchCriteria);
		assertEquals(expectedNewsCount, actualNewsCount);
	}

	/**
	 * Tests {@link NewsDAO#getSearchedNewsNumber(SearchCriteria, Long)} for
	 * positive case.
	 * 
	 * @throws DAOException
	 */

	@Test
	public void testGetSearchedNewsNumber() throws DAOException {
		Long expectedNewsNumber = 3L;
		Long newsId = 2L;
		SearchCriteria searchCriteria = new SearchCriteria();
		Long actualNewsNumber = newsDAO.getSearchedNewsNumber(searchCriteria,
				newsId);
		assertEquals(expectedNewsNumber, actualNewsNumber);
	}

	/**
	 * Tests {@link NewsDAO#getPreviousNewsId(Long, SearchCriteria)} for
	 * positive case.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetPreviousNews() throws DAOException, ParseException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Long newsId = 1L;
		Long expectedNewsId = 3L;
		Long actualNewsId = newsDAO.getPreviousNewsId(newsId, searchCriteria);
		assertEquals(expectedNewsId, actualNewsId);
	}

	/**
	 * Tests {@link NewsDAO#getNextNewsId(Long, SearchCriteria)} for positive
	 * case.
	 * 
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testGetNextNews() throws DAOException, ParseException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Long newsId = 3L;
		Long expectedNewsId = 1L;
		Long actualNewsId = newsDAO.getNextNewsId(newsId, searchCriteria);
		assertEquals(expectedNewsId, actualNewsId);
	}

	/**
	 * Asserts that two NewsTO objects are equal.
	 * 
	 * @param expected
	 *            Expected NewsTO object
	 * @param actual
	 *            Actual NewsTO object
	 */
	private void assertNewsEquals(NewsTO expected, NewsTO actual) {
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getModificationDate(),
				actual.getModificationDate());
	}

	/**
	 * Creates new NewsTO object and sets its fields with values.
	 * 
	 * @param newsId
	 *            News id
	 * @param title
	 *            News title
	 * @param shortText
	 *            Short text
	 * @param fullText
	 *            Full text
	 * @param creationDate
	 *            Creation date
	 * @param modificationDate
	 *            Modification date
	 * @return Created NewsTO object
	 * @throws ParseException
	 */
	private NewsTO createNews(Long newsId, String title, String shortText,
			String fullText, String creationDate, String modificationDate)
			throws ParseException {
		NewsTO news = new NewsTO();
		news.setNewsId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(DateParser.parse(creationDate,
				"yyyy-MM-dd HH:mm:ss"));
		news.setModificationDate(DateParser.parse(modificationDate,
				"yyyy-MM-dd"));
		return news;
	}

}
