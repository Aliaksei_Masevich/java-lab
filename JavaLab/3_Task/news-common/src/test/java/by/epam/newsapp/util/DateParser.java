package by.epam.newsapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class is used for date parsing.
 *
 */
public class DateParser {
	/**
	 * Parses source accordingly to given pattern to produce a date.
	 * @param source String to be parsed
	 * @param pattern Date pattern
	 * @return Parsed date
	 * @throws ParseException
	 */
	public static Date parse(String source, String pattern) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date date = (Date)formatter.parse(source);
		return date;
	}
}
