package by.epam.newsapp.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for {@link TagDAO} testing.
 */
@RunWith(UnitilsJUnit4TestClassRunner.class)
@SpringApplicationContext("test-application-context.xml")
@DataSet("dao/TagDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class TagDAOTest {
	
	@SpringBean("tagDAO")
	private TagDAO tagDAO;
	
	/**
	 * Sets spring profiles with value from unitils.properties
	 */
	@BeforeClass
	public static void setSystemProperty(){
		Properties systemProperties = System.getProperties();
		Properties unitilsProperties = new Properties();
		InputStream input = null;
		try {
			String fileName = "unitils.properties";
			input = TagDAOTest.class.getClassLoader().getResourceAsStream(fileName);
			unitilsProperties.load(input);
			systemProperties.setProperty("spring.profiles.active",unitilsProperties.getProperty("spring.profiles"));
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			if (input != null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Tests {@link TagDAO#getAllTags()} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetAllTags() throws DAOException{
		List<TagTO> tagList = tagDAO.getAllTags();
		int expectedTagCount = 4;
		int actualTagCount = tagList.size();
		assertEquals(expectedTagCount, actualTagCount);
		TagTO expectedTag = createTag(2L, "����");
		TagTO actualTag = tagList.get(0);
		assertTagEquals(expectedTag, actualTag);		
	}
	
	/**
	 * Tests {@link TagDAO#getTagById(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetTagById() throws DAOException{
		TagTO expectedTag = createTag(1L, "����");
		TagTO actualTag = tagDAO.getTagById(expectedTag.getTagId());
		assertTagEquals(expectedTag, actualTag);
	}
	
	/**
	 * Tests {@link TagDAO#addTag(TagTO)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testAddTag() throws DAOException{
		TagTO expectedTag = createTag(null,"�����");
		expectedTag.setTagId(tagDAO.addTag(expectedTag));
		TagTO actualTag = tagDAO.getTagById(expectedTag.getTagId());
		assertTagEquals(expectedTag, actualTag);
	}
	
	/**
	 * Tests {@link TagDAO#deleteTag(Long)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testDeleteTag() throws DAOException{
		Long tagId = 2L;
		tagDAO.deleteTag(tagId);
		TagTO expectedTag = tagDAO.getTagById(tagId);
		assertNull(expectedTag);
	}
	
	/**
	 * Tests {@link TagDAO#updateTag(TagTO)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testUpdateTag() throws DAOException{
		TagTO expectedTag = createTag(1L, "��������");
		tagDAO.updateTag(expectedTag);
		TagTO actualTag = tagDAO.getTagById(expectedTag.getTagId());
		assertTagEquals(expectedTag, actualTag);
	}
	
	/**
	 * Tests {@link TagDAO#getAuthorListByIdList(List)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetTagListByIdList() throws DAOException {
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(2L);
		tagIdList.add(3L);
		List<TagTO> tagList = tagDAO.getTagListByIdList(tagIdList);
		assertEquals(2, tagList.size());
		TagTO expectedTag = tagDAO.getTagById(2L);
		assertTagEquals(expectedTag, tagList.get(0));
	}
	
	/**
	 * Asserts that two TagTO objects are equal.
	 * @param expected Expected TagTO object
	 * @param actual Actual TagTO object
	 */
	private void assertTagEquals(TagTO expected, TagTO actual){
		assertEquals(expected.getTagId(), actual.getTagId());
		assertEquals(expected.getTagName(), actual.getTagName());
	}
	
	/**
	 * Creates new TagTO object and sets its fields with values.
	 * @param tagId Tag id
	 * @param tagName Tag name
	 * @return Created TagTO object
	 */
	private TagTO createTag(Long tagId, String tagName){
		TagTO tag = new TagTO();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		return tag;
	}
}
