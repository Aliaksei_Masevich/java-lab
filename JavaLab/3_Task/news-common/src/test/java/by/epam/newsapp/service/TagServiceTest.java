package by.epam.newsapp.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.entity.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;

/**
 * Class is used for {@link TagService} testing.
 */
@ContextConfiguration(locations = {"classpath:/test-application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {
	
	@InjectMocks
	@Autowired
	private TagService tagService;
	
	@Mock
	private TagDAO tagDAO;
	
	private TagTO testTag;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		testTag = new TagTO();
		testTag.setTagId(1L);
		testTag.setTagName("�����");
	}
	
	/**
	 * Tests  {@link TagService#getAllTags()} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetAllTags() throws ServiceException, DAOException {
		List<TagTO> tagList = new ArrayList<TagTO>();
		tagList.add(testTag);
		when(tagDAO.getAllTags()).thenReturn(tagList);
		List<TagTO> actualTagList = tagService.getAllTags();
		assertEquals(tagList.size(), actualTagList.size());
		assertTagEquals(tagList.get(0),actualTagList.get(0));
		verify(tagDAO).getAllTags();
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Tests  {@link TagService#addTag(TagTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testAddTag() throws ServiceException, DAOException {
		when(tagDAO.addTag(testTag)).thenReturn(testTag.getTagId());
		Long actualTagId = tagService.addTag(testTag);
		assertEquals(testTag.getTagId(), actualTagId);
		verify(tagDAO).addTag(testTag);
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Tests  {@link TagService#getTagById(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetTagById() throws ServiceException, DAOException {
		Long tagId = testTag.getTagId();
		when(tagDAO.getTagById(tagId)).thenReturn(testTag);
		TagTO actualTag = tagService.getTagById(tagId);
		assertTagEquals(testTag, actualTag);
		verify(tagDAO).getTagById(tagId);
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Tests  {@link TagService#deleteTag(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testDeleteTag() throws ServiceException, DAOException {
		tagService.deleteTag(testTag.getTagId());
		verify(tagDAO).deleteTag(testTag.getTagId());
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Tests  {@link TagService#deleteTag(Long)} for throwing an exception.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test(expected = ServiceException.class)
	public void testDeleteTagForException() throws ServiceException, DAOException {
		doThrow(new DAOException()).when(tagDAO).deleteTag(testTag.getTagId());
		tagService.deleteTag(testTag.getTagId());
	}
	
	/**
	 * Tests  {@link TagService#updateTag(TagTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testUpdateTag() throws ServiceException, DAOException {
		tagService.updateTag(testTag);
		verify(tagDAO).updateTag(testTag);
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Tests  {@link TagService#getTagListByIdList(List)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetTagListByIdList() throws ServiceException, DAOException {
		List<Long> tagIdList = new ArrayList<Long>();
		tagService.getTagListByIdList(tagIdList);
		verify(tagDAO).getTagListByIdList(tagIdList);
		verifyNoMoreInteractions(tagDAO);
	}
	
	/**
	 * Asserts that two TagTO objects are equal.
	 * @param expected Expected TagTO object
	 * @param actual Actual TagTO object
	 */
	private void assertTagEquals(TagTO expected, TagTO actual){
		assertEquals(expected.getTagId(), actual.getTagId());
		assertEquals(expected.getTagName(), actual.getTagName());
	}
}
