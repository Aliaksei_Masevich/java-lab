package by.epam.newsapp.dao;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import by.epam.newsapp.entity.AuthorTO;
import by.epam.newsapp.exception.DAOException;

/**
 * Class is used for {@link AuthorDAO} testing.
 */
@RunWith(UnitilsJUnit4TestClassRunner.class)
@SpringApplicationContext("test-application-context.xml")
@DataSet("dao/AuthorDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class AuthorDAOTest {

	@SpringBean("authorDAO")
	private AuthorDAO authorDAO;

	/**
	 * Sets spring profiles with value from unitils.properties
	 */
	@BeforeClass
	public static void setSystemProperty(){
		Properties systemProperties = System.getProperties();
		Properties unitilsProperties = new Properties();
		InputStream input = null;
		try {
			String fileName = "unitils.properties";
			input = AuthorDAOTest.class.getClassLoader().getResourceAsStream(fileName);
			unitilsProperties.load(input);
			systemProperties.setProperty("spring.profiles.active",unitilsProperties.getProperty("spring.profiles"));
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			if (input != null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Tests {@link AuthorDAO#getAllAuthors()} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testGetAllAuthors() throws DAOException {
		List<AuthorTO> authorList = authorDAO.getAllAuthors();
		int expectedAuthorCount = 4;
		int actualAuthorCount = authorList.size();
		assertEquals(expectedAuthorCount, actualAuthorCount);
		AuthorTO expectedAuthor = createAuthor(2L, "Lenta.ru", null);
		AuthorTO actualAuthor = authorList.get(0);
		assertAuthorEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Tests {@link AuthorDAO#getAuthorById(Long)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testGetAuthorById() throws DAOException {
		AuthorTO expectedAuthor = createAuthor(1L, "Константин Сидорович", null);
		AuthorTO actualAuthor = authorDAO.getAuthorById(expectedAuthor
				.getAuthorId());
		assertAuthorEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Tests {@link AuthorDAO#addAuthor(AuthorTO)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testAddAuthor() throws DAOException {
		AuthorTO expectedAuthor = createAuthor(null, "AUTO.TUT.BY", null);
		expectedAuthor.setAuthorId(authorDAO.addAuthor(expectedAuthor));
		AuthorTO actualAuthor = authorDAO.getAuthorById(expectedAuthor
				.getAuthorId());
		assertAuthorEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Tests {@link AuthorDAO#deleteAuthor(Long)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testDeleteAuthor() throws DAOException {
		Long authorId = 1L;
		authorDAO.deleteAuthor(authorId);
		AuthorTO actualAuthor = authorDAO.getAuthorById(authorId);
		assertNull(actualAuthor);
	}

	/**
	 * Tests {@link AuthorDAO#updateAuthor(AuthorTO)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testUpdateAuthor() throws DAOException {
		AuthorTO expectedAuthor = createAuthor(1L, "TUT.BY", new Date());
		authorDAO.updateAuthor(expectedAuthor);
		AuthorTO actualAuthor = authorDAO.getAuthorById(expectedAuthor
				.getAuthorId());
		assertAuthorEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Tests {@link AuthorDAO#setExpired(Long, Date)} for positive case.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testSetExpired() throws DAOException {
		Long authorId = 1L;
		Date expired = new Date();
		authorDAO.setExpired(authorId, expired);
		AuthorTO author = authorDAO.getAuthorById(authorId);
		assertEquals(expired, author.getExpired());
	}
	
	/**
	 * Tests {@link AuthorDAO#getAuthorListByIdList(List)} for positive case.
	 * @throws DAOException
	 */
	@Test
	public void testGetAuthorListByIdList() throws DAOException {
		List<Long> authorIdList = new ArrayList<Long>();
		authorIdList.add(1L);
		authorIdList.add(3L);
		List<AuthorTO> authorList = authorDAO.getAuthorListByIdList(authorIdList);
		assertEquals(2, authorList.size());
		AuthorTO expectedAuthor = createAuthor(1L, "Константин Сидорович", null);
		assertAuthorEquals(expectedAuthor, authorList.get(0));
	}

	/**
	 * Asserts that two AuthorTO objects are equal.
	 * 
	 * @param expected
	 *            Expected AuthorTO object
	 * @param actual
	 *            Actual AuthorTO object
	 */
	private void assertAuthorEquals(AuthorTO expected, AuthorTO actual) {
		assertEquals(expected.getAuthorId(), actual.getAuthorId());
		assertEquals(expected.getAuthorName(), actual.getAuthorName());
		assertEquals(expected.getExpired(), actual.getExpired());
	}

	/**
	 * Creates new AuthorTO object and sets its fields with values.
	 * 
	 * @param authorId
	 *            Author id
	 * @param authorName
	 *            Author Name
	 * @param expired
	 *            Author expiration date
	 * @return Created AuthorTO object
	 */
	private AuthorTO createAuthor(Long authorId, String authorName, Date expired) {
		AuthorTO author = new AuthorTO();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);
		author.setExpired(expired);
		return author;
	}
}
