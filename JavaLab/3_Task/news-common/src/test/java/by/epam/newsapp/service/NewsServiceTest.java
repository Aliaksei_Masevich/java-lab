package by.epam.newsapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.OptimisticLockModificationException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.SearchCriteria;

/**
 * Class is used for {@link NewsService} testing
 */
@ContextConfiguration(locations = {"classpath:/test-application-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

	@InjectMocks
	@Autowired
	private NewsService newsService;
	
	@Mock
	private NewsDAO newsDAO;
	
	private NewsTO testNews;
	private Long testNewsAuthorId;
	private List<Long> testNewsTagIdList;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		testNews = new NewsTO();
		testNews.setNewsId(1L);
		testNews.setTitle("������������ �������");
		testNews.setShortText("������������ ������� �������� �� �������� ��������� �������");
		testNews.setFullText("������������ �������� ������� ������ ������ � ���������� � ������ ����� �������� �����������. ...");
		testNews.setCreationDate(new Date());
		testNews.setModificationDate(new Date());
		testNewsAuthorId = 1L;
		testNewsTagIdList = new ArrayList<Long>();
		testNewsTagIdList.add(1L);
		testNewsTagIdList.add(3L);
	}
	
	/**
	 * Tests {@link NewsService#saveNews(NewsTO)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 * @throws OptimisticLockModificationException 
	 */
	@Test
	public void testAddNews() throws ServiceException, DAOException, OptimisticLockModificationException {
		when(newsDAO.saveNews(testNews)).thenReturn(testNews.getNewsId());
		Long actualNewsId = newsService.saveNews(testNews);
		assertEquals(testNews.getNewsId(), actualNewsId);
		verify(newsDAO).saveNews(testNews);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#getNewsById(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetNewsById() throws ServiceException, DAOException {
		Long newsId = testNews.getNewsId();
		when(newsDAO.getNewsById(newsId)).thenReturn(testNews);
		NewsTO actualNews = newsService.getNewsById(newsId);
		assertNewsEquals(testNews,actualNews);
		verify(newsDAO).getNewsById(newsId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#deleteNews(Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testDeleteNews() throws ServiceException, DAOException {
		List<Long> newsIdList = new ArrayList<Long>();
		newsIdList.add(testNews.getNewsId());
		newsService.deleteNews(newsIdList);
		verify(newsDAO).deleteNews(newsIdList);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#search(SearchCriteria)} for search by author.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSearchByAuthor() throws ServiceException, DAOException {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(testNews);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(testNewsAuthorId);
		searchCriteria.setTagIdList(new ArrayList<Long>());
		when(newsDAO.search(searchCriteria,1L,1L)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.search(searchCriteria,1L,1L);
		assertEquals(expectedNewsList.size(), actualNewsList.size());
		assertNewsEquals(expectedNewsList.get(0), actualNewsList.get(0));
		verify(newsDAO).search(searchCriteria,1L,1L);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#search(SearchCriteria)} for search by tags.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSearchByTags() throws ServiceException, DAOException {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(testNews);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setTagIdList(testNewsTagIdList);
		when(newsDAO.search(searchCriteria,1L,1L)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.search(searchCriteria,1L,1L);
		assertEquals(expectedNewsList.size(), actualNewsList.size());
		assertNewsEquals(expectedNewsList.get(0), actualNewsList.get(0));
		verify(newsDAO).search(searchCriteria,1L,1L);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#search(SearchCriteria)} for search by author and tags.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSearchByAuthorAndTags() throws ServiceException, DAOException {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(testNews);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(testNewsAuthorId);
		searchCriteria.setTagIdList(testNewsTagIdList);
		when(newsDAO.search(searchCriteria,1L,1L)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.search(searchCriteria,1L,1L);
		assertEquals(expectedNewsList.size(), actualNewsList.size());
		assertNewsEquals(expectedNewsList.get(0), actualNewsList.get(0));
		verify(newsDAO).search(searchCriteria,1L,1L);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#search(SearchCriteria)} for search without any criteria.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testSearch() throws ServiceException, DAOException {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(testNews);
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setTagIdList(new ArrayList<Long>());
		when(newsDAO.search(searchCriteria,1L,1L)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.search(searchCriteria,1L,1L);
		assertEquals(expectedNewsList.size(), actualNewsList.size());
		assertNewsEquals(expectedNewsList.get(0), actualNewsList.get(0));
		verify(newsDAO).search(searchCriteria,1L,1L);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#getSearchedNewsCount(SearchCriteria)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetSearchedNewsCount() throws ServiceException, DAOException {
		Long expectedNewsCount = 1L;
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(testNewsAuthorId);
		searchCriteria.setTagIdList(testNewsTagIdList);
		when(newsDAO.getSearchedNewsCount(searchCriteria)).thenReturn(expectedNewsCount);
		Long actualNewsCount = newsService.getSearchedNewsCount(searchCriteria);
		assertEquals(expectedNewsCount, actualNewsCount);
		verify(newsDAO).getSearchedNewsCount(searchCriteria);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#getSearchedNewsNumber(SearchCriteria, Long)} for positive case.
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetSearchedNewsNumber() throws ServiceException, DAOException {
		Long expectedNewsNumber = 3L;
		Long newsId = 2L;
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDAO.getSearchedNewsNumber(searchCriteria, newsId)).thenReturn(expectedNewsNumber);
		Long actualNewsNumber = newsService.getSearchedNewsNumber(searchCriteria, newsId);
		assertEquals(expectedNewsNumber, actualNewsNumber);
		verify(newsDAO).getSearchedNewsNumber(searchCriteria, newsId);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#getNextNewsId(Long, SearchCriteria)}
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetNextNews() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDAO.getNextNewsId(testNews.getNewsId(), searchCriteria)).thenReturn(testNews.getNewsId());
		Long actualNewsId = newsService.getNextNewsId(testNews.getNewsId(), searchCriteria);
		assertEquals(testNews.getNewsId(), actualNewsId);
		verify(newsDAO).getNextNewsId(testNews.getNewsId(), searchCriteria);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Tests {@link NewsService#getPreviousNewsId(Long, SearchCriteria)}
	 * @throws ServiceException
	 * @throws DAOException
	 */
	@Test
	public void testGetPreviousNews() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDAO.getPreviousNewsId(testNews.getNewsId(), searchCriteria)).thenReturn(testNews.getNewsId());
		Long actualNewsId = newsService.getPreviousNewsId(testNews.getNewsId(), searchCriteria);
		assertEquals(testNews.getNewsId(), actualNewsId);
		verify(newsDAO).getPreviousNewsId(testNews.getNewsId(), searchCriteria);
		verifyNoMoreInteractions(newsDAO);
	}
	
	/**
	 * Asserts that two NewsTO objects are equal.
	 * @param expected Expected NewsTO object
	 * @param actual Actual NewsTO object
	 */
	private void assertNewsEquals(NewsTO expected, NewsTO actual){
		assertEquals(expected.getNewsId(), actual.getNewsId());
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getModificationDate(), actual.getModificationDate());
	}
}
