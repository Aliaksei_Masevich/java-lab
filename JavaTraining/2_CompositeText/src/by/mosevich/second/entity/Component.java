package by.mosevich.second.entity;

public interface Component {
    String getContent();
    ComponentType getType();
}
