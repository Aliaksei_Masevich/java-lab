package by.mosevich.second.entity;

public class Listing implements Component{
    private String content;

    public Listing(String content) {
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ComponentType getType() {
        return ComponentType.LISTING;
    }
}
