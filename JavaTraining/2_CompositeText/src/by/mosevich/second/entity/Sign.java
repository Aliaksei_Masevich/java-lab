package by.mosevich.second.entity;


public class Sign implements Component{
    private char content;

    public Sign(char content) {
        this.content = content;
    }

    @Override
    public String getContent() {
        return Character.toString(content);
    }

    @Override
    public ComponentType getType(){
        return ComponentType.SIGN;
    }
}
