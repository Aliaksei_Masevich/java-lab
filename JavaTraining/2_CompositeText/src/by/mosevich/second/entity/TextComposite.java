package by.mosevich.second.entity;

import java.util.ArrayList;
import java.util.List;

public class TextComposite implements Component{
    private List<Component>  components;
    private ComponentType type;

    public TextComposite(ComponentType type) {
        this.components = new ArrayList<>();
        this.type=type;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    public boolean add(Component component){
        return components.add(component);
    }

    @Override
    public String getContent() {
        String result = "";
        for(Component component:components){
            if (!result.isEmpty()){
                result=String.join(type.getDelimiter(),result,component.getContent());
            }
            else {
                result=String.join("",component.getContent());
            }
        }
        return result;
    }
}
