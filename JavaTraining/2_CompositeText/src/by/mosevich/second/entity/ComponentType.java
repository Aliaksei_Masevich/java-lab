package by.mosevich.second.entity;

public enum ComponentType {
    TEXT("\n"), PARAGRAPH(" "), LISTING, SENTENCE(" "), LEXEME(""), WORD(""), SIGN;

    private String delimiter;

    ComponentType(){}

    ComponentType(String delimiter) {
        this.delimiter=delimiter;
    }

    public String getDelimiter() {
        return delimiter;
    }
}
