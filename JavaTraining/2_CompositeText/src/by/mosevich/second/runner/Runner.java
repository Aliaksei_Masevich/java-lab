package by.mosevich.second.runner;

import by.mosevich.second.action.FirstAction;
import by.mosevich.second.action.SecondAction;
import by.mosevich.second.entity.TextComposite;
import by.mosevich.second.exception.TechnicalException;
import by.mosevich.second.parser.TextParser;
import by.mosevich.second.report.ReportWriter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Runner {
    static {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("log4j.properties"));
            PropertyConfigurator.configure(props);
        } catch (IOException e) {
            throw new ExceptionInInitializerError("Initialization Error: "+e.getMessage());
        }
    }
    static Logger log = Logger.getLogger(Runner.class);
    public static void main(String[] args) {
        try{
            TextParser parser = new TextParser();
            TextComposite text=parser.parseText("input/text.txt");
            ReportWriter reportWriter = new ReportWriter();
            reportWriter.write("output/text.txt",text);
            reportWriter.write("output/first_task.txt",FirstAction.action(text));
            reportWriter.write("output/second_task.txt", SecondAction.action(text));
        } catch (TechnicalException e){
            log.error(e.getMessage());
        }
    }
}
