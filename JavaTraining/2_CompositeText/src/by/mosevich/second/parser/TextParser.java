package by.mosevich.second.parser;

import by.mosevich.second.entity.ComponentType;
import by.mosevich.second.entity.Listing;
import by.mosevich.second.entity.Sign;
import by.mosevich.second.entity.TextComposite;
import by.mosevich.second.exception.TechnicalException;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {
    private static final String PARAGRAPH_OR_LISTING="!~[^~]*~+([^!~][^~]*~+)*!|(.+)(?!!~)";
    private static final String LISTING= "!~[^~]*~+([^!~][^~]*~+)*!";
    private static final String SENTENCE="(?:(?!\\. |\\? |! ).)*\\. |\\? |! ";
    private static final String WORD_OR_PUNCTUATION="[\\.,!\\?:;\\-]+$|.*[^\\.,!\\?:;\\-]";
    private static final String WORD=".*[^\\.,!\\?:;\\-]";
    private static final String SPACE=" ";
    private static final String NEW_LINE="\n";

    public TextComposite parseText(String fileName) throws TechnicalException{
        TextComposite textComposite = new TextComposite(ComponentType.TEXT);
        String text = readText(fileName);
        Pattern p = Pattern.compile(PARAGRAPH_OR_LISTING);
        Matcher m = p.matcher(text);
        while (m.find()){
            String paragraph = m.group();
            if (Pattern.matches(LISTING,paragraph)){
                textComposite.add(new Listing(paragraph));
            }
            else {
                textComposite.add(parseParagraph(paragraph + SPACE));
            }
        }
        return textComposite;
    }

    private String readText(String fileName) throws TechnicalException{
        String text = "";
        Scanner scanner = null;
        try{
            FileReader fileReader = new FileReader(fileName);
            scanner = new Scanner(fileReader);
            scanner.useDelimiter(NEW_LINE);
            while (scanner.hasNext()){
                text=String.join(NEW_LINE,text,scanner.next());
            }
        } catch (FileNotFoundException e){
            throw new TechnicalException(e);
        } finally {
            if (scanner!=null){
                scanner.close();
            }
        }
        return text;
    }

    private TextComposite parseParagraph(String paragraph){
        TextComposite paragraphComposite = new TextComposite(ComponentType.PARAGRAPH);
        Pattern p = Pattern.compile(SENTENCE);
        Matcher m = p.matcher(paragraph);
        while (m.find()){
            paragraphComposite.add(parseSentence(m.group()));
        }
        return paragraphComposite;
    }

    private TextComposite parseSentence(String sentence){
        TextComposite sentenceComposite = new TextComposite(ComponentType.SENTENCE);
        for (String lexeme:sentence.split(SPACE)){
            sentenceComposite.add(parseLexeme(lexeme));
            }
        return  sentenceComposite;
    }

    private TextComposite parseLexeme(String lexeme){
        TextComposite lexemeComposite = new TextComposite(ComponentType.LEXEME);
        Pattern p = Pattern.compile(WORD_OR_PUNCTUATION);
        Matcher m = p.matcher(lexeme);
        while (m.find()) {
            String word = m.group();
            if (Pattern.matches(WORD, word)) {
                lexemeComposite.add(parseWord(word));
            } else {
                for (char sign : word.toCharArray()) {
                    lexemeComposite.add(new Sign(sign));
                }
            }
        }
        return lexemeComposite;
    }

    private TextComposite parseWord(String word){
        TextComposite wordComposite = new TextComposite(ComponentType.WORD);
        for (char symbol:word.toCharArray()){
            wordComposite.add(new Sign(symbol));
        }
        return  wordComposite;
    }
}
