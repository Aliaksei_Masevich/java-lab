package by.mosevich.second.report;

import by.mosevich.second.entity.TextComposite;
import by.mosevich.second.exception.TechnicalException;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ReportWriter {
    static Logger log = Logger.getLogger(ReportWriter.class);
    public void write(String fileName, TextComposite text) throws TechnicalException{
        PrintWriter printWriter = null;
        try{
            printWriter=new PrintWriter(new FileOutputStream(fileName));
            printWriter.println(text.getContent());
            log.info("Report "+fileName+" have been successfully written");
        } catch (FileNotFoundException e){
            throw new TechnicalException(e);
        } finally {
            if (printWriter!=null){
                printWriter.close();
            }
        }
    }
}
