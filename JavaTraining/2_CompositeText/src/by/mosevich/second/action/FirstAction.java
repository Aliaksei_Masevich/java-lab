package by.mosevich.second.action;

import by.mosevich.second.entity.Component;
import by.mosevich.second.entity.ComponentType;
import by.mosevich.second.entity.TextComposite;

import java.util.Collections;
import java.util.Comparator;

public class FirstAction {
    public static TextComposite action(final TextComposite text){
        TextComposite sentences = findSentences(text);
        Comparator<Component> comp = (one, two) -> {
            TextComposite firstSentence = (TextComposite)one;
            TextComposite secondSentence= (TextComposite)two;
            return Integer.compare(countWords(firstSentence),countWords(secondSentence));
        };
        Collections.sort(sentences.getComponents(),comp);
        return sentences;
    }

    private static TextComposite findSentences(TextComposite text){
        TextComposite sentences = new TextComposite(ComponentType.TEXT);
        text.getComponents().stream().filter(component -> ComponentType.PARAGRAPH.equals(component.getType())).forEach(component -> {
            TextComposite paragraph = (TextComposite) component;
            paragraph.getComponents().stream().filter(sentence -> ComponentType.SENTENCE.equals(sentence.getType())).forEach(sentences::add);
        });
        return sentences;
    }

    private static int countWords(TextComposite sentence){
        int count=0;
        for(Component lexeme:sentence.getComponents()){
            if (ComponentType.LEXEME.equals(lexeme.getType())){
                TextComposite lexemeComposite = (TextComposite)lexeme;
                for (Component lexemePart:lexemeComposite.getComponents()){
                    if (ComponentType.WORD.equals(lexemePart.getType())){
                        count++;
                    }
                }
            }
        }
        return count;
    }
}
