package by.mosevich.second.action;

import by.mosevich.second.entity.Component;
import by.mosevich.second.entity.ComponentType;
import by.mosevich.second.entity.TextComposite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SecondAction {

    private static final String VOWELS = "[AEIOUYaeiouyАЕЁИОУЭЮЯаеёиоуыэюя]";

    public static TextComposite action(final TextComposite text){
        TextComposite words = findWords(text);
        Comparator<Component> comp = (one, two) -> {
            TextComposite firstWord = (TextComposite)one;
            TextComposite secondWord= (TextComposite)two;
            return Double.compare(vowelWeight(firstWord),vowelWeight(secondWord));
        };
        Collections.sort(words.getComponents(),comp);
        return words;
    }

    private static TextComposite findWords(TextComposite text){
        TextComposite words = new TextComposite(ComponentType.TEXT);
        for (Component sentence:findSentences(text)){
            TextComposite sentenceComposite = (TextComposite)sentence;
            sentenceComposite.getComponents().stream().filter(lexeme -> ComponentType.LEXEME.equals(lexeme.getType())).forEach(lexeme -> {
                TextComposite lexemeComposite = (TextComposite) lexeme;
                lexemeComposite.getComponents().stream().filter(lexemePart -> ComponentType.WORD.equals(lexemePart.getType())).forEach(words::add);
            });
        }
        return words;
    }

    private static List<Component> findSentences(TextComposite text){
        List<Component> sentences = new ArrayList<>();
        text.getComponents().stream().filter(component -> ComponentType.PARAGRAPH.equals(component.getType())).forEach(component -> {
            TextComposite paragraph = (TextComposite) component;
            sentences.addAll(paragraph.getComponents().stream().filter(sentence -> ComponentType.SENTENCE.equals(sentence.getType())).collect(Collectors.toList()));
        });
        return sentences;
    }

    private static double vowelWeight(TextComposite word){
        int vowelCount=0;
        int totalCount=0;
        for(Component letter:word.getComponents()){
            if (letter.getContent().matches(VOWELS)){
                vowelCount++;
            }
            totalCount++;
        }
        return totalCount!=0?(double)vowelCount/totalCount:0;
    }
}
