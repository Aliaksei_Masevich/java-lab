package by.mosevich.first.manager;

import by.mosevich.first.entity.equipment.Equipment;
import by.mosevich.first.entity.motorcyclist.Motorcyclist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class EquipmentManager {

    public static double calculateEquipmentPrice(Motorcyclist motorcyclist){
        double price = 0;
        for (Equipment eq: motorcyclist.getEquipment()){
            price+=eq.getPrice();
        }
        return price;
    }

    public static void sortByWeight(Motorcyclist motorcyclist){
        Comparator<Equipment> comp = new Comparator<Equipment>(){
            public int compare(Equipment one, Equipment two){
                return Double.compare(one.getWeight(),two.getWeight());
            }
        };
        Collections.sort(motorcyclist.getEquipment(), comp);
    }

    public static List<Equipment> findByPrice(Motorcyclist motorcyclist,double minPrice,double maxPrice){
        List<Equipment> resultList = new ArrayList<>();
        List<Equipment> list= motorcyclist.getEquipment();
        for (Equipment eq : list ){
            if (eq.getPrice()>=minPrice && eq.getPrice()<=maxPrice){
                resultList.add(eq);
            }
        }
        return resultList;
    }
}
