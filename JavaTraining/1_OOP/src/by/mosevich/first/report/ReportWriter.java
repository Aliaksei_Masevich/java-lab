package by.mosevich.first.report;

import by.mosevich.first.entity.motorcyclist.Motorcyclist;
import by.mosevich.first.exception.TechnicalException;
import by.mosevich.first.manager.EquipmentManager;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ReportWriter {
    static Logger log = Logger.getLogger(ReportWriter.class);
    public static void write(Motorcyclist motorcyclist, double minPrice, double maxPrice) throws TechnicalException{
        PrintWriter printWriter = null;
        try{
            printWriter=new PrintWriter(new FileOutputStream("output/report.txt"));
            printWriter.println("Report:\n");
            printWriter.println(motorcyclist);
            printWriter.println("\nEquipment price:" + EquipmentManager.calculateEquipmentPrice(motorcyclist) + "$");
            EquipmentManager.sortByWeight(motorcyclist);
            printWriter.println("\nAfter sorting by weight:");
            printWriter.println(motorcyclist);
            printWriter.println("\nPrice range "+minPrice+"-"+maxPrice+":");
            printWriter.println(EquipmentManager.findByPrice(motorcyclist, minPrice, maxPrice));
            log.info("Report have been successfully written");
        } catch (FileNotFoundException e){
            throw new TechnicalException("Unable To Write a Report");
        } finally {
            if (printWriter!=null){
                printWriter.close();
            }
        }

    }
}
