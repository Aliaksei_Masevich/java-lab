package by.mosevich.first.creator;

import by.mosevich.first.entity.equipment.*;
import by.mosevich.first.exception.TechnicalException;
import by.mosevich.first.validator.BodyValidator;
import by.mosevich.first.validator.BootsValidator;
import by.mosevich.first.validator.GlovesValidator;
import by.mosevich.first.validator.HelmetValidator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Creator {
    static private Logger log = Logger.getLogger(Creator.class);
    public static List<Equipment> create(String path) throws TechnicalException{
        try{
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(new FileReader(path)).getAsJsonObject();
            List<Equipment> list = new ArrayList<>();
            for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()){
                if (!addEquipment(entry,list)){
                    log.info("Equipment hasn't been created");
                }
            }
            return list;
        } catch (FileNotFoundException e){
            throw new TechnicalException(e.getMessage());
        }

    }
    private static boolean addEquipment(Map.Entry<String,JsonElement> entry,List<Equipment> list){
            JsonObject parameters = entry.getValue().getAsJsonObject();
                switch (entry.getKey()){
                    case "Helmet":
                        if (HelmetValidator.validate(parameters)){
                            int size = parameters.get("Size").getAsInt();
                            double price = parameters.get("Price").getAsDouble();
                            double weight = parameters.get("Weight").getAsDouble();
                            String color = parameters.get("Color").getAsString();
                            String material = parameters.get("Material").getAsString();
                            boolean opened = parameters.get("Opened").getAsBoolean();
                            boolean havingGlass = parameters.get("HavingGlass").getAsBoolean();
                            int ventilationHoles = parameters.get("VentilationHoles").getAsInt();
                            list.add(new Helmet(size,price,weight,color,material,opened,havingGlass,ventilationHoles));
                            return true;
                        }
                        break;
                    case "Body":
                        if (BodyValidator.validate(parameters)){
                            int size = parameters.get("Size").getAsInt();
                            double price = parameters.get("Price").getAsDouble();
                            double weight = parameters.get("Weight").getAsDouble();
                            String color = parameters.get("Color").getAsString();
                            String material = parameters.get("Material").getAsString();
                            String protectionType = parameters.get("ProtectionType").getAsString();
                            boolean havingVentilation = parameters.get("HavingVentilation").getAsBoolean();
                            list.add(new Body(size,price,weight,color,material,protectionType,havingVentilation));
                            return true;
                        }
                        break;
                    case "Gloves":
                        if (GlovesValidator.validate(parameters)){
                            int size = parameters.get("Size").getAsInt();
                            double price = parameters.get("Price").getAsDouble();
                            double weight = parameters.get("Weight").getAsDouble();
                            String color = parameters.get("Color").getAsString();
                            String material = parameters.get("Material").getAsString();
                            String length = parameters.get("Length").getAsString();
                            list.add(new Gloves(size,price,weight,color,material,length));
                            return true;
                        }
                        break;
                    case "Boots":
                        if (BootsValidator.validate(parameters)){
                            int size = parameters.get("Size").getAsInt();
                            double price = parameters.get("Price").getAsDouble();
                            double weight = parameters.get("Weight").getAsDouble();
                            String color = parameters.get("Color").getAsString();
                            String material = parameters.get("Material").getAsString();
                            String soleStiffness = parameters.get("SoleStiffness").getAsString();
                            list.add(new Boots(size,price,weight,color,material,soleStiffness));
                            return true;
                        }
                        break;
                    default:
                        log.info("Unknown equipment: "+entry.getKey());
                }
            return false;
    }
}
