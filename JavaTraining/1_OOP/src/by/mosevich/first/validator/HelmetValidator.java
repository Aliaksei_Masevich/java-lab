package by.mosevich.first.validator;

import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

public class HelmetValidator {
    private static Logger log = Logger.getLogger(HelmetValidator.class);
    public static boolean validate(JsonObject parameters){
        if (!parameters.has("Size") || !parameters.has("Price") || !parameters.has("Weight") ||
                !parameters.has("Color") || !parameters.has("Material") || !parameters.has("Opened") ||
                !parameters.has("HavingGlass") || !parameters.has("VentilationHoles")) {
            log.info("Incorrect parameter list");
            return false;
        }
        try{
            if (parameters.get("Size").getAsInt()<=0){
                log.info("Size should be positive");
                return false;
            }
            if (parameters.get("Price").getAsDouble()<=0){
                log.info("Price should be positive");
                return false;
            }
            if (parameters.get("Weight").getAsDouble()<=0){
                log.info("Weight should be positive");
                return false;
            }
            if (parameters.get("VentilationHoles").getAsInt()<=0){
                log.info("Ventilation Hole Count should be positive");
                return false;
            }
        } catch (NumberFormatException e){
            log.info("Wrong Number Format "+e.getMessage());
            return false;
        }
        return true;
    }
}
