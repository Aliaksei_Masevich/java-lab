package by.mosevich.first.runner;

import by.mosevich.first.creator.Creator;
import by.mosevich.first.entity.motorcyclist.Motorcyclist;
import by.mosevich.first.exception.TechnicalException;
import by.mosevich.first.report.ReportWriter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Runner {
    static {
        PropertyConfigurator.configure("log4j.properties");
    }
    static Logger log = Logger.getLogger(Runner.class);
    public static void main(String[] args) {
        try {
            Motorcyclist motorcyclist = new Motorcyclist("Stephen King");
            motorcyclist.setEquipment(Creator.create("input/equipment.json"));
            ReportWriter.write(motorcyclist,60,120);
        } catch (TechnicalException e){
            log.error(e.getMessage());
        }
    }
}
