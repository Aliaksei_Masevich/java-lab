package by.mosevich.first.entity.motorcyclist;

import by.mosevich.first.entity.equipment.Equipment;

import java.util.ArrayList;
import java.util.List;

public class Motorcyclist {
    private List<Equipment> equipment;
    private String name;

    public Motorcyclist(String name){
        this.name=name;
        equipment = new ArrayList<>();
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Equipment> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<Equipment> equipment){
        this.equipment = equipment;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MOTORCYCLIST INFO:\nNAME:");
        stringBuilder.append(name);
        stringBuilder.append("\nEQUIPMENT:");
        stringBuilder.append(equipment.isEmpty()?"NO EQUIPMENT": equipment.toString());
        return stringBuilder.toString();
    }
}
