package by.mosevich.first.entity.equipment;

public abstract class Equipment {
    private int size;
    private double price;
    private double weight;
    private String color;
    private String material;

    public Equipment(int size, double price, double weight, String color, String material){
        this.size=size;
        this.price=price;
        this.weight=weight;
        this.color=color;
        this.material=material;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size){
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight){
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material){
        this.material = material;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SIZE:");
        stringBuilder.append(size);
        stringBuilder.append("; PRICE:");
        stringBuilder.append(price);
        stringBuilder.append("$; WEIGHT:");
        stringBuilder.append(weight);
        stringBuilder.append("g.; COLOR:");
        stringBuilder.append(color);
        stringBuilder.append("; MATERIAL:");
        stringBuilder.append(material);
        return stringBuilder.toString();
    }
}
