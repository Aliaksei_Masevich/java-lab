package by.mosevich.first.entity.equipment;

public class Boots extends Equipment {
    private String soleStiffness;

    public Boots(int size, double price, double weight, String color, String material, String soleStiffness){
        super(size, price, weight, color, material);
        this.soleStiffness=soleStiffness;
    }

    public String getSoleStiffness() {
        return soleStiffness;
    }

    public void setSoleStiffness(String soleStiffness){
            this.soleStiffness = soleStiffness;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nBOOTS:\n");
        stringBuilder.append(super.toString());
        stringBuilder.append("; SOLE STIFFNESS:");
        stringBuilder.append(soleStiffness);
        return stringBuilder.toString();
    }
}
