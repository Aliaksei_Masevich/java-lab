package by.mosevich.first.entity.equipment;

public class Gloves extends Equipment{
    private String length;

    public Gloves(int size, double price, double weight, String color, String material, String length){
        super(size, price, weight, color, material);
        this.length=length;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length){
        this.length = length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nGLOVES:\n");
        stringBuilder.append(super.toString());
        stringBuilder.append("; LENGTH:");
        stringBuilder.append(length);
        return stringBuilder.toString();
    }
}
