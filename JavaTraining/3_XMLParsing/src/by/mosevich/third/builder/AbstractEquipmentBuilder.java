package by.mosevich.third.builder;

import by.mosevich.third.entity.Equipment;

import java.util.Set;

public abstract class AbstractEquipmentBuilder {
    protected Set<Equipment> equipmentSet;

    public AbstractEquipmentBuilder() {}

    public AbstractEquipmentBuilder(Set<Equipment> equipmentSet) {
        this.equipmentSet = equipmentSet;
    }

    public Set<Equipment> getEquipmentSet() {
        return equipmentSet;
    }

    abstract public void buildEquipmentSet(String fileName);
}
