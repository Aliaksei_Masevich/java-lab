package by.mosevich.third.builder;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

public class EquipmentSAXBuilder extends AbstractEquipmentBuilder{
    static Logger log = Logger.getLogger(EquipmentSAXBuilder.class);
    private EquipmentHandler equipmentHandler;
    private XMLReader reader;

    public EquipmentSAXBuilder() {
        equipmentHandler = new EquipmentHandler();
        try{
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(equipmentHandler);
        } catch (SAXException e){
            log.error("SAX Parser Error: " + e);
        }
    }

    @Override
    public void buildEquipmentSet(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            log.error("SAX Parser Error: " + e);
        } catch (IOException e) {
            log.error("I/O Stream Error: " + e);
        }
        equipmentSet = equipmentHandler.getEquipmentSet();
    }

}
