package by.mosevich.third.builder;

import by.mosevich.third.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class EquipmentHandler extends DefaultHandler {
    private Set<Equipment> equipmentSet;
    private Equipment current = null;
    private EquipmentEnum currentEnum = null;
    private EnumSet<EquipmentEnum> withText;
    public EquipmentHandler() {
        equipmentSet = new HashSet<>();
        withText = EnumSet.range(EquipmentEnum.SIZE,EquipmentEnum.VENTILATION_HOLES);
    }

    public Set<Equipment> getEquipmentSet() {
        return equipmentSet;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (EquipmentEnum.valueOf(localName.replace('-','_').toUpperCase())) {
            case BODY:
                current = new Body();
                current.setId(attributes.getValue(0));
                break;
            case BOOTS:
                current = new Boots();
                current.setId(attributes.getValue(0));
                break;
            case GLOVES:
                current = new Gloves();
                current.setId(attributes.getValue(0));
                break;
            case HELMET:
                current = new Helmet();
                current.setId(attributes.getValue(0));
                break;
            default:
                EquipmentEnum temp = EquipmentEnum.valueOf(localName.replace('-','_').toUpperCase());
                if (withText.contains(temp)){
                    currentEnum = temp;
                }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (EquipmentEnum.BODY.getValue().equals(localName) ||
                EquipmentEnum.BOOTS.getValue().equals(localName) ||
                        EquipmentEnum.GLOVES.getValue().equals(localName) ||
                                EquipmentEnum.HELMET.getValue().equals(localName)){
            equipmentSet.add(current);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String s = new String(ch,start,length).trim();
        if (currentEnum != null){
            switch (currentEnum){
               case SIZE:
                   current.setSize(Integer.parseInt(s));
                   break;
               case PRICE:
                   current.setPrice(Double.parseDouble(s));
                   break;
               case WEIGHT:
                   current.setWeight(Double.parseDouble(s));
                   break;
               case COLOR:
                   current.setColor(s);
                   break;
               case MATERIAL:
                   current.setMaterial(s);
                   break;
               case PROTECTION_TYPE:
                   ((Body)current).setProtectionType(s);
                   break;
               case HAVING_VENTILATION:
                   ((Body)current).setHavingVentilation(Boolean.parseBoolean(s));
                   break;
               case SOLE_STIFFNESS:
                   ((Boots)current).setSoleStiffness(s);
                   break;
               case LENGTH:
                   ((Gloves)current).setLength(s);
                   break;
               case OPENED:
                   ((Helmet)current).setOpened(Boolean.parseBoolean(s));
                   break;
               case HAVING_GLASS:
                   ((Helmet)current).setHavingGlass(Boolean.parseBoolean(s));
                   break;
               case VENTILATION_HOLES:
                   ((Helmet)current).setVentilationHoles(Integer.parseInt(s));
                   break;
               default:
                   throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(),currentEnum.name());
            }
        }
        currentEnum=null;
    }
}
