package by.mosevich.third.builder;

import by.mosevich.third.entity.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;

public class EquipmentDOMBuilder extends AbstractEquipmentBuilder{
    static Logger log = Logger.getLogger(EquipmentDOMBuilder.class);
    private DocumentBuilder documentBuilder;

    public EquipmentDOMBuilder() {
        equipmentSet=new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e){
            log.error("DOM Parser Configuration Error: " + e);
        }
    }

    @Override
    public void buildEquipmentSet(String fileName) {
        Document doc;
        try{
            doc = documentBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList equipmentList = root.getElementsByTagName(EquipmentEnum.BODY.getValue());
            for (int i=0;i<equipmentList.getLength();i++){
                Element equipmentElement = (Element)equipmentList.item(i);
                Equipment equipment = buildBody(equipmentElement);
                equipmentSet.add(equipment);
            }
            equipmentList = root.getElementsByTagName(EquipmentEnum.BOOTS.getValue());
            for (int i=0;i<equipmentList.getLength();i++){
                Element equipmentElement = (Element)equipmentList.item(i);
                Equipment equipment = buildBoots(equipmentElement);
                equipmentSet.add(equipment);
            }
            equipmentList = root.getElementsByTagName(EquipmentEnum.GLOVES.getValue());
            for (int i=0;i<equipmentList.getLength();i++){
                Element equipmentElement = (Element)equipmentList.item(i);
                Equipment equipment = buildGloves(equipmentElement);
                equipmentSet.add(equipment);
            }
            equipmentList = root.getElementsByTagName(EquipmentEnum.HELMET.getValue());
            for (int i=0;i<equipmentList.getLength();i++){
                Element equipmentElement = (Element)equipmentList.item(i);
                Equipment equipment = buildHelmet(equipmentElement);
                equipmentSet.add(equipment);
            }
        } catch (IOException e){
            log.error("File error or I/O error: " + e);
        } catch (SAXException e){
            log.error("Parsing failure: " + e);
        }
    }

    private Equipment buildBody(Element equipmentElement){
        Body body = new Body();
        body.setSize(Integer.parseInt(getElementTextContent(equipmentElement,EquipmentEnum.SIZE.getValue())));
        body.setPrice(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.PRICE.getValue())));
        body.setWeight(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.WEIGHT.getValue())));
        body.setColor(getElementTextContent(equipmentElement,EquipmentEnum.COLOR.getValue()));
        body.setMaterial(getElementTextContent(equipmentElement,EquipmentEnum.MATERIAL.getValue()));
        body.setProtectionType(getElementTextContent(equipmentElement, EquipmentEnum.PROTECTION_TYPE.getValue()));
        body.setHavingVentilation(Boolean.parseBoolean(getElementTextContent(equipmentElement, EquipmentEnum.HAVING_VENTILATION.getValue())));
        if (equipmentElement.hasAttribute(EquipmentEnum.ID.getValue())){
            body.setId(equipmentElement.getAttribute(EquipmentEnum.ID.getValue()));
        }
        return body;
    }

    private Equipment buildBoots(Element equipmentElement){
        Boots boots = new Boots();
        boots.setSize(Integer.parseInt(getElementTextContent(equipmentElement,EquipmentEnum.SIZE.getValue())));
        boots.setPrice(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.PRICE.getValue())));
        boots.setWeight(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.WEIGHT.getValue())));
        boots.setColor(getElementTextContent(equipmentElement,EquipmentEnum.COLOR.getValue()));
        boots.setMaterial(getElementTextContent(equipmentElement,EquipmentEnum.MATERIAL.getValue()));
        boots.setSoleStiffness(getElementTextContent(equipmentElement, EquipmentEnum.SOLE_STIFFNESS.getValue()));
        if (equipmentElement.hasAttribute(EquipmentEnum.ID.getValue())){
            boots.setId(equipmentElement.getAttribute(EquipmentEnum.ID.getValue()));
        }
        return boots;
    }

    private Equipment buildGloves(Element equipmentElement){
        Gloves gloves = new Gloves();
        gloves.setSize(Integer.parseInt(getElementTextContent(equipmentElement,EquipmentEnum.SIZE.getValue())));
        gloves.setPrice(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.PRICE.getValue())));
        gloves.setWeight(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.WEIGHT.getValue())));
        gloves.setColor(getElementTextContent(equipmentElement,EquipmentEnum.COLOR.getValue()));
        gloves.setMaterial(getElementTextContent(equipmentElement,EquipmentEnum.MATERIAL.getValue()));
        gloves.setLength(getElementTextContent(equipmentElement, EquipmentEnum.LENGTH.getValue()));
        if (equipmentElement.hasAttribute(EquipmentEnum.ID.getValue())){
            gloves.setId(equipmentElement.getAttribute(EquipmentEnum.ID.getValue()));
        }
        return gloves;
    }

    private Equipment buildHelmet(Element equipmentElement){
        Helmet helmet = new Helmet();
        helmet.setSize(Integer.parseInt(getElementTextContent(equipmentElement,EquipmentEnum.SIZE.getValue())));
        helmet.setPrice(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.PRICE.getValue())));
        helmet.setWeight(Double.parseDouble(getElementTextContent(equipmentElement, EquipmentEnum.WEIGHT.getValue())));
        helmet.setColor(getElementTextContent(equipmentElement,EquipmentEnum.COLOR.getValue()));
        helmet.setMaterial(getElementTextContent(equipmentElement,EquipmentEnum.MATERIAL.getValue()));
        helmet.setOpened(Boolean.parseBoolean(getElementTextContent(equipmentElement, EquipmentEnum.OPENED.getValue())));
        helmet.setVentilationHoles(Integer.parseInt(getElementTextContent(equipmentElement, EquipmentEnum.VENTILATION_HOLES.getValue())));
        if (equipmentElement.hasAttribute(EquipmentEnum.ID.getValue())){
            helmet.setId(equipmentElement.getAttribute(EquipmentEnum.ID.getValue()));
        }
        return helmet;
    }

    private static String getElementTextContent(Element element, String elementName){
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }
}
