package by.mosevich.third.builder;

public enum EquipmentEnum {
    BODY("body"),
    BOOTS("boots"),
    GLOVES("gloves"),
    HELMET("helmet"),
    SIZE("size"),
    PRICE("price"),
    WEIGHT("weight"),
    COLOR("color"),
    MATERIAL("material"),
    PROTECTION_TYPE("protection-type"),
    HAVING_VENTILATION("having-ventilation"),
    SOLE_STIFFNESS("sole-stiffness"),
    LENGTH("length"),
    OPENED("opened"),
    HAVING_GLASS("having-glass"),
    VENTILATION_HOLES("ventilation-holes"),
    ID("id");
    private String value;
    private EquipmentEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
