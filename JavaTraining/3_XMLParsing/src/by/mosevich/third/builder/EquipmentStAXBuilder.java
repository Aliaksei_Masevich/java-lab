package by.mosevich.third.builder;

import by.mosevich.third.entity.*;
import by.mosevich.third.exception.TechnicalException;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

public class EquipmentStAXBuilder extends AbstractEquipmentBuilder{
    static Logger log = Logger.getLogger(EquipmentStAXBuilder.class);
    private XMLInputFactory inputFactory;

    public EquipmentStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
        equipmentSet = new HashSet<>();
    }

    @Override
    public void buildEquipmentSet(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader=inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()){
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT){
                    name = reader.getLocalName();
                    if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.BODY){
                        equipmentSet.add(buildBody(reader));
                    }
                    if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.BOOTS){
                        equipmentSet.add(buildBoots(reader));
                    }
                    if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.GLOVES){
                        equipmentSet.add(buildGloves(reader));
                    }
                    if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.HELMET){
                        equipmentSet.add(buildHelmet(reader));
                    }
                }
            }
        } catch (XMLStreamException|TechnicalException e){
            log.error("StAX parsing error! " + e);
        } catch (FileNotFoundException e){
            log.error("File " + fileName + " not found! " + e);
        } finally {
            try {
                if (inputStream!=null){
                    inputStream.close();
                }
            } catch (IOException e){
                log.error("Impossible to close file " + fileName + ": " + e);
            }
        }
    }

    private Equipment buildBody(XMLStreamReader reader) throws TechnicalException{
        Body body = new Body();
        body.setId(reader.getAttributeValue(null,EquipmentEnum.ID.getValue()));
        String name;
        try {
            while (reader.hasNext()){
                int type=reader.next();
                switch (type){
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())){
                            case SIZE:
                                body.setSize(Integer.parseInt(getXMLText(reader)));
                                break;
                            case PRICE:
                                body.setPrice(Double.parseDouble(getXMLText(reader)));
                                break;
                            case WEIGHT:
                                body.setWeight(Double.parseDouble(getXMLText(reader)));
                                break;
                            case COLOR:
                                body.setColor(getXMLText(reader));
                                break;
                            case MATERIAL:
                                body.setMaterial(getXMLText(reader));
                                break;
                            case PROTECTION_TYPE:
                                body.setProtectionType(getXMLText(reader));
                                break;
                            case HAVING_VENTILATION:
                                body.setHavingVentilation(Boolean.parseBoolean(getXMLText(reader)));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.BODY){
                            return body;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            throw new TechnicalException(e);
        }
        throw new TechnicalException("Unknown element in tag Body");
    }

    private Equipment buildBoots(XMLStreamReader reader) throws TechnicalException{
        Boots boots = new Boots();
        boots.setId(reader.getAttributeValue(null,EquipmentEnum.ID.getValue()));
        String name;
        try {
            while (reader.hasNext()){
                int type=reader.next();
                switch (type){
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (EquipmentEnum.valueOf(name.replace('-', '_').toUpperCase())){
                            case SIZE:
                                boots.setSize(Integer.parseInt(getXMLText(reader)));
                                break;
                            case PRICE:
                                boots.setPrice(Double.parseDouble(getXMLText(reader)));
                                break;
                            case WEIGHT:
                                boots.setWeight(Double.parseDouble(getXMLText(reader)));
                                break;
                            case COLOR:
                                boots.setColor(getXMLText(reader));
                                break;
                            case MATERIAL:
                                boots.setMaterial(getXMLText(reader));
                                break;
                            case SOLE_STIFFNESS:
                                boots.setSoleStiffness(getXMLText(reader));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.BOOTS){
                            return boots;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            throw new TechnicalException(e);
        }
        throw new TechnicalException("Unknown element in tag Boots");
    }

    private Equipment buildGloves(XMLStreamReader reader) throws TechnicalException{
        Gloves gloves = new Gloves();
        gloves.setId(reader.getAttributeValue(null, EquipmentEnum.ID.getValue()));
        String name;
        try {
            while (reader.hasNext()){
                int type=reader.next();
                switch (type){
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())){
                            case SIZE:
                                gloves.setSize(Integer.parseInt(getXMLText(reader)));
                                break;
                            case PRICE:
                                gloves.setPrice(Double.parseDouble(getXMLText(reader)));
                                break;
                            case WEIGHT:
                                gloves.setWeight(Double.parseDouble(getXMLText(reader)));
                                break;
                            case COLOR:
                                gloves.setColor(getXMLText(reader));
                                break;
                            case MATERIAL:
                                gloves.setMaterial(getXMLText(reader));
                                break;
                            case LENGTH:
                                gloves.setLength(getXMLText(reader));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.GLOVES){
                            return gloves;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            throw new TechnicalException(e);
        }
        throw new TechnicalException("Unknown element in tag Gloves");
    }

    private Equipment buildHelmet(XMLStreamReader reader) throws TechnicalException{
        Helmet helmet = new Helmet();
        helmet.setId(reader.getAttributeValue(null,EquipmentEnum.ID.getValue()));
        String name;
        try {
            while (reader.hasNext()){
                int type=reader.next();
                switch (type){
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (EquipmentEnum.valueOf(name.replace('-', '_').toUpperCase())){
                            case SIZE:
                                helmet.setSize(Integer.parseInt(getXMLText(reader)));
                                break;
                            case PRICE:
                                helmet.setPrice(Double.parseDouble(getXMLText(reader)));
                                break;
                            case WEIGHT:
                                helmet.setWeight(Double.parseDouble(getXMLText(reader)));
                                break;
                            case COLOR:
                                helmet.setColor(getXMLText(reader));
                                break;
                            case MATERIAL:
                                helmet.setMaterial(getXMLText(reader));
                                break;
                            case OPENED:
                                helmet.setOpened(Boolean.parseBoolean(getXMLText(reader)));
                                break;
                            case HAVING_GLASS:
                                helmet.setHavingGlass(Boolean.parseBoolean(getXMLText(reader)));
                                break;
                            case VENTILATION_HOLES:
                                helmet.setVentilationHoles(Integer.parseInt(getXMLText(reader)));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (EquipmentEnum.valueOf(name.replace('-','_').toUpperCase())==EquipmentEnum.HELMET){
                            return helmet;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            throw new TechnicalException(e);
        }
        throw new TechnicalException("Unknown element in tag Helmet");
    }

    private String getXMLText(XMLStreamReader reader) throws TechnicalException{
        try {
            String text = null;
            if (reader.hasNext()){
                reader.next();
                text=reader.getText();
            }
            return text;
        } catch (XMLStreamException e) {
            throw new TechnicalException(e);
        }
    }
}
