package by.mosevich.third.entity;

public class Body extends Equipment {
    private String protectionType;
    private boolean havingVentilation;

    public Body() {
    }

    public Body(int size, double price, double weight, String color, String material,String id, String protectionType, boolean havingVentilation){
        super(size, price, weight, color, material, id);
        this.protectionType=protectionType;
        this.havingVentilation=havingVentilation;
    }

    public String getProtectionType() {
        return protectionType;
    }

    public void setProtectionType(String protectionType){
            this.protectionType = protectionType;
    }

    public boolean isHavingVentilation() {
        return havingVentilation;
    }

    public void setHavingVentilation(boolean havingVentilation) {
        this.havingVentilation = havingVentilation;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nBODY PROTECTION");
        stringBuilder.append(super.toString());
        stringBuilder.append(havingVentilation?"; Having":"; No");
        stringBuilder.append(" Ventilation; PROTECTION TYPE:");
        stringBuilder.append(protectionType);
        return stringBuilder.toString();
    }
}
