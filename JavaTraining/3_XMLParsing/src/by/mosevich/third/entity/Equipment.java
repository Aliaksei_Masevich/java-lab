package by.mosevich.third.entity;

public abstract class Equipment {
    private int size;
    private double price;
    private double weight;
    private String color;
    private String material;
    private String id;

    public Equipment() {
    }

    public Equipment(int size, double price, double weight, String color, String material, String id){
        this.size=size;
        this.price=price;
        this.weight=weight;
        this.color=color;
        this.material=material;
        this.id=id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size){
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight){
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material){
        this.material = material;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ID:");
        stringBuilder.append(id);
        stringBuilder.append("]:\nSIZE:");
        stringBuilder.append(size);
        stringBuilder.append("; PRICE:");
        stringBuilder.append(price);
        stringBuilder.append("$; WEIGHT:");
        stringBuilder.append(weight);
        stringBuilder.append("g.; COLOR:");
        stringBuilder.append(color);
        stringBuilder.append("; MATERIAL:");
        stringBuilder.append(material);
        return stringBuilder.toString();
    }
}
