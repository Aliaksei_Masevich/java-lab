package by.mosevich.third.entity;

public class Boots extends Equipment {
    private String soleStiffness;

    public Boots() {
    }

    public Boots(int size, double price, double weight, String color, String material, String id, String soleStiffness){
        super(size, price, weight, color, material, id);
        this.soleStiffness=soleStiffness;
    }

    public String getSoleStiffness() {
        return soleStiffness;
    }

    public void setSoleStiffness(String soleStiffness){
            this.soleStiffness = soleStiffness;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nBOOTS");
        stringBuilder.append(super.toString());
        stringBuilder.append("; SOLE STIFFNESS:");
        stringBuilder.append(soleStiffness);
        return stringBuilder.toString();
    }
}
