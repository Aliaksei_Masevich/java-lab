package by.mosevich.third.entity;

public class Gloves extends Equipment{
    private String length;

    public Gloves() {
    }

    public Gloves(int size, double price, double weight, String color, String material, String id, String length){
        super(size, price, weight, color, material, id);
        this.length=length;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length){
        this.length = length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nGLOVES");
        stringBuilder.append(super.toString());
        stringBuilder.append("; LENGTH:");
        stringBuilder.append(length);
        return stringBuilder.toString();
    }
}
