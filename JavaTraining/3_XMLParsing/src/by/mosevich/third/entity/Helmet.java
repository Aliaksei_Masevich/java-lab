package by.mosevich.third.entity;

public class Helmet extends Equipment {
    private boolean opened;
    private boolean havingGlass;
    private int ventilationHoles;

    public Helmet() {
    }

    public Helmet(int size, double price, double weight, String color, String material, String id, boolean opened, boolean havingGlass, int ventilationHoles){
        super(size, price, weight, color, material, id);
        this.opened=opened;
        this.havingGlass=havingGlass;
        this.ventilationHoles=ventilationHoles;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isHavingGlass() {
        return havingGlass;
    }

    public void setHavingGlass(boolean havingGlass) {
        this.havingGlass = havingGlass;
    }

    public int getVentilationHoles() {
        return ventilationHoles;
    }

    public void setVentilationHoles(int ventilationHoles) {
        this.ventilationHoles = ventilationHoles;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nHELMET");
        stringBuilder.append(super.toString());
        stringBuilder.append(opened?"; Opened":"; Closed");
        stringBuilder.append(havingGlass?"; Having":"; No");
        stringBuilder.append(" Glass; VENTILATION HOLE COUNT:");
        stringBuilder.append(ventilationHoles);
        return stringBuilder.toString();
    }
}
