package by.mosevich.third.runner;

import by.mosevich.third.builder.AbstractEquipmentBuilder;
import by.mosevich.third.factory.EquipmentBuilderFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Runner {
    static {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("log4j.properties"));
            PropertyConfigurator.configure(props);
        } catch (IOException e) {
            throw new ExceptionInInitializerError("Initialization Error: " + e.getMessage());
        }
    }
    static Logger log = Logger.getLogger(Runner.class);
    public static void main(String[] args) {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        String fileName="data/equipment.xml";
        String schemaName="data/equipment.xsd";
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(schemaName);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);
            EquipmentBuilderFactory builderFactory = new EquipmentBuilderFactory();
            AbstractEquipmentBuilder builder = builderFactory.createEquipmentBuilder("sax");
            builder.buildEquipmentSet(fileName);
            System.out.println("SAX Parser:\r\n" + builder.getEquipmentSet());
            builder = builderFactory.createEquipmentBuilder("dom");
            builder.buildEquipmentSet(fileName);
            System.out.println("\r\nDOM Parser:\r\n" + builder.getEquipmentSet());
            builder = builderFactory.createEquipmentBuilder("stax");
            builder.buildEquipmentSet(fileName);
            System.out.println("\r\nStAX Parser:\r\n" + builder.getEquipmentSet());
        } catch (SAXException e){
            log.error("Validation " + fileName + " is not valid because " + e.getMessage());
        } catch (IOException e){
            log.error(fileName + " is not valid because " + e.getMessage());
        }
    }
}
