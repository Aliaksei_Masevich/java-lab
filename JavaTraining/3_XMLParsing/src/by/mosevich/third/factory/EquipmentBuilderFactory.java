package by.mosevich.third.factory;

import by.mosevich.third.builder.AbstractEquipmentBuilder;
import by.mosevich.third.builder.EquipmentDOMBuilder;
import by.mosevich.third.builder.EquipmentSAXBuilder;
import by.mosevich.third.builder.EquipmentStAXBuilder;

public class EquipmentBuilderFactory {
    private enum TypeParser{
        DOM, SAX, STAX
    }
    public AbstractEquipmentBuilder createEquipmentBuilder(String typeParser){
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type){
            case DOM:
                return new EquipmentDOMBuilder();
            case SAX:
                return new EquipmentSAXBuilder();
            case STAX:
                return new EquipmentStAXBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
        }
    }
}
