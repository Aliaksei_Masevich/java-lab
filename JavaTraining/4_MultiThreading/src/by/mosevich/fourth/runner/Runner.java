package by.mosevich.fourth.runner;

import by.mosevich.fourth.exception.LogicException;
import by.mosevich.fourth.resource.Tunnel;
import by.mosevich.fourth.thread.DirectionEnum;
import by.mosevich.fourth.thread.Train;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Runner {
    static {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("log4j.properties"));
            PropertyConfigurator.configure(props);
        } catch (IOException e) {
            throw new ExceptionInInitializerError("Initialization Error: " + e.getMessage());
        }
    }
    static Logger log = Logger.getLogger(Runner.class);
    public static void main(String[] args) {
        try {
            Tunnel tunnel1 = Tunnel.getInstance(0);
            Tunnel tunnel2 = Tunnel.getInstance(1);
            new Train(1,tunnel2,tunnel1,DirectionEnum.FORWARD,70).start();
            new Train(2,tunnel1,tunnel2,DirectionEnum.BACK,60).start();
            new Train(3,tunnel2,tunnel1,DirectionEnum.FORWARD,50).start();
            new Train(4,tunnel1,tunnel2,DirectionEnum.BACK,75).start();
            new Train(5,tunnel1,tunnel2,DirectionEnum.BACK,65).start();
            new Train(6,tunnel2,tunnel1,DirectionEnum.BACK,57).start();
            new Train(7,tunnel1,tunnel2,DirectionEnum.FORWARD,78).start();
            new Train(8,tunnel2,tunnel1,DirectionEnum.FORWARD,45).start();
            new Train(9,tunnel2,tunnel1,DirectionEnum.BACK,67).start();
            new Train(10,tunnel1,tunnel2,DirectionEnum.BACK,78).start();
        } catch (LogicException e){
            log.error(e);
        }

    }
}
