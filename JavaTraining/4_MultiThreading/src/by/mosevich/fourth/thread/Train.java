package by.mosevich.fourth.thread;

import by.mosevich.fourth.exception.TechnicalException;
import by.mosevich.fourth.resource.Tunnel;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Train extends Thread {
    private static final int CHANGE_TIME = 10;
    private static Logger log = Logger.getLogger(Train.class);
    private int trainId;
    private Tunnel currentTunnel;
    private Tunnel reserveTunnel;
    private DirectionEnum direction;
    private long timeOut;

    public Train(int trainId, Tunnel currentTunnel, Tunnel reserveTunnel,  DirectionEnum direction, long timeOut) {
        this.trainId = trainId;
        this.reserveTunnel=reserveTunnel;
        this.currentTunnel = currentTunnel;
        this.direction = direction;
        this.timeOut = timeOut;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    @Override
    public void run() {
        try {
            while (!currentTunnel.enter(timeOut)){
                changeTunnel();
            }
        } catch (TechnicalException e){
            log.info("Поезд #" + trainId + ": " + e.getMessage());
        }

    }

    private void changeTunnel() throws TechnicalException {
        try {
            System.out.println("Поезд #" + trainId + ": превышено время ожидания на проезд. Смена тоннеля: "
                    + currentTunnel.getTunnelId() + " -> " + reserveTunnel.getTunnelId());
            TimeUnit.MILLISECONDS.sleep(CHANGE_TIME);
            Tunnel temp = currentTunnel;
            currentTunnel = reserveTunnel;
            reserveTunnel = temp;
        } catch (InterruptedException e) {
            throw  new TechnicalException(e);
        }
    }
}
