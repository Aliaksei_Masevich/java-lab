package by.mosevich.fourth.resource;

import by.mosevich.fourth.exception.LogicException;
import by.mosevich.fourth.exception.TechnicalException;
import by.mosevich.fourth.thread.DirectionEnum;
import by.mosevich.fourth.thread.Train;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Tunnel {
    private static final int MAX_AVAILABLE = 2;
    private static final int PASSING_TIME = 30;
    private static Semaphore createSemaphore = new Semaphore(MAX_AVAILABLE, true);
    private static List<Tunnel> instances = new ArrayList<Tunnel>(MAX_AVAILABLE);
    private int tunnelId;
    private Lock lock = new ReentrantLock();

    private Tunnel(int tunnelId){
        this.tunnelId = tunnelId;
    }

    public int getTunnelId() {
        return tunnelId;
    }

    public void setTunnelId(int tunnelId) {
        this.tunnelId = tunnelId;
    }

    public static Tunnel getInstance(int index) throws LogicException {
        if (index >= 0 && index < instances.size()){
            return instances.get(index);
        }
        if (createSemaphore.tryAcquire()){
            Tunnel tunnel = new Tunnel(index);
            instances.add(tunnel);
            return tunnel;
        }
        throw new LogicException("Превышен лимит на число экземпляров");
    }

    public boolean enter (long timeOut) throws TechnicalException {
        try {
            if (lock.tryLock(timeOut, TimeUnit.MILLISECONDS)) {
                try {
                    System.out.println("Поезд #" + ((Train)Thread.currentThread()).getTrainId() + " вошел в тоннель #" + tunnelId);
                    TimeUnit.MILLISECONDS.sleep(PASSING_TIME);
                    System.out.println("Поезд #" + ((Train)Thread.currentThread()).getTrainId()
                            + " прошел через тоннель #" + tunnelId + " в "
                            + (DirectionEnum.FORWARD.equals(((Train) Thread.currentThread()).getDirection()) ? "прямом" : "обратном") + " направлении");
                    return true;
                } finally {
                    lock.unlock();
                }
            }
            return false;
        } catch (InterruptedException e) {
            throw new TechnicalException(e);
        }
    }
}
